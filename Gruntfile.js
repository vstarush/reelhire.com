module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngAnnotate: {
		    options: {
		        singleQuotes: true
		    },
		    app: {
		        files: {
		            './web/min-safe/app/app.js': [
		            	'./web/app/app.js',
		            	'./web/app/loading/loading.controller.js',
		            	'./web/app/loading/loading.js',
		            	'./web/app/login/login.controller.js',
		            	'./web/app/login/login.js',
		            	'./web/app/chooseRole/chooseRole.controller.js',
		            	'./web/app/register/register.controller.js',
		            	'./web/app/register/register.js',
		            	'./web/app/profile/profile.controller.js',
		            	'./web/app/profile/profile.js',
		            	'./web/app/changePassword/changePassword.controller.js',
		            	'./web/app/changePassword/changePassword.js',
		            	'./web/app/resetPwd/resetPassword.controller.js',
		            	'./web/app/resetPwd/resetPassword.js',
		            	'./web/app/resumeEdit/resumeEdit.controller.js',
		            	'./web/app/resumeEdit/resumeEdit.js',
		            	'./web/app/questionnaires/questionnaires.controller.js',
		            	'./web/app/questionnaires/questionnaires.js',
		            	'./web/app/questionnairesEdit/questionnairesEdit.controller.js',
		            	'./web/app/questionnairesEdit/questionnairesEdit.js',
		            	'./web/app/preview/preview.controller.js',
		            	'./web/app/preview/preview.js',
		            	'./web/app/getLink/getLink.controller.js',
		            	'./web/app/getLink/getLink.js',
		            	'./web/app/sendTo/sendTo.controller.js',
		            	'./web/app/sendTo/sendTo.js',
		            	'./web/app/photoUpload/photoUpload.controller.js',
		            	'./web/app/photoUpload/photoUpload.js',
		            	'./web/app/videoUpload/videoUpload.controller.js',
		            	'./web/app/videoUpload/videoUpload.js',
		            	'./web/app/webResume/webResume.controller.js',
		            	'./web/app/webResume/webResume.js',
		            	'./web/app/website/pages/pages.routes.js',
		            	'./web/app/insideQuestionnaire/insideQuestionnaire.controller.js',
		            	'./web/app/insideQuestionnaire/insideQuestionnaire.js',
		            	'./web/app/newQuestionnaire/newQuestionnaire.controller.js',
		            	'./web/app/newQuestionnaire/newQuestionnaire.js',
		            	'./web/app/recruiterQuestionnaires/recruiterQuestionnaires.controller.js',
		            	'./web/app/recruiterQuestionnaires/recruiterQuestionnaires.js',
		            	'./web/app/website/main/pageMain.controller.js',
			            './web/app/website/main/pageMain.js',
			            './web/app/website/about/about.controller.js',
			            './web/app/website/about/about.js',
			            './web/app/panel/panel.controller.js',
			            './web/app/panelRecruiter/panelRecruiter.controller.js',
			            './web/app/tutorial/tutorial.controller.js',
			            './web/app/topbar/topbar.controller.js',
			            './web/app/popup/popup.service.js',
		            	'./web/app/factories/resumeFactory.js',
		            	'./web/app/factories/emailFactory.js',
		            	'./web/app/factories/systemFactory.js',
		            	'./web/app/factories/questionnarieFactory.js',
		            	'./web/app/factories/recruiterFactory.js',
		            	'./web/app/serverRequests/hashids.min.js',
		            	'./web/app/exampleResume/exampleResume.js'

		            ]

		        }
		    }
		},
		
		concat: {
		    js: { //target
		        src: ['./web/min-safe/app/*.js' ],
		        dest: './web/min/app.js'
		    }
		},
		
		uglify: {
		    js: { //target
		        src: ['./web/min/app.js'],
		        dest: './web/min/app.js'
		    }
		}    
    });

    //load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate'); 

    
    
    
	
	//register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify']);
}


/*
    <script src="app/website/main/pageMain.controller.js"></script>
    <script src="app/website/main/pageMain.js"></script>
    <script src="app/website/about/about.controller.js"></script>
    <script src="app/website/about/about.js"></script>


    <script src="app/panel/panel.controller.js"></script>
    <script src="app/panelRecruiter/panelRecruiter.controller.js"></script>
    <script src="app/tutorial/tutorial.controller.js"></script>
    <script src="app/topbar/topbar.controller.js"></script>
	<script src="app/popup/popup.service.js"></script>
*/

/*
	<script src="app/factories/resumeFactory.js"></script>
    <script src="app/factories/emailFactory.js"></script>
    <script src="app/factories/systemFactory.js"></script>
    <script src="app/factories/questionnarieFactory.js"></script>
    <script src="app/factories/recruiterFactory.js"></script>
    <script src="app/serverRequests/hashids.min.js"></script>
	<script src="app/exampleResume/exampleResume.js"></script>
*/