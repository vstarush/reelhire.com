(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appResumeEdit = angular.module('seeMeHireMeApp')	 	
	.factory('questionnarieFactory', function($rootScope,$http,cookieManager,systemFactory,$sce,resumeFactory,$q,$state,popUpManager){
	    
        var questionnaries = [];
        var notifications = [];
        var ankets = [];
        var getAnket = function(id){
            var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'anket/'+id,
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    return $http(requestData).then(
                function(result){
                    var anket = result.data;
                    var isFoundResumeForAnket = false;
                    var questionnariesId = 0;
                    var currQ = {};
					for(var i=0; i<questionnaries.length; i++){
						if(questionnaries[i].anket_id === anket.id){
							currQ = questionnaries[i];
							if(currQ.ispublished){
					        	isFoundResumeForAnket = true;
								currQ.isHasAnket = true;
					        }else{
						        questionnariesId = currQ.id; 
					        }
				        }
			        }
			        
			        if(!isFoundResumeForAnket){
				        var newQuestionnarie = resumeFactory.emptyResume();
				        newQuestionnarie.companyInfo = anket.companyInfo;
				        newQuestionnarie.companyLogo = anket.companyLogo;
				        newQuestionnarie.companyName = anket.companyName;
				        newQuestionnarie.companyPosition = anket.companyPosition;
				        newQuestionnarie.anket_id = anket.id;
				        newQuestionnarie.video1.text = anket.video1_title;
				        newQuestionnarie.video2.text = anket.video2_title;
				        newQuestionnarie.video3.text = anket.video3_title;
				        newQuestionnarie.video4.text = anket.video4_title;
				        newQuestionnarie.isHasAnket = true;
						newQuestionnarie.isNew = false;
						newQuestionnarie.notificationId = 0;
						for(var i=0; i<notifications.length; i++){
							if(notifications[i].anket_id === newQuestionnarie.anket_id){
								newQuestionnarie.isNew = true;
								newQuestionnarie.notificationId = notifications[i].id;
							}
						}
						if(questionnariesId !== 0){
							if(currQ.hasOwnProperty('video1_url')){
								newQuestionnarie.video1.link = currQ.video1_url.replace("watch?v=", "v/");
						        newQuestionnarie.video2.link = currQ.video2_url.replace("watch?v=", "v/");
						        newQuestionnarie.video3.link = currQ.video3_url.replace("watch?v=", "v/");
						        newQuestionnarie.video4.link = currQ.video4_url.replace("watch?v=", "v/");
					        }

					        newQuestionnarie.id = currQ.id;
// 					        currQ = newQuestionnarie;
					        for(var i=0; i<questionnaries.length; i++){
								if(questionnaries[i].id === questionnariesId){
									if(!currQ.hasOwnProperty("firstname")){
										questionnaries[i] = newQuestionnarie;
									}else{
										questionnaries[i] = jQuery.extend(true, {}, currQ);
										questionnaries[i].isHasAnket = true;
										
									}
								}
							}
						}else{
					        questionnaries.push(newQuestionnarie);
					    }
			        }
			       if($rootScope.outSideAnketIdToShow != undefined && $rootScope.outSideAnketIdToShow != "" && $rootScope.outSideAnketIdToShow == anket.id){
				   		$rootScope.outSideAnketIdToShow = "";
		               	currQ.isNew = false;
						factory.deleteNotification(currQ.notificationId);
						if(currQ.ispublished){
							factory.setCurrentQuestionnarie(currQ.id);
							$state.go('publishedQuestionnaire');
						}else{
							factory.setCurrentQuestionnarie(anket.id);
		
							if(!currQ.hasOwnProperty('id')){
								factory.publishQuestionnarie(true);
							}else{
								$rootScope.currentResumeId = currQ.id;
							}
							$state.go('questionnairesEdit');
						}
		            }
			       
                }
            );
        };
       
        var getAnketsList = function(){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'folder/1',
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    $http(requestData).then(
		    	function(result){
			    	var anketList = result.data;
			    	ankets = anketList;
			    	if($rootScope.outSideAnketId){
				    	var isAlreadyInArray = false;
				    	for(var i=0;i<anketList.length;i++){
					    	if(anketList[i].id == $rootScope.outSideAnketId){
						    	isAlreadyInArray = true;
					    	}
				    	}
				    	if(!isAlreadyInArray){
					    	var temp = {};
					    	temp.id = $rootScope.outSideAnketId[0].toString();
					    	anketList[anketList.length] = temp;
					    	
				    		var request = {};
							request.aid = temp.id;
							request.to_user_id = $rootScope.userId;
							var requestData = {
			                    method: 'POST',
			                    url: systemFactory.serverUrl()+'share_anket',
			                    data: request,
			                    headers: {
								   'skey': systemFactory.md5(),
								}
			                };
			                $http(requestData).then(
			                	function(respond){console.log(respond);}
			                );
				    	}
				    	$rootScope.outSideAnketIdToShow = $rootScope.outSideAnketId;
				    	$rootScope.outSideAnketId = ''; 
				    }
					
			    	for(var i=0; i<anketList.length; i++)
                    {
                        getAnket(anketList[i].id);
                    }
                }
			);
		};
		var getNotificatoinList = function(){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'notify',
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    $http(requestData).then(
		    	function(result){
			    	var notificationList = result.data;
			    	notifications = notificationList;
			    	for(var i=0; i<notificationList.length; i++)
                    {
                        for(var j=0; j<questionnaries.length; j++){
							if(questionnaries[j].anket_id === notificationList[i].anket_id){
					        	questionnaries[j].isNew = true;
					        	questionnaries[j].notificationId = notificationList[i].id;
					        }
				        }
                    }
                    angular.element(document.getElementById('wrapper')).scope().updateNotifications();
                }
                
                
			);
		};

		var updateWithMyResume = function(questionnarie){
			var myResume = resumeFactory.myResume();
			
			if(!isFilled(questionnarie.firstname.text)) questionnarie.firstname.text = myResume.firstname.text;
			if(!isFilled(questionnarie.lastname.text)) questionnarie.lastname.text = myResume.lastname.text;
			if(!isFilled(questionnarie.email.text)) questionnarie.email.text = myResume.email.text;
			if(!isFilled(questionnarie.zipcode.text)) questionnarie.zipcode.text = myResume.zipcode.text;
			if(!isFilled(questionnarie.education.text)) questionnarie.education.text = myResume.education.text;
			if(!isFilled(questionnarie.areaofstudy.text)) questionnarie.areaofstudy.text = myResume.areaofstudy.text;
			if(!isFilled(questionnarie.experience.text)) questionnarie.experience.text = myResume.experience.text;
			if(!isFilled(questionnarie.industrydesired.text)) questionnarie.industrydesired.text = myResume.industrydesired.text;
			if(!isFilled(questionnarie.abletorelocate.text)) questionnarie.abletorelocate.text = myResume.abletorelocate.text;
			if(!isFilled(questionnarie.linkedIn.text)) questionnarie.linkedIn.text = myResume.linkedIn.text;
			if(!isFilled(questionnarie.additionalLink.text)) questionnarie.additionalLink.text = myResume.additionalLink.text;
			if(!isFilled(questionnarie.photo.link)) questionnarie.photo.link = myResume.photo.link;
			//if(!isFilled(questionnarie.id)) questionnarie.id = myResume.id;
		}
		var isFilled = function(textToCheck){
			if(textToCheck === undefined || textToCheck == null) textToCheck = "";
			return textToCheck.toString().length>2;
		}
		var currentQuestionnarie = 0;
		var currentVideo = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var currentPhoto = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var getCurrentQuestionnarie = function(){
	       	var index = 0;
	        for(var i=0; i<questionnaries.length; i++){
		        if(questionnaries[i].anket_id === currentQuestionnarie)
		        	index = i;
		    }
		    updateWithMyResume(questionnaries[index]);
			return questionnaries[index];  
        };
        var emptyQuestionnarie = function(){
	        	return {
		        	companyInfo:"",
					companyLogo:"",
					companyName:"",
					companyPosition:"",
					first_name:"",
					last_name:"",
					email:"",
					zip:"",
					city:"",
					state:"",
					education:"",
					areaofstudy:"",
					expirience:"",
					industrydesired:"",
					abletorelocate:"",
					linkedIn:"",
					photo_url:"",
					additionalLink:"",
					video1_title :"",
					video2_title :"",
					video3_title:"",
					video4_title :"",
					
					video1_url :"",
					video2_url :"",
					video3_url :"",
					video4_url :"",
					
					video1_thumbnail_url :"",
					video2_thumbnail_url :"",
					video3_thumbnail_url :"",
					video4_thumbnail_url :"",
					
					video_merged_title :"",
					video_merged_url :"",
					video_merged_thumbnail_url :"",
					
					ispublished :"",
					anket_id :""};
        }
	    var factory = {
		    notifications: function(){
			    return notifications
			},
		    questionnaries: function(){
			    return questionnaries;
		    },
		    ankets: function(){
			    return ankets;
		    },
		    deleteAnket: function(id){
			    var q = {};
			    for(var i=0;i<questionnaries.length;i++){
				    if(questionnaries[i].anket_id == id){
					    q = questionnaries[i];
				    }
			    }
			    this.deleteNotification(q.notificationId);
				this.eraseQuestionnarie(q);
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deleteanket',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
		
				}; 

			    $http(requestData).then(
				    function(respond){					   
					    for(var prop in ankets){
						    if(id == ankets[prop].id){
						        ankets.splice(ankets.indexOf(ankets[prop]), 1);
						    }
						}
				    }
			    );
			},
			deleteAllQuestionnaries: function(){
				
				questionnaries.forEach(function(q){
						this.deleteAnket(q.anket_id);	
					},this);
				questionnaries = [];
				var newServerResume = this.eraseQuestionnarie(resumeFactory.serverResume());
				resumeFactory.setServerResume(newServerResume);
				resumeFactory.restoreResume(true);
			},
			eraseQuestionnarie: function(q){
				var questionnarieToPublish = emptyQuestionnarie();
				questionnarieToPublish.id = q.id;
				questionnarieToPublish.anket_id = q.anket_id;
				questionnarieToPublish.ispublished = "false";
				q = questionnarieToPublish;

				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':questionnarieToPublish},
                    headers: {
					   'skey': systemFactory.md5()
					}
                };

                $http(requestData).then(
                    function(result){
	                    
	            });


				return questionnarieToPublish;
			},
		    deleteNotification: function(id){
			    if(id == 0) return;
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deletenotify',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
				};  
				 
			    $http(requestData);
			    for(var i=0; i<notifications.length; i++){
				    if(notifications[i].id === id){
			    		notifications.splice(i, 1);
			    	}
			    }
			    angular.element(document.getElementById('wrapper')).scope().updateNotifications();
		    },
		    setAnketsAnswers: function(){
			   
		        questionnaries = resumeFactory.allResume();
		        for(var i=0; i<questionnaries.length; i++){
			        questionnaries[i].isHasAnket = false;
			        questionnaries[i].isNew = false;
			        questionnaries[i].notificationId = 0;
		        }
		        getAnketsList();
		        getNotificatoinList();


	        },
	        currentQuestionnarie: function(){
		        return getCurrentQuestionnarie();
	        },
	        setCurrentQuestionnarie: function(anket_id){
		        currentQuestionnarie = anket_id;
	        },
			getPublishedQuestionnarieData: function()
			{
				urlparams=getUrlVars();	
				return $http.get(systemFactory.serverUrl()+'resume/'+currentQuestionnarie,null) 
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        getQuestionnairesPreviewData: function()
			{
				urlparams=getUrlVars();	
				var qResume = {};
				var qCurrent = getCurrentQuestionnarie();

				qResume.companyInfo = qCurrent.companyInfo;
				qResume.companyLogo = qCurrent.companyLogo;
				qResume.companyName = qCurrent.companyName;
				qResume.companyPosition = qCurrent.companyPosition;
				qResume.first_name = qCurrent.firstname.text;
				qResume.last_name = qCurrent.lastname.text;
				qResume.email = qCurrent.email.text;
				qResume.zip = qCurrent.zipcode.text;
				qResume.city = qCurrent.city;
				qResume.state = qCurrent.state;
				qResume.education = qCurrent.education.text;
				qResume.areaofstudy = qCurrent.areaofstudy.text;
				qResume.expirience = qCurrent.experience.text;
				qResume.industrydesired = qCurrent.industrydesired.text;
				qResume.abletorelocate = qCurrent.abletorelocate.text==="Yes"?1:0;
				qResume.linkedIn = qCurrent.linkedIn.text;
				qResume.additionalLink = qCurrent.additionalLink.text;
				qResume.photo_url = qCurrent.photo.link;
				qResume.video1_title = qCurrent.video1.text;
				qResume.video2_title = qCurrent.video2.text;
				qResume.video3_title = qCurrent.video3.text;
				qResume.video4_title = qCurrent.video4.text;
				
				qResume.video1_url = qCurrent.video1.link;
				qResume.video2_url = qCurrent.video2.link;
				qResume.video3_url = qCurrent.video3.link;
				qResume.video4_url = qCurrent.video4.link;
				
				qResume.video_merged_url = "local";
				qResume.data = qResume;
				return qResume;
	        },
	        getCurrentVideo: function(){
			    return currentVideo;
		    },
            setCurrentVideo: function(video){
                jQuery.each(currentVideo, function(key, val) {
                    if(video.hasOwnProperty(key)){
                        currentVideo[key] = video[key];
                    }
                });
                getCurrentQuestionnarie()["video"+$rootScope.currentVideoIndex].link = video.link;
		    },
		    resetCurrentVideo: function(){
                currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
		    getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            setCurrentPhoto: function(photo){
                jQuery.each(currentPhoto, function(key, val) {
                    if(photo.hasOwnProperty(key)){
                        currentPhoto[key] = photo[key];
                        getCurrentQuestionnarie().photo.link = photo[key];
                    }
                });
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
		    checkCurrentQuestionnarie: function(){
			    return resumeFactory.checkResume(getCurrentQuestionnarie());
		    },
		    publishResume: function (isBackgroundPublish){
			    factory.publishQuestionnarie(isBackgroundPublish);
			},
		    publishQuestionnarie: function (isBackgroundPublish){
			    var currQuestionnarie = getCurrentQuestionnarie();
				var questionnarieToPublish = {};
				questionnarieToPublish.companyInfo = currQuestionnarie.companyInfo;
				questionnarieToPublish.companyLogo = currQuestionnarie.companyLogo;
				questionnarieToPublish.companyName = currQuestionnarie.companyName;
				questionnarieToPublish.companyPosition = currQuestionnarie.companyPosition;
				questionnarieToPublish.first_name = currQuestionnarie.firstname.text;
				questionnarieToPublish.last_name = currQuestionnarie.lastname.text;
				questionnarieToPublish.email = currQuestionnarie.email.text;
				questionnarieToPublish.zip = currQuestionnarie.zipcode.text;
				questionnarieToPublish.city = currQuestionnarie.city;
				questionnarieToPublish.state = currQuestionnarie.state;
				questionnarieToPublish.education = currQuestionnarie.education.text;
				questionnarieToPublish.areaofstudy = currQuestionnarie.areaofstudy.text;
				questionnarieToPublish.expirience = currQuestionnarie.experience.text;
				questionnarieToPublish.industrydesired = currQuestionnarie.industrydesired.text;
				questionnarieToPublish.abletorelocate = currQuestionnarie.abletorelocate.text==="Yes"?"1":"0";
				questionnarieToPublish.linkedIn = currQuestionnarie.linkedIn.text;
				questionnarieToPublish.photo_url = currQuestionnarie.photo.link;
				questionnarieToPublish.additionalLink = currQuestionnarie.additionalLink.text;
				questionnarieToPublish.video1_title = currQuestionnarie.video1.text;
				questionnarieToPublish.video2_title = currQuestionnarie.video2.text;
				questionnarieToPublish.video3_title = currQuestionnarie.video3.text;
				questionnarieToPublish.video4_title = currQuestionnarie.video4.text;
				
				questionnarieToPublish.video1_url = currQuestionnarie.video1.link;
				questionnarieToPublish.video2_url = currQuestionnarie.video2.link;
				questionnarieToPublish.video3_url = currQuestionnarie.video3.link;
				questionnarieToPublish.video4_url = currQuestionnarie.video4.link;
				
				questionnarieToPublish.video1_thumbnail_url = "";
				questionnarieToPublish.video2_thumbnail_url = "";
				questionnarieToPublish.video3_thumbnail_url = "";
				questionnarieToPublish.video4_thumbnail_url = "";
				
				questionnarieToPublish.video_merged_title = "";
				questionnarieToPublish.video_merged_url = currQuestionnarie.videoMerged.link;
				questionnarieToPublish.video_merged_thumbnail_url = "";
				
				questionnarieToPublish.ispublished = !isBackgroundPublish;
				questionnarieToPublish.anket_id = currQuestionnarie.anket_id;
				if(currQuestionnarie.hasOwnProperty('id'))
				questionnarieToPublish.id = currQuestionnarie.id;
				
				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':questionnarieToPublish},
                    headers: {
					   'skey': systemFactory.md5()
					}
                };
                //console.log(questionnarieToPublish);
                if(!isBackgroundPublish){
	                $state.go('loading');
	            }
                $http(requestData).then(
                    function(result){
                        var questionnarieId = result.data;
                        if(questionnarieId !== -1){
	                        if(!isBackgroundPublish){
		                        currQuestionnarie.id = questionnarieId;
		                        var requestDataMerge = {
									method: 'GET',
				                    url: "http://apevovideo.cloudapp.net/v/service1.svc/ffmpegmerge?resume_id="+questionnarieId
								};
								$http(requestDataMerge);
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnaire was successfully published. Now you can share it by link or send it directly to jobseekers using the ReelHire app',
									btnOneName:"OK"
								}
								popUpManager.popupModal(popupMessage);
		                        currQuestionnarie.ispublished = true;
		                        //alert("Congratulations! Your questionnarie has been published." );
		                        $state.go('questionnaires');
		                    }else{
			                    currQuestionnarie.id = questionnarieId;
			                    $rootScope.currentResumeId = questionnarieId;
			                    currQuestionnarie.ispublished = false;
		                    }
                        }
                    },function(error){
	                    
	                    var popupMessage = {
								header:'Something went wrong...',
								body:'Your questionnarie was not published. Try again.',
								btnOneName:"OK"
							}
							popUpManager.popupModal(popupMessage);
							
	                    
	                }
                );
	            
			},
			publishPhoto:function(){
				return $q(function(resolve, reject) {
					if(getCurrentQuestionnarie().photo.link.search('data') == 0){
						var imageData = getCurrentQuestionnarie().photo.link;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl() + "uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	$window.history.back();
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your photo was not uploaded. Try again.',
									btnOneName:"OK"
								}
								popUpManager.popupModal(popupMessage);
					        	
					        	
								reject(info);
				        	}
				        })
				        .error(function(err){
					        $window.history.back();
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again.',
								btnOneName:"OK"
							}
							popUpManager.popupModal(popupMessage);
					        
					        
				        	reject(err);
				        });
				    }else{
					    resolve(getCurrentQuestionnarie().photo.link);
				    };
				});
			},
    	}; 
		return factory;
	});
})();


