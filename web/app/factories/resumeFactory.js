(function() {
	
    'use strict';

    var appResumeEdit = angular.module('seeMeHireMeApp')	 	
	.factory('resumeFactory', function($rootScope,$http,cookieManager,$sce,systemFactory,$q,popUpManager,$state,$location){
	    var allResume = [];
        var sysAnketID = "0";
        var emptyResume =  {
                 firstname : {text:'',error:false},
				 lastname : {text:'',error:false},
				 email : {text:'',error:false},
				 zipcode : {text:'',error:false},
                 city : '',
				 state : '',
				 education : {text:'',error:false},
				 areaofstudy : {text:'',error:false},
				 experience : {text:'',error:false},
				 industrydesired : {text:'',error:false},
				 abletorelocate : {text:'',error:false},
                 linkedIn : {text:''},
				 additionalLink : {text:''},
                 ispublished: false,
                 photo : {link:'', text:'',error:false},
				 video1 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video2 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video3 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video4 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 videoMerged : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                }
            };
        var myResume = jQuery.extend(true, {}, emptyResume);
        var serverResume = {};

        var currentVideo = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var currentPhoto = {
                name:'',
                link:'',
                html:'',
                error:false
        };
         var restoreResume = function(firstLoad){
                if(!serverResume.ispublished && !firstLoad) return;
                if(!serverResume.hasOwnProperty('id')) return;
                
                var abletorelocate = (serverResume.abletorelocate=="1" || serverResume.abletorelocate==="Yes")?'Yes':'No'; 
                
                myResume.firstname = {text:serverResume.first_name,error:false};
                myResume.lastname = {text:serverResume.last_name,error:false};
                myResume.email = {text:serverResume.email,error:false};
                myResume.zipcode = {text:serverResume.zip,error:false};
                myResume.city = serverResume.city;
                myResume.state = serverResume.state;
                myResume.education = {text:serverResume.education,error:false};
                myResume.areaofstudy = {text:serverResume.areaofstudy,error:false};
                myResume.experience = {text:serverResume.expirience,error:false};
                myResume.industrydesired = {text:serverResume.industrydesired,error:false};
                myResume.abletorelocate = {text:abletorelocate,error:false};
                myResume.linkedIn = {text:serverResume.linkedIn};
                myResume.additionalLink = {text:serverResume.additionalLink};
                myResume.photo = {link:serverResume.photo_url,text:'',error:false};
       
                myResume.video1.link = serverResume.video1_url.replace("watch?v=", "v/");
		        myResume.video2.link = serverResume.video2_url.replace("watch?v=", "v/");
		        myResume.video3.link = serverResume.video3_url.replace("watch?v=", "v/");
		        myResume.video4.link = serverResume.video4_url.replace("watch?v=", "v/");
		        myResume.video1.html = "";
		        myResume.video2.html = "";
		        myResume.video3.html = "";
		        myResume.video4.html = "";
                myResume.videoMerged.link = serverResume.video_merged_url;
                myResume.videoMerged.html = $sce.trustAsHtml('<iframe src="'+serverResume.video_merged_url.replace("https://www.youtube.com/watch?v=", "http://www.youtube.com/embed/")+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  width="100%"  height="100%"></iframe>');
                
                serverResume.myResumeFormat = jQuery.extend(true,{},myResume);            
            };
            var setMyResumeFormatForServerResume = function(){	        
                if(!serverResume.ispublished) return;
                
                var abletorelocate = (serverResume.abletorelocate=="1" || serverResume.abletorelocate==="Yes") ?'Yes':'No';
                var myResumeFormat = {};
                
                myResumeFormat.firstname = {text:serverResume.first_name,error:false};
                myResumeFormat.lastname = {text:serverResume.last_name,error:false};
                myResumeFormat.email = {text:serverResume.email,error:false};
                myResumeFormat.zipcode = {text:serverResume.zip,error:false};
                myResumeFormat.city = serverResume.city;
                myResumeFormat.state = serverResume.state;
                myResumeFormat.education = {text:serverResume.education,error:false};
                myResumeFormat.areaofstudy = {text:serverResume.areaofstudy,error:false};
                myResumeFormat.experience = {text:serverResume.expirience,error:false};
                myResumeFormat.industrydesired = {text:serverResume.industrydesired,error:false};                
                myResumeFormat.abletorelocate = {text:abletorelocate,error:false};
                myResumeFormat.linkedIn = {text:serverResume.linkedIn};
                myResumeFormat.additionalLink = {text:serverResume.additionalLink};
                myResumeFormat.photo = {link:serverResume.photo_url,text:'',error:false};
                myResumeFormat.videoMerged = {};
                myResumeFormat.videoMerged.link = serverResume.video_merged_url;
                myResumeFormat.videoMerged.html = $sce.trustAsHtml('<iframe src="'+serverResume.video_merged_url.replace("watch?v=", "v/")+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  width="100%"  height="100%"></iframe>');
                
                serverResume.myResumeFormat = myResumeFormat;
                
            };
            var isEqualWithServer = function(isPublishButton){
	            var isEqual = true;

                if(Object.keys(serverResume).length < 1 || !serverResume.ispublished){
	                if(isPublishButton){
		                return false;
	                }else{
	                 	return true;
	                }
                }
                
                jQuery.each(serverResume.myResumeFormat, function(key, val) {	                
                    if(val != myResume[key] && val.constructor != {}.constructor){
                        isEqual = false;
                        return false;
                    }
                    if(key.indexOf("video")>-1 && key!="videoMerged") return true;
                    if(val.constructor != {}.constructor)  return true;
                    jQuery.each(val, function(key1, val1) {
	                    
                        if(myResume[key][key1]==null || val1.toString() !== myResume[key][key1].toString()){
                            isEqual = false;
                            return false;
                        }
                    });
                });
                
                return isEqual;
            }
	    return {
		    restoreResume: function(firstLoad){
			    restoreResume(firstLoad);
		    },
		    emptyResume: function(){
			    return jQuery.extend(true, {}, emptyResume);
		    },
            getCurrentVideo: function(){
			    return currentVideo;
		    },
            setCurrentVideo: function(video){
                jQuery.each(currentVideo, function(key, val) {
                    if(video.hasOwnProperty(key)){
                        currentVideo[key] = video[key];
                    }
                });
                myResume["video"+$rootScope.currentVideoIndex].link = video.link;
		    },
            resetResume:function(){
            	myResume = jQuery.extend(true, {}, emptyResume);
            	currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
            	 currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
                serverResume = {};
                allResume = [];
            },
            resetCurrentVideo: function(){
                currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            setCurrentPhoto: function(photo){
                jQuery.each(currentPhoto, function(key, val) {
                    if(photo.hasOwnProperty(key)){
                        currentPhoto[key] = photo[key];
                        myResume.photo.link = photo[key];
                    }
                });
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            myResume: function(){
                return myResume;
            },
            serverResume: function(){
                return serverResume;
            },
            setServerResume: function(_serverResume){
                serverResume = _serverResume;
            },
            setMyResume: function(newResume){
                jQuery.each(myResume, function(key, val) {
                    if(newResume.hasOwnProperty(key)){
                        myResume[key] = newResume[key];
                    }
                });
            },
            allResume: function(){
			    return allResume;
		    },
			
		    getAllResume: function(cookieObject){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'resumes/all',
					headers: {
					   'skey': systemFactory.md5(),
					}
				};  
			    return $http(requestData).then(
				    	function(result){
					    	
					    	allResume = result.data;					    	
							for(var i =0;i<allResume.length;i++)
                            {
	                          	var isPubl = allResume[i].ispublished.toString().toLowerCase()==="true"?true:false;
								allResume[i].ispublished = isPubl;
								// if url starts with "/" then video was filmed on mobile
								if(allResume[i].video1_url.indexOf("/") == 0) allResume[i].video1_url = "";
								if(allResume[i].video2_url.indexOf("/") == 0) allResume[i].video2_url = "";
								if(allResume[i].video3_url.indexOf("/") == 0) allResume[i].video3_url = "";
								if(allResume[i].video4_url.indexOf("/") == 0) allResume[i].video4_url = "";
	                        }  
                            for(var i =0;i<allResume.length;i++)
                            {
                                myResume = jQuery.extend(true, {}, emptyResume);
                                serverResume = {};
                                if(sysAnketID == allResume[i].anket_id){
                                	serverResume = allResume[i];
	                                allResume.splice(i,1);
	                                restoreResume(true);
									return result;
								}
							}						
							return {};


	                    }
					);
			    },
            updateAllResume:function(cookieObject){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'resumes/all',
					headers: {
					   'skey': systemFactory.md5(),
					}
				};  
			    return $http(requestData).then(
				    	function(result){	          
							return allResume;
	                    }
					);
			    },
            checkResumeIsFilled: function(resume){
                var isCorrect = true;

                if(resume === undefined || resume === '')
                {
                    return false;
                }

                jQuery.each(resume, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
                        if(!val.hasOwnProperty('link')){
                            if(!val.text || val.text === undefined || val.text.length<1 || val.error) {
                                isCorrect = false;
                            }
                        }
                        else{
                            if(!val.link || val.link === undefined || val.link.length<1 ){
                                if(key === 'photo'){
                                    isCorrect = false;
                                }else if (val.text==undefined || val.text.length > 1 && (!resume.hasOwnProperty('videoMerged') || resume.videoMerged.link.length<1)){
                                    isCorrect = false;
                                }
                            }
                        }
                    }
                });

                return isCorrect;
            },
            checkRegistration: function(){
	            //console.log($rootScope.userEmail.length);
	            var ifRegistered = $rootScope.userEmail.indexOf("anonimus") != 0 || $rootScope.userEmail.length != 21;
	            if(!ifRegistered){
		            var popupMessage = {
						header:'',
						body:'Please register to have access for the all functionality',
						btnOneName:'Register',
						btnTwoName:'Register Later',
						btnOneClick:function(){
							$state.go('register');
						},
						btnTwoClick:function(){
							
						}
					}

					popUpManager.popupModal(popupMessage);
	            }
	            return ifRegistered;
            },
			checkResume: function(resume){
                var isCorrect = true;

                jQuery.each(resume, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
						if(!val.hasOwnProperty('link')){
	                        if(!val.text || val.text === undefined || val.text.length<1 || val.error) {
	                            val.error = true;
	                            isCorrect = false;
	                        }
	                    }else{
		                    if(!val.link || val.link === undefined || val.link.length<1 ){
			                    if(key === 'photo'){
			                    	val.error = true;
		                            isCorrect = false;
	                            }else if (val.text.length > 1 && (!resume.hasOwnProperty('videoMerged') || resume.videoMerged.link.length<1)){
		                            val.error = true;
		                            isCorrect = false;
		                        }
		                    }
	                    }
                    }
                });

                if(!isCorrect){

	               var popupMessage = {
						header:'',
						body:'Please check fields marked with red',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);

                }
                return isCorrect;
            },
            getSysAnketID:function(){
                
                var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'config',
                    data: {'name':'public_anket'}
                };	
                return $http(requestData).then(
                    function(result){
                        sysAnketID = result.data;
                        return result.data;
                    }
                );
            },
	        getResumeFromUrlId: function()
			{
				urlparams=getUrlVars();	
				
				var resumeDecodedId = $rootScope.hashToId(urlparams[0],'webResume');
					
				var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'resume/'+ resumeDecodedId,
                    headers: {
					   'skey': urlparams[1]
					}
                };	
				return $http(requestData)
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        getExampleData: function()
			{
				urlparams=getUrlVars();			

				return $http.get(systemFactory.serverUrl()+'resume/36',null) 
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        isEqualWithServer: function(isPublishButton){
                return isEqualWithServer(isPublishButton);
            },
            isCurrentVersionPublished:function(){
	            var isCurrentVersionPublished = isEqualWithServer();
	            if(Object.keys(serverResume).length < 1) return false;
	            if(serverResume.ispublished == false || serverResume.ispublished == "false") return false;
                return isCurrentVersionPublished;
            },
            resumeData: function()
			{
				urlparams=getUrlVars();	
				var currentResume = {};
				
				currentResume.first_name = myResume.firstname.text;
				currentResume.last_name = myResume.lastname.text;
				currentResume.email = myResume.email.text;
				currentResume.zip = myResume.zipcode.text;
				currentResume.city = myResume.city;
				currentResume.state = myResume.state;
				currentResume.education = myResume.education.text;
				currentResume.areaofstudy = myResume.areaofstudy.text;
				currentResume.expirience = myResume.experience.text;
				currentResume.industrydesired = myResume.industrydesired.text;
				currentResume.abletorelocate = (myResume.abletorelocate.text=="1" || myResume.abletorelocate.text==="Yes") ?"1":"0";
				currentResume.linkedIn = myResume.linkedIn.text;
				currentResume.additionalLink = myResume.additionalLink.text;
				currentResume.photo_url = myResume.photo.link;
				currentResume.video1_title = myResume.video1.text;
				currentResume.video2_title = myResume.video2.text;
				currentResume.video3_title = myResume.video3.text;
				currentResume.video4_title = myResume.video4.text;				
				currentResume.video1_url = myResume.video1.link;
				currentResume.video2_url = myResume.video2.link;
				currentResume.video3_url = myResume.video3.link;
				currentResume.video4_url = myResume.video4.link;
				
				if(myResume.videoMerged.html.length<1){
					currentResume.video_merged_url = "local";
				}else{
					currentResume.video_merged_url = serverResume.video_merged_url;
				}
				
				currentResume.data = currentResume;
				return currentResume;	        
			},
			publishResume: function (isBackgroundPublish){
				if (typeof(isBackgroundPublish)==='undefined') isBackgroundPublish=false;
				var resumeToPublish = {};
				resumeToPublish.companyInfo = "";
				resumeToPublish.companyLogo = "";
				resumeToPublish.companyName = "";
				resumeToPublish.companyPosition = "";
				resumeToPublish.first_name = myResume.firstname.text;
				resumeToPublish.last_name = myResume.lastname.text;
				resumeToPublish.email = myResume.email.text;
				resumeToPublish.zip = myResume.zipcode.text;
				resumeToPublish.city = myResume.city;
				resumeToPublish.state = myResume.state;
				resumeToPublish.education = myResume.education.text;
				resumeToPublish.areaofstudy = myResume.areaofstudy.text;
				resumeToPublish.expirience = myResume.experience.text;
				resumeToPublish.industrydesired = myResume.industrydesired.text;
				resumeToPublish.abletorelocate = (myResume.abletorelocate.text=="1" || myResume.abletorelocate.text==="Yes")?"1":"0";
				resumeToPublish.linkedIn = myResume.linkedIn.text;
				resumeToPublish.photo_url = myResume.photo.link;
				resumeToPublish.additionalLink = myResume.additionalLink.text;
				resumeToPublish.video1_title = myResume.video1.text;
				resumeToPublish.video2_title = myResume.video2.text;
				resumeToPublish.video3_title = myResume.video3.text;
				resumeToPublish.video4_title = myResume.video4.text;				
				resumeToPublish.video1_url = myResume.video1.link;
				resumeToPublish.video2_url = myResume.video2.link;
				resumeToPublish.video3_url = myResume.video3.link;
				resumeToPublish.video4_url = myResume.video4.link;				
				resumeToPublish.video1_thumbnail_url = "";
				resumeToPublish.video2_thumbnail_url = "";
				resumeToPublish.video3_thumbnail_url = "";
				resumeToPublish.video4_thumbnail_url = "";
				
				resumeToPublish.video_merged_title = "";
				resumeToPublish.video_merged_url = myResume.videoMerged.link;
				resumeToPublish.video_merged_thumbnail_url = "";
				
				resumeToPublish.ispublished = !isBackgroundPublish;
				resumeToPublish.anket_id = sysAnketID;
				if(Object.keys(serverResume).length > 1)
				resumeToPublish.id = serverResume.id;

				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':resumeToPublish},
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };
                
                $http(requestData).then(
                    function(result){
	                    var resumeId = result.data;
                        if(resumeId !== -1){
	                        if(!isBackgroundPublish){
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your resume has been published. It will be available in several minutes via the link that you can find in \"Get Link\" menu',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
		                        serverResume = resumeToPublish;
		                        
		                        restoreResume(true);
		                    }else{
								serverResume = resumeToPublish;
			                    serverResume.id = resumeId;
			                     $rootScope.currentResumeId = resumeId;
			                    resumeToPublish.ispublished = false;
		                    }
                        }
                    },function(error){
	                    
	                     var popupMessage = {
							header:'Something went wrong...',
							body:'You resume was not published. Try again',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
	                    
	                }
                );
	            
			},
			publishPhoto:function(){
				return $q(function(resolve, reject) {
					if(myResume.photo.link.search('data') == 0){
						var imageData = myResume.photo.link;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl()+"uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your photo was not uploaded. Try again',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
					        	
								reject(info);
				        	}
				        })
				        .error(function(err){
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);					        

				        	reject(err);
				        });
				    }else{
					    resolve(myResume.photo.link);
				    };
				});
            },
    	}; 
    
	});
	appResumeEdit.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
	}]);
})();

