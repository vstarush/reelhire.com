(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var recruiterFactory = angular.module('seeMeHireMeApp')
   	.factory('recruiterFactory',function($q,$http,systemFactory,$rootScope,popUpManager,$state){
	   	$rootScope.favorites = [];
        var recruiterFolder = "";
        var listOfAnketsId = [];
        var ankets = [];
		var currentPhoto = {
                name:'companyLogo',
                link:'',
                html:'',
                error:false
        };
        
        
        
    	var client = new XMLHttpRequest();
        var emailTemplate = "";
		client.open('GET', '/app/serverRequests/emailTemplate.html');
		client.onreadystatechange = function() {
		  	emailTemplate = client.responseText;
		}
		client.send();
		
		
		var messageToNotify = function(name,link){
			var message = ""+emailTemplate;
			var startOfCandidateSection = message.indexOf("<br><div style=\"b");
			var lengthOfCandidateSection = message.indexOf('---->')-startOfCandidateSection+5;
			var sectionCandidate = message.slice(message.indexOf("<br><div style=\"b"),message.indexOf('<!---->'));
			message = message.replace(sectionCandidate,"");
			
			message = message.replace("{textTitle}","")
					.replace("{textBody}","Hi"+name+",<br><br>Please take a few moments and complete this brief Video Questionnaire from your phone or laptop.<br><br>http://reelhire.com/"+link+"<br><br>Show us your best, we can’t wait to see you!");
			return message;
		}
		
		var timeconvert = function(ds){
	        var D, dtime, T, tz, off,
	        dobj= ds.match(/(\d+)|([+-])|(\d{4})/g);
	        T= parseInt(dobj[0]);
	        tz= dobj[1];
	        off= dobj[2];
	        if(off){
	            off= (parseInt(off.substring(0, 2), 10)*3600000)+
	(parseInt(off.substring(2), 10)*60000);
	            if(tz== '-') off*= -1;
	        }
	        else off= 0;
	        return new Date(T+= off);
	    };
	    
	    
        var factory = {
	        
	        searchOnServer:function(request,candidates){
		        var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'search',
                    headers: {
					   'skey': systemFactory.md5()
					},
					data: request
                };	
                return $http(requestData).then(
                    function(result){
	                    var dates = [];
	                    for (var i = 0; i < result.data.length; i++) {
						    candidates[i] = jQuery.extend(true, {}, result.data[i]);
						    candidates[i].isNew = false;
						    var tempDate = timeconvert(candidates[i].dateadded);
							candidates[i].abletorelocate = candidates[i].abletorelocate=="1"?"Yes":"No"; 
							candidates[i].date = (tempDate.getMonth() + 1) + '/' + tempDate.getDate() + '/' +  tempDate.getFullYear();
							candidates[i].dateMilisec = candidates[i].dateadded.replace("/Date(", "").replace("+0000)/", "");
							dates[i] = candidates[i].dateMilisec;

						    if($rootScope.currentAnket != undefined && $rootScope.currentAnket != {} && candidates[i].anket_id != systemFactory.systemAnketID()){
							    for (var j = 0; j < $rootScope.currentAnket.notificationList.length; j++) {
								    var foundCandidateForNotification = false;
							    	if($rootScope.currentAnket.notificationList[j].oid === candidates[i].id){
								    	foundCandidateForNotification = true;
								    	candidates[i].isNew = true;
								    	candidates[i].notificationId = $rootScope.currentAnket.notificationList[j].id;
							    	}
							    	if(!foundCandidateForNotification){
								    	console.log($rootScope.currentAnket);
								    	console.log($rootScope.currentAnket.notificationList[j]);
								    	//factory.deleteNotification($rootScope.currentAnket.notificationList[j].id);
							    	}
							    }
						    }
						}
						if(result.data.length == 0){
							if($rootScope.currentAnket != undefined && $rootScope.currentAnket != {} && $rootScope.currentAnket.id!= systemFactory.systemAnketID()){
							    for (var j = 0; j < $rootScope.currentAnket.notificationList.length; j++) {
								    factory.deleteNotification($rootScope.currentAnket.notificationList[j].id);
							    }
						    }
						}
						candidates.splice(result.data.length,candidates.length-result.data.length);
						candidates.sort(function(a, b) {
							return b.dateMilisec - a.dateMilisec});
						return(candidates);
                    }
                );
	        },
	        getListOfFavorites:function(){
		         var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'favorites',
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
                        $rootScope.favorites = result.data;
                    }
                );
	        },
	        setFavorites:function(id,isFavorite){
		         var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'favorites',
                    headers: {
					   'skey': systemFactory.md5()
					},
					data: {'id':id}
                };
                if(!isFavorite){
	                requestData = {
	                    method: 'POST',
	                    url: systemFactory.serverUrl()+'removefavorites',
	                    headers: {
						   'skey': systemFactory.md5()
						},
						data: {'oid':id}
	                };
                }	
                return $http(requestData).then(
	                function(){
	                    if(isFavorite){
		                    if($.inArray(id, $rootScope.favorites) == -1) $rootScope.favorites.push(id);
	                    }else{
		                    if($.inArray(id, $rootScope.favorites) != -1) $rootScope.favorites.splice($.inArray(id, $rootScope.favorites), 1);
	                    }
	                }
                );
	        },
	        getRecruiterFolder: function(){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'folder',
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
	                    if(result.data.length == 0){
		                    var requestData2 = {
			                    method: 'POST',
			                    url: systemFactory.serverUrl()+'folder',
			                    data: {'name':'anketFolder'},
			                    headers: {
								   'skey': systemFactory.md5(),
								}
			                };	
			                return $http(requestData2).then(
			                    function(result){
				                    recruiterFolder = result.data;
				                    return result.data;
				                }
				            );
	                    }else{
                        	recruiterFolder = result.data[0].id;
							return recruiterFolder;
						}
                    }
                );
            },
            getListOfAnkets: function(){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'folder/'+recruiterFolder,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
                        listOfAnketsId = result.data;
                        ankets = [];
                        for(var i=0;i<listOfAnketsId.length;i++){
	                       ankets[i] = {};
	                       ankets[i].id = listOfAnketsId[i].id;
	                       ankets[i].additionalLink = "";
	                       ankets[i].companyInfo = "";
	                       ankets[i].companyLogo = "";
	                       ankets[i].companyName = "";
	                       ankets[i].companyPosition = "";
	                       ankets[i].email = "";
	                       ankets[i].folder_id = "";
	                       ankets[i].ispublished = "";
	                       ankets[i].owner_id = "";
	                       ankets[i].video1_title = "";
	                       ankets[i].video2_title = "";
	                       ankets[i].video3_title = "";
	                       ankets[i].video4_title = "";
	                       ankets[i].notificationList = [];
	                       factory.getAnket(i);
                        }
                        factory.getNotificatoinList();
                        return listOfAnketsId;
                    }
                );
            },
            ankets: function(){
	            return ankets;
            },
            newAnket:function(){
	        	var newAnket = {};
                var maxNumber = 0;
				for(var i=0;i<ankets.length;i++){
					if(Number(ankets[i].id)>=maxNumber){
						maxNumber = Number(ankets[i].id)*1.0000001;
						newAnket.companyLogo = ankets[i].companyLogo;
		                newAnket.companyName = ankets[i].companyName;
					}
				}
				newAnket.id = maxNumber;
                newAnket.additionalLink = "";
                newAnket.companyInfo = "";
                newAnket.companyPosition = "";
                newAnket.email = "";
                newAnket.folder_id = recruiterFolder;
                newAnket.ispublished = "False";
                newAnket.owner_id = $rootScope.userId;
                newAnket.video1_title = "";
                newAnket.video2_title = "";
                newAnket.video3_title = "";
                newAnket.video4_title = "";
                newAnket.notificationList = [];
				ankets.unshift(newAnket);
				return newAnket; 
            },
            getAnket:function(anketIndex){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'anket/'+ankets[anketIndex].id,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
	                    var notificationList = ankets[anketIndex].notificationList;
	                    ankets[anketIndex] = result.data;
	                    ankets[anketIndex].notificationList = notificationList;
	                    
                    }
                );
            },
            copyAnket:function(anket){
	        	var newAnket = jQuery.extend(true, {}, anket);
				newAnket.ispublished = "False";
				newAnket.notificationList = [];
				var maxNumber = 0;
				for(var i=0;i<ankets.length;i++){
					if(Number(ankets[i].id)>=maxNumber) maxNumber = Number(ankets[i].id)*(1.0000001);
				}
				newAnket.id = maxNumber;
				newAnket.companyPosition = anket.companyPosition + " Copy";
				ankets.unshift(newAnket);  
            },
            deleteAnket:function(anket){
	            for(var i=0;i<ankets.length;i++){
		            if(JSON.stringify(ankets[i]) === JSON.stringify(anket) ){
			            factory.deleteAnketFromServer(ankets[i].id);
			            ankets.splice(i, 1);
			            break;
		            }
	            }				
            },
            deleteAnketFromServer:function(anketIndex){ 
                var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'deleteanket',
                    headers: {
					   'skey': systemFactory.md5(),
					},
					data: {'id': anketIndex}
                };	
                return $http(requestData).then(
                    function(result){
// 						console.log(result);
                    }
                );
            },
            deleteAllAnkets:function(){
	            for(var i=0;i<ankets.length;i++){
			        factory.deleteAnketFromServer(ankets[i].id);
	            };
	            ankets = {};				
            },
            setRank: function(id,rank){
	            var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'rank',
                    headers: {
					   'skey': systemFactory.md5(),
					},
					data: {'resume_id': id,'rank':rank}
                };	
                return $http(requestData);
            },
            getNotificatoinList: function(){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'notify',
					headers: {
					   'skey': systemFactory.md5()
					}
				};  
			    $http(requestData).then(
			    	function(result){
				    	var notificationList = result.data;
				    	for(var j=0;j<notificationList.length;j++){
					    	for(var i=0;i<ankets.length;i++){
					    		if(notificationList[j].anket_id === ankets[i].id){
						    		ankets[i].notificationList.push(notificationList[j]);
					    		}
					    	}
					    }
				    	
                	}
                );
            },
            deleteNotification: function(id){
			    if(id == 0) return;
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deletenotify',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
				};  
				 
			    $http(requestData);
			    for(var i=0;i<ankets.length;i++){
				    for(var j=0;j<ankets[i].notificationList.length;j++){
			    		if(ankets[i].notificationList[j].id === id){
				    		ankets[i].notificationList.splice(j,1);
			    		}
			    	}
		    	}
			    //angular.element(document.getElementById('wrapper')).scope().updateNotifications();
		    },
            errorField:function(){
	            var error = {};
	            error.companyInfo = false;
	            error.companyLogo = false;
	            error.companyName = false;
	            error.companyPosition = false;
	            error.video1_title = false;
	            return error;
            },
            checkAnket:function(anket,error){
	            var isCorrect = true;
                if(anket.companyInfo.length < 1){ error.companyInfo = true; isCorrect = false; };
                if(anket.companyLogo.length < 1){ error.companyLogo = true; isCorrect = false; };
                if(anket.companyName.length < 1){ error.companyName = true; isCorrect = false; };
                if(anket.companyPosition.length < 1){ error.companyPosition = true; isCorrect = false; };
                if(!isCorrect){
	                var popupMessage = {
						header:'',
						body:'Please check fields marked with red',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
					return false;
                }
                var oneFieldNotEmplty = false;
                for(var i=1;i<5;i++){
					if(anket["video"+i+"_title"].length > 0) oneFieldNotEmplty = true;
				}
				if(!oneFieldNotEmplty){
					var popupMessage = {
						header:'',
						body:'Please fill out at least one question',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
					error.video1_title = true;
					return false;
				}
                return isCorrect;
            },
            setCurrentPhoto: function(photo){
	            currentPhoto.link = photo.link;
		    },
		    getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'companyLogo',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            publishPhoto:function(photoLink){
	            return $q(function(resolve, reject) {
					if(photoLink != undefined && photoLink.search('data') == 0){
						var imageData = photoLink;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl()+"uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your questionnaire was not published. Try again',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
					        	
					        	//alert("Something went wrong. You resume was not published. Try again.");
								reject(info);
				        	}
				        })
				        .error(function(err){
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);					        

					        //alert("Something went wrong. You resume was not published. Try again.");
				        	reject(err);
				        });
				    }else{
					    resolve(photoLink);
				    };
				});
            },
            sendAnket:function(){
				for(var i=0;i<$rootScope.selectedCandidates.length;i++){
					var request = {};
					request.aid = $rootScope.currentAnket.id;
					request.to_user_id = $rootScope.selectedCandidates[i].user_id;
					var requestData = {
	                    method: 'POST',
	                    url: systemFactory.serverUrl()+'share_anket',
	                    data: request,
	                    headers: {
						   'skey': systemFactory.md5(),
						}
	                };
	                //console.log($rootScope.selectedCandidates);
	                $http(requestData).then(
	                	function(respond){}
	                	);
	                	

                	var request = $http({
				    method: "post",
				    url: "/components/sendgrid-php/myMail.php",
				    data: {
					    sm: systemFactory.mc().id,
					    sp: systemFactory.mc().value,
				        emailTo: $rootScope.selectedCandidates[i].email,
				        emailFrom: $rootScope.userEmail,
				        subject : "Job Application",
						message : messageToNotify($rootScope.selectedCandidates[i].first_name,$rootScope.idToHash($rootScope.currentAnket.id,'webAnket'))
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					request.success(function (dataQQQ) {

					});
	            }
	            $rootScope.selectedCandidates = [];
	            var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnarie was send.',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
            },
            checkRegistration: function(){
	            var ifRegistered = $rootScope.userEmail.indexOf("anonimus") != 0 || $rootScope.userEmail.length != 21;
	            if(!ifRegistered){
		            var popupMessage = {
						header:'',
						body:'Please register to have access for the all functionality',
						btnOneName:'Register',
						btnTwoName:'Register Later',
						btnOneClick:function(){
							$state.go('register');
						},
						btnTwoClick:function(){
							
						}
					}

					popUpManager.popupModal(popupMessage);
	            }
	            return ifRegistered;
            },
            publishAnket:function(anket){
				var anketToPublish = {};
				anketToPublish.companyInfo = anket.companyInfo;
				anketToPublish.companyLogo = anket.companyLogo;
				anketToPublish.companyName = anket.companyName;
				anketToPublish.ispublished = "True";
				anketToPublish.additionalLink = anket.additionalLink;
				anketToPublish.folder_id = anket.folder_id;
				anketToPublish.companyPosition = anket.companyPosition;
				anketToPublish.email = anket.email;
				anketToPublish.name = "";
				anketToPublish.linkedIn = "";
				anketToPublish.questions = [];
				anketToPublish.questions[0] = "q1";
				for(var i=1;i<6;i++){
					anketToPublish["video"+i+"_title"] = "";
				}
				var j = 1;
				for(var i=1;i<5;i++){
					if(anket["video"+i+"_title"].toString().length>0){
						anketToPublish["video"+j+"_title"] = ""+anket["video"+i+"_title"];
						j++;
					}
				}
				
				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'anket',
                    data: anketToPublish,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };
                $http(requestData).then(
                    function(result){
	                    var anketID = result.data;
                        if(anketID != -1){
	                        $rootScope.currentAnket.id = anketID;
	                        if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
// 	                        	console.log(anketID);
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnaire was successfully published. Now you can share it by link or send it directly to job seekers using app.',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
								$state.go("recruiterQuestionnaires");
							}else{
								factory.sendAnket();
							}
								anket.ispublished = "True";
                        }else{
	                        var popupMessage = {
								header:'Something went wrong...',
								body:'Your questionnaire was not published. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
                        }
                    },function(error){
	                    
	                     var popupMessage = {
							header:'Something went wrong...',
							body:'Your questionnaire was not published. Try again',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
	                }
                );
            },
        };
        return factory;
     })
	
})();