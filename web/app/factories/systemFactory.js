(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appResumeEdit = angular.module('seeMeHireMeApp')
   	.factory('systemFactory',function($q,$http,$rootScope,popUpManager){
        var systemAnketID;
        var systemAnket = {};
        var userType = "none";
        var md5 = "";
        var mc = {};
        var welcomeSeekerEmailTemplate = "";
        var welcomeRecruiterEmailTemplate = "";
        
        $rootScope.idToHash = function(id,key){	        
	        var hashids = new Hashids(key, 8,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	        id = Number(id);
	        return hashids.encode(id);
        };
        
        $rootScope.hashToId = function(hash,key){
	        var hashids = new Hashids(key, 8,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	        return hashids.decode(hash);
        };
		
		$rootScope.checkIfMobile = function()
        {
            if( navigator.userAgent.match(/Android/i)
             || navigator.userAgent.match(/webOS/i)
             || navigator.userAgent.match(/iPhone/i)
             || navigator.userAgent.match(/iPad/i)
             || navigator.userAgent.match(/iPod/i)
             || navigator.userAgent.match(/BlackBerry/i)
             || navigator.userAgent.match(/Windows Phone/i)
             ){
                var popupMessage = {
					header:'',
					body:'Please download ReelHire mobile app or use desktop version!',
					btnOneName:"OK",
					btnOneClick:function(){}
				}

				popUpManager.popupModal(popupMessage);
                return true;
            }
            else {
                return false;
            }
        };
        $rootScope.checkIfMobileNoPopup = function()
        {
             if( navigator.userAgent.match(/Android/i)
	             || navigator.userAgent.match(/webOS/i)
	             || navigator.userAgent.match(/iPhone/i)
	             || navigator.userAgent.match(/iPad/i)
	             || navigator.userAgent.match(/iPod/i)
	             || navigator.userAgent.match(/BlackBerry/i)
	             || navigator.userAgent.match(/Windows Phone/i)
             ){
	            $rootScope.isMobile = true;
                return true;
            }
            else {
	            $rootScope.isMobile = false;
                return false;
            }
        };
        
        $rootScope.isWebResume = false;
        
        var loadWelcomeEmailTemplates = function(){
	        var client = new XMLHttpRequest();
			client.open('GET', '/app/serverRequests/welcomeEmailSeeker.html');
			client.onreadystatechange = function() {
			  	welcomeSeekerEmailTemplate = client.responseText;
			}
			client.send();
			
			var client2 = new XMLHttpRequest();
			client2.open('GET', '/app/serverRequests/welcomeEmailRecruiter.html');
			client2.onreadystatechange = function() {
			  	welcomeRecruiterEmailTemplate = client2.responseText;
			}
			client2.send();
        };
        loadWelcomeEmailTemplates();
        
		
        var factory  = {
	        serverUrl: function(){
		        return 'http://apevo.cloudapp.net/service.svc/';
		    },
	        md5: function(){ 
                return md5;
            },
            setMd5: function(_md5){ 
                md5 = _md5;
            },
	        userType: function(){ 
                return userType;
            },
            setUserType: function(typeOfUser){ 
                userType = typeOfUser;
            },		
            systemAnketID: function(){ 
                return systemAnketID;
            },
            getSystemAnketID:function(){
                var requestData = {
                    method: 'POST',
                    url: factory.serverUrl()+'config',
                    data: {'name':'public_anket'}
                };	
                return $http(requestData).then(
                    function(result){
                        systemAnketID = result.data;
                        return result.data;
                    }
                );
            },
            systemAnket: function(){ 
                return systemAnket;
            },
            getSystemAnket:function(){
                var requestData = {
                    method: 'GET',
                    url: factory.serverUrl()+'anket/'+systemAnketID,
                    headers: {'skey':md5}
                };	
                return $http(requestData).then(
                    function(result){
                        systemAnket = result.data;
                        return result.data;
                    }
                );
            },
            sendWelcomeEmail: function(email,userType){
	            var message = "";
	            if(userType === "seeker"){
		            message = welcomeSeekerEmailTemplate;
	            }else{
		            message = welcomeRecruiterEmailTemplate;
	            }
	            
	            
	          	var request = $http({
				    method: "post",
				    url: "/components/sendgrid-php/myMail.php",
				    data: {
					    sm: mc.id,
					    sp: mc.value,
				        emailTo: email,
				        emailFrom: "no-reply@reelhire.com",
				        subject : "Welcome to ReelHire",
						message : message
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					request.success(function (dataQQQ) {
	
					});  
            },
            mc: function(){
	          return mc;  
            },
            getMailConfig:function(){
                var requestData = {
                    method: 'GET',
                    url: factory.serverUrl()+'systemmail/zxc34223145'
                };	
                return $http(requestData).then(
                    function(result){
                        mc = result.data;
                        mc.value = "IlInam17";
                    }
                );
            },
            waitForChangeOnServer: function (url,fieldToLookAt,scope) {
			  	return $q(function(resolve, reject) {				  		
				  	var requestData = {
	                    method: 'GET',
	                    url: factory.serverUrl()+url,
	                    headers: {'skey':md5}
	                };
	                var attempt = 0;
	                var maxAttempt = 50;
					var prevField = "";
					
		            function newRequest(){ 
			            if(scope.isRequestCanceled){
				            reject('0');
				            return;
				        }  	
		                $http(requestData).then(
							function(result){
								var data = result.data;
		                        var newField = data[fieldToLookAt];
		                        if(attempt == 0){
			                        prevField = newField;
			                        setTimeout(function() {
										newRequest();
							    	}, 3000);
		                        }else{
			                        if(prevField === newField){
				                        if(attempt<maxAttempt){
					                        setTimeout(function() {
												newRequest();
									    	}, 3000);
					                    }else{
						                    reject('server doesn\'t have changes');
					                    }
			                        }else{
				                        resolve(newField);
			                        }
		                        }
		                        attempt = attempt + 1;
							}
						);
		            };   
					newRequest();
			  	});
			}
        }
        return factory;
     })
     .directive('myOnChange', function() {
      return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                  var onChangeHandler = scope.$eval(attrs.myOnChange);
                  element.bind('change', onChangeHandler);
            }
        };
    });
     
	
})();