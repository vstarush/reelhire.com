(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appEmails = angular.module('seeMeHireMeApp')	 	
	.factory('emailFactory', function($http,$sce,resumeFactory,systemFactory,$rootScope){
        var candidates = [];
	    return{
		    candidates: function(){
			    if(systemFactory.userType() == "seeker"){
				    var serverResume = resumeFactory.serverResume();
				    candidates = [];
				    candidates[0] = {};
				    candidates[0].photoLink = serverResume.photo_url;
					candidates[0].firstName = serverResume.first_name;
					candidates[0].lastName = serverResume.last_name;
					candidates[0].resumeId = serverResume.id; 
			    }else{
				    candidates = [];
				    for(var i=0;i<$rootScope.selectedCandidates.length;i++){
					    candidates[i] = {};
					    candidates[i].photoLink = $rootScope.selectedCandidates[i].photo_url;
						candidates[i].firstName = $rootScope.selectedCandidates[i].first_name;
						candidates[i].lastName = $rootScope.selectedCandidates[i].last_name;
						candidates[i].resumeId = $rootScope.selectedCandidates[i].id;
						candidates[i].email = $rootScope.selectedCandidates[i].email;  
						
				    }
			    }
			    return candidates;
		    },
		    resetCandidates: function(){
			    candidates = [];
		    },
		    emailTitle: function(){
			    if(systemFactory.userType() == "seeker"){
				    return "";//"Please see my completed Video Resume & Questionnaire";
				}else{
					if($rootScope.emailType == 'emailSelected'){
						return "";//"This is a candidate of interest. Please check out the Video Resume";
					}
					else{
						return "";
					}
				}
				return true;
		    },
            checkEmail: function(scope){
                var isCorrect = true;
                jQuery.each(scope, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
	                    
                        if(!val.text || val.text === undefined || val.text.length<1 ) {
	                        console.log(val);
                            val.error = true;
                            isCorrect = false;   
                        }
                    }
                });
                return isCorrect;
            }
    	}; 
    
	});
})();