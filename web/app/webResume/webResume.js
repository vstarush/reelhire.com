(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider,resumeFactory,questionnarieFactory,$rootScope) {
        $stateProvider
          .state('webResume', {
            url: '/webResume',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:false,
	            showRating:false,
	            showLink:true
            },
            resolve: {
	            prepareData: function(resumeFactory)
				{
					return resumeFactory.getResumeFromUrlId();
				}
            }
          });
      $stateProvider
          .state('publishedQuestionnaire', {
            url: '/publishedQuestionnaire',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:false
            },
            resolve: {
	            prepareData: function(questionnarieFactory)
				{
					return questionnarieFactory.getPublishedQuestionnarieData();
				}
            }
          });
      $stateProvider
      .state('questionnairesPreview', {
        url: '/questionnairesPreview',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:false
            },
        resolve: {
            prepareData: function(questionnarieFactory)
			{
				return questionnarieFactory.getQuestionnairesPreviewData();
			}
        }
      });
      $stateProvider
      .state('resumePreview', {
        url: '/resumePreview',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:true
            },
        resolve: {
            prepareData: function(resumeFactory)
			{
				return resumeFactory.resumeData();
			}
        }
      });
      $stateProvider
      .state('resumeAnswer', {
        url: '/resumeAnswer',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:true,
	            showLink:true
            },
        resolve: {
            prepareData: function($rootScope)
			{
				$rootScope.currentQuestionnarie.data = $rootScope.currentQuestionnarie;
				return $rootScope.currentQuestionnarie;
			}
        }
      });
 	}]);
      
})();
