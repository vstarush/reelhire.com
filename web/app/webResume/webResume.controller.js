'use strict';


var questions=1;
var questionnaire = false;
var urlparams=[];
var videoSource = [];
var mergedVideo = "";

(function() {

    angular.module('seeMeHireMeApp')
        .controller('webResumeCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window','$sce','prepareData','$stateParams','recruiterFactory','resumeFactory', function($rootScope,$scope, $timeout, $state, $window, $sce, prepareData,$stateParams,recruiterFactory,resumeFactory) {
            
//            COPY TO CLIPBOARD
//            new Clipboard('.clipBoardLinkCopy');
		$rootScope.isWebResume = true;
 		$rootScope.checkIfMobileNoPopup();
 		$scope.isMobile = $rootScope.isMobile;
             $scope.jobseekers = prepareData.data; //add json to object
            $scope.linkToCurrentResume = 'http://reelhire.com/'+$rootScope.idToHash($scope.jobseekers.id,'webResume');
            if(!$scope.jobseekers.ispublished){
	            $scope.linkToCurrentResume = "Publish resume to get link";
            }
	    
	    if($scope.jobseekers.rank<1) // Animation check 
	    {
		    	    	    // ----------   Animation for rate stars   ------------ //
	    var stars = document.getElementsByClassName('answerRatingStars')[0].getElementsByClassName('fa-star');
					
				var timeout;
				var ratingAnimation = setInterval(function(){
		            for(var i=stars.length;i>0;i--)
		            {
			            timeout = setTimeout(function(){
				           var temp = i++;
				           $(stars[temp]).animate({color:"white"});
				           $(stars[temp]).animate({color:"rgb(188,196,200)"}); 
			            }, i*200)
		            }  
	            }, 5000);
	            
	            var onStartClick = function(event){    
		            clearInterval(ratingAnimation);
		            clearTimeout(timeout);
		            
		            var index;
		            for(var i = 0;i<stars.length;i++)
		            {
			            if(stars[i]===event.target)
			            {
				            index = i;
				            break;
			            }
		            }
		            
		            for(var i = 0;i<stars.length;i++)
		            {
			           if(i<=index)
			           	$(stars[i]).css("color","rgb(0,176,80)");
			           else
			           	$(stars[i]).css("color","rgb(188,196,200)");
		            }
	            }
	            $(stars).click(onStartClick);
	            $(".invisible-star").click(onStartClick);
	    //---------------------------- animation end -------------//
		
	    }

		$scope.showBackButton = $stateParams.buttonBack;
		$scope.showRating = $stateParams.showRating;
		$scope.showLink = $stateParams.showLink;
		
		$scope.isFavorite = function(){
            return $.inArray($scope.jobseekers.id, $rootScope.favorites) != -1;
        }
        $scope.setFavorite = function(){
            recruiterFactory.setFavorites($scope.jobseekers.id, $.inArray($scope.jobseekers.id, $rootScope.favorites) == -1);
        }
		$scope.newRating = function(rate){
			$scope.jobseekers.rank = rate;
			recruiterFactory.setRank($scope.jobseekers.id,rate);
		}
		$scope.goBack = function(){
            $window.history.back();
        };
        
        $scope.openLinkedIn =  function(){
	        if(!$scope.jobseekers.linkedIn) return;
	        if($rootScope.isMobile){
				window.open("uniwebview://web?link=" + $scope.jobseekers.linkedIn, '_blank');
            }else{
	            window.open(addLink($scope.jobseekers.linkedIn), '_blank');
            }
        }
        $scope.openAdditionalLink =  function(){
	        if(!$scope.jobseekers.additionalLink) return;
	        if($rootScope.isMobile){
	            window.open("uniwebview://web?link=" + $scope.jobseekers.additionalLink, '_blank');
            }else{
	            window.open(addLink($scope.jobseekers.additionalLink), '_blank');
            }
        }
		
		var personName = $scope.jobseekers.first_name + ' ' + $scope.jobseekers.last_name;//connect name
		
		checkLength(personName);//add name
		
		$scope.jobseekers.location = $scope.jobseekers.city+", "+$scope.jobseekers.state;
		if($scope.jobseekers.zip == "00000"){
			$scope.jobseekers.location = "Non US";
		}
		
		var videoId = $sce.trustAsResourceUrl($scope.jobseekers.video_merged_url).toString().split("?v=");
		
		$scope.videoAnswer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1');//video answer
		
		if($scope.jobseekers.video_merged_url === "local"){
			mergedVideo = "";
			videoSource[0] = $scope.jobseekers.video1_url.replace("watch?v=", "v/");
			videoSource[1] = $scope.jobseekers.video2_url.replace("watch?v=", "v/");
			videoSource[2] = $scope.jobseekers.video3_url.replace("watch?v=", "v/");
			videoSource[3] = $scope.jobseekers.video4_url.replace("watch?v=", "v/");
			var videoId = videoSource[0].split("v/");

			$scope.videoAnswer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1');
		}else{
			mergedVideo = 'https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1';
		}
		
		
		
		questions = 1;
		
		if($scope.jobseekers.video2_title.length>0)
			questions=2;
			
		if($scope.jobseekers.video3_title.length>0)
			questions=3;
		
		if($scope.jobseekers.video4_title.length>0)
			questions=4;
				
		numberOfAnswers(questions);//see if there are all video answers
			
		questionnaire = false;
		if($scope.jobseekers.companyLogo&&$scope.jobseekers.companyName&&$scope.jobseekers.companyPosition&&$scope.jobseekers.companyInfo)
			questionnaire = true;
					
		headerType(questionnaire);//if resume or questionnaire
		
		var atrl = 'Yes';
		
		if($scope.jobseekers.abletorelocate==0)
			atrl = 'No';
			
		$scope.abletorelocate = atrl;// able to relocate

		
		urlparams = undefined;

		if(player != undefined) onYouTubeIframeAPIReady();
		
		$scope.openApp = function(){
			if($rootScope.isMobile){
				window.open("http://reelhire.com/"+$rootScope.idToHash($scope.jobseekers.id,'webResume'));
// 				 window.open("uniwebview://web?link=" + "reelhire://?resume="+$scope.jobseekers.id, '_blank');
			}
			else{
				$rootScope.outsideResumeId = $scope.jobseekers.id;
				$state.go('login');
			}
		}
		$scope.sendEmail = function(){
	           $window.location = "mailto:" + $scope.jobseekers.email + "?" +
			"subject=I%20found%20your%20resume%20on%20ReelHire.com&body=Email%20Body"; 
		}
		
		$rootScope.$on('$stateChangeStart', function(){ 
		    $rootScope.isWebResume = false;
		})
    }]);
})();

function checkLength(a){
	
	var seekername = a;
	var nameLength = seekername.length;
	var standard = 30;
	var minusfont = 0;
	var nameFont = 0;
	var namepadding= 6;

	if(nameLength>standard)
	{
		for(var i = 1;i<=100;i++)
		{
			minusfont+=2;
			standard+=10;
			if(standard>nameLength)
			{
				nameFont = 30 - minusfont;
				namepadding+=minusfont;
				$("#name").append('<p class="boldFont">' + seekername + '</p>' );
				$("#name p").css({'font-size':nameFont});
				
				break;
			}
		}
	}
	else
	{
		$("#name").append('<p class="boldFont">' + seekername + '</p>' );
		$("#name p").css({'font-size':standard});
		
	}
	

}
function onStateChangeTest(){
// 	console.log("onStateChangeTest");
}
function numberOfAnswers(a){
	if(a==3)
	{
		$("#q4").hide();
	}
	else if(a==2)
	{
		$("#q3").hide();
		$("#q4").hide();
	}
	else if(a==1)
	{
		$("#q2").hide();
		$("#q3").hide();
		$("#q4").hide();
	}
}

function headerType(a){
	if(a)
		$("#resumeTop").remove();
	else
		$("#questionnaireTop").remove();
}

function addLink(teststring){
	if(!teststring) return "";
	var patternHttp = /^http(s)?:\/\//;
	var patternWww = /www./;
	var template = 'http://www.';
	
	if(patternHttp.test(teststring) && patternWww.test(teststring))
	{
		return teststring;
	}
	else if(patternHttp.test(teststring))
	{
		return teststring.replace(patternHttp, template);
	}
	else if(patternWww.test(teststring))
	{
		return teststring.replace(patternWww, template);
	}
	else
	{
		return teststring = template + teststring;
	}

}


function getUrlVars()
{
    //console.log('inside');

    var vars = [];
    var hash = '';
    var hashes = window.location.href.slice(window.location.href.indexOf('?')+1);
    
    vars = hashes.split('&');
    
    if(vars[1])
    {
		vars[1] = vars[1].slice(vars[1].indexOf('=')+1);
	}
    return vars;

}
var player;
var videoNumber = 1;
function onPlayerStateChange(event) {
	
    switch(event.data) {
      case 0:
      	if(mergedVideo.length > 0){
	      	player.cueVideoByUrl(mergedVideo, 0,"large");
		  	player.playVideo();
      	}else{
	      	videoNumber = videoNumber + 1;
	      	if(videoNumber > questions) videoNumber = 1;
	      	player.cueVideoByUrl(videoSource[videoNumber-1]+ '?enablejsapi=1', 0,"large");
	      	player.playVideo();
        }
        break;
    }
}
function onYouTubeIframeAPIReady() {
	player = new YT.Player( 'videoAnswer', {
	  	events: { 'onStateChange': onPlayerStateChange }
	});
}
