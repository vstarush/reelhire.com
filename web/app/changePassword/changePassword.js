(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('changePassword', {
            url: '/changePassword',
            templateUrl: 'app/changePassword/changePassword.html',
            controller: 'ChangePasswordCtrl'
          });
      }]);
      
})();
