(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('ChangePasswordCtrl', ['$scope',  '$state', 'resourceLogin','$window','resourceRegister','popUpManager', function($scope, $state, resourceLogin,$window,resourceRegister,popUpManager) {
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
             
                $scope.email = {
                    text:'',
                    error : false
                };
                $scope.oldPassword = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
                $scope.passwordConfirm = {
                    text:'',
                    error : false
                };
                $scope.goBack = function(){
	                 $window.history.back();
                };
                $scope.checkEmail = function(){
	                if(!$scope.email.text){
                        $scope.email.error = true;
                    }
                }
                 $scope.changePwd = function(){
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        var popupMessage = {
							header:'',
							body:'Please check your email',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                       
                        return;
                    }
                    if($scope.password.text.length<5){
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        $scope.password.error = true;
                        $scope.passwordConfirm.error = true;
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    if($scope.password.text != $scope.passwordConfirm.text){
                        $scope.passwordConfirm.error = true;
                        $scope.password.error = true;
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        var popupMessage = {
							header:'',
							body:'Please check your password',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    
                    resourceRegister.changePassword($scope.email.text,$scope.oldPassword.text,$scope.password.text).then(
	                    function(respond){
		                    var popupMessage = {
								header:'',
								body:'Your password was successfuly changed!',
								btnOneName:'OK'
							}
		
							popUpManager.popupModal(popupMessage);
							
		                    $state.go("profile");
	                    },
	                    function(err){
		                    //alert(err);
		                    $window.history.back();
	                    }
                    );
                    
                }
        }]
    );

    
})();
