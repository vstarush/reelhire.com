(function() {
	
    'use strict';
    
    angular.element(document).ready([function() {
    	angular.bootstrap(document, ['seeMeHireMeApp']);
    }]);
    
    var appApp = angular.module(
        'seeMeHireMeApp', [
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ui.router',
            'ui.bootstrap',
            'angularMoment',
            'ngAnimate',
            'ngStorage'
        ]
    )

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$urlMatcherFactoryProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $urlMatcherFactoryProvider) {
        $urlRouterProvider
        	.otherwise('/');

        $stateProvider
          	.state(
            'checkpoint', {
                url: '/',
                controller: ['$state', function( $state ) {
                    var defaultState = 'main';
                        $state.transitionTo(defaultState);
                }]
            }
        );

        $urlMatcherFactoryProvider.strictMode(false);
        $locationProvider.html5Mode(true);
    }])

    .controller('AppCtrl', ['$rootScope','$scope','$state','resourceLogin','$location','popUpManager', function($rootScope, $scope, $state,resourceLogin,$location,popUpManager){
        
	   
	    var urlParamsHandler = function(){ 
		    var urlParams = $location.path().replace('/','').split('/');
	        if(urlParams[0] == "resetPwd"){
				$rootScope.resetEmail = urlParams[1];
				$rootScope.resetCode = urlParams[2];
			}
	        if($rootScope.hashToId(urlParams[0],'webResume').length > 0){
			    $rootScope.outSideResumeHash = urlParams[0];
			    $rootScope.outSideResumeMd5 = urlParams[1];
			    $scope.ifWebResume = true;
		    }else if($rootScope.hashToId(urlParams[0],'webAnket').length > 0){
			    $rootScope.outSideAnketId = $rootScope.hashToId(urlParams[0],'webAnket');
		    }
	        
        	setTimeout(function () {
	        	if($rootScope.resetEmail != undefined && $rootScope.resetEmail != ""){
					$state.go("resetPwd");
				}
			},300);
            
            if($rootScope.outSideResumeHash != undefined && $rootScope.outSideResumeHash!=""){
            	$location.path('/webResume').search($rootScope.outSideResumeHash+'&'+$rootScope.outSideResumeMd5);
				$state.go("webResume");
			    $rootScope.outSideResumeHash = "";
			    $scope.ifWebResume = true;
			}
            if(urlParams === 'help'){
	            setTimeout(function(){
	            	$state.go("help");
	            },0);
            }
			if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId!=""){
				if( $rootScope.checkIfMobile()){
					$window.location.href = "reelhire://?webanket="+$rootScope.outSideAnketId;
					$rootScope.outSideAnketId = "";
				}else{
					setTimeout(function(){
                        $state.go("login");
                    },0);
				}
			}
		}(); 
        
        
        
        $scope.ifWebResume = function()
        {
            return $rootScope.isWebResume;
        };
        
	    
        $scope.checkIfMobile = function()
        {
            return $rootScope.checkIfMobileNoPopup();
        };
        $scope.ifMobile = $rootScope.checkIfMobileNoPopup();   
	    
	    $scope.$watch('userType', function(newValue){
		    $scope.type = newValue;
	    });
	    
    }]);
	
	appApp.service('cookieManager', function($cookies){
		
		this.getCookies = function(sObj){
                if(!$cookies.get(sObj)) return '{}';
				return $cookies.get(sObj);
			},
		this.putCookies = function(sObj, data){
				$cookies.put(sObj, data);
			},
        this.clearCookies = function(sObj){
				$cookies.remove(sObj);
			},
		this.systemResumeObject = function(){
				return 'publicResumeObject';
			}
		
	});


})();