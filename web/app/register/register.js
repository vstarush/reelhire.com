(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('register', {
                        url: '/register',
                        templateUrl: 'app/register/register.html',
                        controller: 'registerCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
            }
        ]);
})();

