(function() {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    .factory('resourceRegister', function($resource,$http,$state,systemFactory,resourceLogin,$q,popUpManager,$rootScope){
	    var updateUser = function(email,pwd,userType){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'resetcode/'+$rootScope.userEmail,
			}; 
            return $http(requestData).then(function(responce){
                var requestData3 = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'user/update',
					data: {
                        'email':email,
                        'pwd':pwd,
                        'md5':responce.data,
                        'name': email,
                        'use_type': $rootScope.userType
                    },
                    headers: {
					   'skey': systemFactory.md5()
					},
				}; 

                return $http(requestData3).then(function(responce3){
                   if(responce3 != undefined){
                       resourceLogin.loginWithMd5(responce3.data);
                   }else{
                       reject('Sorry :( Something went wrong. Please try again');
                   }
                   
               });
	    	});
	    };	
	    return{
			    RegisterSocial: function(){
				   this.RegisterWithPassword(resourceLogin.currentEmail(),resourceLogin.currentPwd(),$rootScope.userType,$rootScope.userType);
			    },
			 	RegisterWithPassword: function(email,pwd,userType,name){
				 	if($rootScope.userEmail.indexOf("anonimus") == 0){
					 	updateUser(email,pwd,userType);
					}else{
					    var requestData = {
						    method: 'POST',
							url: systemFactory.serverUrl()+'user',
							data: {
	                            'email':email,
	                            'pwd':pwd,
	                            'user_type':userType
	                        },
	                        headers: {
							   'skey': 123
							}
						};
						if(name != undefined){
							requestData.data.name = name;
						} 
					    return $http(requestData).then(function(responce){
						    if(responce.data == '-1'){
							    
							    var popupMessage = {
									header:'',
									body:'The user is already registered',
									btnOneName:"OK"
								}
				
								popUpManager.popupModal(popupMessage);
							    
	                            gapi.auth.signOut();
	                            var auth2 = gapi.auth2.getAuthInstance();
	                            auth2.signOut();
								$state.go('register');
							}else{
                                $rootScope.firstLoginType = userType;
	                            resourceLogin.loginWithMd5(responce.data);
	                            systemFactory.sendWelcomeEmail(email,userType);
							}
						});
					}
				},
				changePasswordCode:function(email,code,newPwd){
					$state.go('loading');
					return $q(function(resolve, reject) {
                            var requestData3 = {
							    method: 'Post',
								url: systemFactory.serverUrl()+'user/update',
								data: {
		                            'email':email,
		                            'name':newPwd,
		                            'pwd':newPwd,
		                            'md5': code
		                        }
		                        
							}; 
                            return $http(requestData3).then(function(responce3){
	                           if(responce3 != undefined){
		                           resolve(responce3.data);
	                           }else{
		                           reject('Sorry :( Something went wrong. Please try again');
	                           }
	                           
                           });

					});
				},
				changePassword:function(email,oldPwd,newPwd){
					$state.go('loading');
					return $q(function(resolve, reject) {
					    var requestData = {
						    method: 'GET',
							url: systemFactory.serverUrl()+'user?email='+email+'&pwd='+oldPwd,
						}; 
					    return $http(requestData).then(function(responce){
						    
						    if(responce.data.md5 != undefined){
	                            var requestData2 = {
								    method: 'GET',
									url: systemFactory.serverUrl()+'resetcode/'+email,
								}; 
	                           return $http(requestData2).then(function(responce2){
		                            var requestData3 = {
									    method: 'Post',
										url: systemFactory.serverUrl()+'user/update',
										data: {
				                            'email':email,
				                            'name':newPwd,
				                            'pwd':newPwd,
				                            'md5':responce2.data
				                        }
									}; 
		                            return $http(requestData3).then(function(responce3){
			                           if(responce3 != undefined){
				                           resolve(responce3.data);
				                           systemFactory.setMd5(responce3.data);
			                           }else{
				                           reject('Sorry :( Something went wrong. Please try again');
			                           }
			                           
		                           });
	                           });
							}else{
	                            reject("Wrong login data");
							}
						});
							
					});
				},
				
		    
		    publishPhoto:function(){
				$state.go('loading');
				return $q(function(resolve, reject) {
				        return $http.post(systemFactory.serverUrl() + "uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	$window.history.back();
					        	alert("Something went wrong. Your photo was not uploaded. Try again.");
								reject(info);
				        	}
				        })
				        .error(function(err){
					        $window.history.back();
					        alert("Something went wrong. Your photo was not uploaded. Try again.");
				        	reject(err);
				        });
				    
				});
			},
		    
		    
		    
		    
		    
		    }; 
	    
    	})
        .controller('registerCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceRegister', '$resource','cookieManager','resourceLogin','popUpManager','$window',
            function($scope, $state, $rootScope, $cookies, resourceRegister, $resource, cookieManager,resourceLogin,popUpManager,$window) {
                if($rootScope.checkIfMobileNoPopup()){
	                $state.go('main');
	                return;
                }
                $scope.userType = 'undefinite';
				$scope.selectTypeBtnActive = true;
                $scope.funcSeeker = function(){
                    $scope.userType = 'seeker';
                }
                $scope.funcRecruiter = function(){
                    $scope.userType = 'recruter';
                }
                if($rootScope.userEmail.indexOf("anonimus") == 0){
	                if($rootScope.userType == "seeker"){
		                $scope.funcSeeker();
	                }else{
		                $scope.funcRecruiter(); 
	                }
	                $scope.selectTypeBtnActive = false;
                }
                $scope.goBack = function(){
		            $window.history.back();
	            };
                
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
             
                $scope.email = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
                $scope.passwordConfirm = {
                    text:'',
                    error : false
                };
                
                $scope.funcRegister = function(){
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Please check your email',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.password.text.length<5){
                        $scope.password.text = '';
                        $scope.password.error = true;
                        $scope.passwordConfirm.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.password.text != $scope.passwordConfirm.text){
                        $scope.passwordConfirm.error = true;
                        $scope.password.error = true;
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        
                        var popupMessage = {
							header:'',
							body:'Please, check your password',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.userType == 'undefinite'){
	                    
	                    var popupMessage = {
							header:'',
							body:'Please, choose who you are',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    resourceRegister.RegisterWithPassword($scope.email.text,$scope.password.text,$scope.userType)
                    
                }
                $scope.fbAsyncInit = false;

                function statusChangeCallback(response) {
                    if (response.status === 'connected') {
                        if($scope.userType == 'undefinite'){
	                        
	                        var popupMessage = {
								header:'',
								body:'Please, choose who you are',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
                            return;
                        }
                        resourceRegister.RegisterWithPassword(
                            response.authResponse.userID,
                            response.authResponse.userID,
                            $scope.userType
                        );
                    } else if (response.status === 'not_authorized') {
//                        console.log('Not Authorized');
                    } else {
//                        console.log('Not Logged-in');
                    }
                }
                
                $scope.fblogin = function(){
						FB.login(function(response) {
						 	window.checkLoginState(response);
						}, {scope: 'public_profile,email'});
					}

                window.checkLoginState = function(){
                    FB.getLoginStatus(function(response) {
                        statusChangeCallback(response);
                    });
                };

                window.fbAsyncInit = function() {
                    if ($scope.fbAsyncInit || window.FB === undefined) 
                        return;

                    FB.init({
                        appId      : '461446334062271',
                        cookie     : true, 
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    $scope.fbAsyncInit = true;
                }();

                
                $scope.options = {
	                'width':175,
	                'longtitle': true,
	                'theme': 'dark',
                    'onsuccess': function(response) {
                      onSignUpGoogle(response.getBasicProfile().getEmail());
                    }
                  }
                function onSignUpGoogle(googleEmail) {
                    var pwdGoogle = resourceLogin.md5Pwd(googleEmail); 
                    if($scope.userType == 'undefinite'){
	                    
	                    var popupMessage = {
							header:'',
							body:'Please choose who you are',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        gapi.auth.signOut();
                        var auth2 = gapi.auth2.getAuthInstance();
                        auth2.signOut();
                        return;
                    }
                    resourceRegister.RegisterWithPassword(googleEmail+"_g",pwdGoogle,$scope.userType);
                }
            }
	       
	    ])
        .directive('googleSignUpButton', function() {
            return {
              scope: {
                buttonId: '@',
                options: '&'
              },
              template: '<div></div>',
              link: function(scope, element, attrs) {
                var div = element.find('div')[0];
                div.id = attrs.buttonId;
                gapi.signin2.render(div.id, scope.options()); //render a google button, first argument is an id, second options
              }
            };
          });
})();