(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('InsideQuestionnaireCtrl', ['$http','$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','$stateParams','$filter','$window','systemFactory',
        function($http,$scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,$stateParams,$filter,$window,systemFactory) {
	        
	        // Filtered Candidates
			var currentFilteredCandidates = [];
			$rootScope.selectedCandidates = [];
            
            $scope.goBack = function(){
	            $window.history.back();
            };
			
            $scope.isChecked = false;
            $scope.previewAnket = function(){
	            $state.go("previewQuestionnaire");
            }
            $scope.candidateClick = function(candidate){
	            $rootScope.currentQuestionnarie = candidate;
	            candidate.isNew = false;
	            recruiterFactory.deleteNotification(candidate.notificationId);
	            $state.go("resumeAnswer");
            }
            $scope.candidates = [];
            $scope.linkToAnket = "";        
            
            $scope.explanaitionPopupCheck = function(){
                if($scope.candidates.length<1)
                    return true;
                return false;
            }
            
                
            var request = {};
            if($stateParams.typeOfList == undefined){
	            request = {
		            'anket_id':$rootScope.currentAnket.id
	            };
	            $scope.showPreviewQuestionnaire = true;
		        $scope.linkToAnket = "http://reelhire.com/"+$rootScope.idToHash($rootScope.currentAnket.id,'webAnket');
            }else if($stateParams.typeOfList == 'searchAll'){
	            request = {
		            'anket_id': systemFactory.systemAnketID()
	            };
	            
	            $scope.showPreviewQuestionnaire = false;
            }else if($stateParams.typeOfList == 'favorites'){
	            request = {
		           
	            };
	            $scope.showPreviewQuestionnaire = false;
            }
            
            recruiterFactory.searchOnServer(request,$scope.candidates);
            recruiterFactory.getListOfFavorites();
            
            $scope.activeFilters = [];
            $scope.sortOptions = ["Experience from less to more","Experience from more to less","Date of Questionnaire from new to old","Date of Questionnaire from old to new","Rating from small to big","Rating from big to small"];
            $scope.filters = {};
            $scope.filters.categories = ["Select Filter Category","Industry Desired","Experience","Education","Area of Study","Able to Relocate","Location"];
            $scope.filters._categories = ["industrydesired","expirience","education","areaofstudy","abletorelocate","location"];
            $scope.filters.filters = [];
            $scope.filterBy = {};
            $scope.filterBy.category = $scope.filters.categories[0];
		    $scope.filterBy.filter = "";
		    $scope.emptyFilter = "";
		    $scope.sortBy = {};
		    
            $scope.onSortChanged = function(){
// 				console.log($scope.sortBy.sort);
				
            }

			

            $scope.sortedFunction = function(candidate){
	            if($scope.sortBy.sort === $scope.sortOptions[0]){
		            return $scope.experiences.indexOf(candidate.expirience);
	            }else if($scope.sortBy.sort === $scope.sortOptions[1]){
		            return -$scope.experiences.indexOf(candidate.dateadded);
	            }else if($scope.sortBy.sort === $scope.sortOptions[2]){
		            return -candidate.dateadded.replace("/Date(", "").replace("+0000)/", "");
	            }else if($scope.sortBy.sort === $scope.sortOptions[3]){
		            return candidate.dateadded.replace("/Date(", "").replace("+0000)/", "");   
	            }else if($scope.sortBy.sort === $scope.sortOptions[4]){
		            return candidate.rank;
	            }else if($scope.sortBy.sort === $scope.sortOptions[5]){
		            return -candidate.rank;
	            }
            }
            $scope.isFavorite = function(candidate){
	            return $.inArray(candidate.id, $rootScope.favorites) != -1;
            }
            $scope.setFavorite = function(candidate){
	            recruiterFactory.setFavorites(candidate.id, $.inArray(candidate.id, $rootScope.favorites) == -1);
            }
            
            $scope.countSelectedCandidates = function(){
	           
	            var selectedCandidates = [];
	            for(var i=0;i<$scope.candidates.length;i++){
			       if($scope.candidates[i].isSelected){
				       selectedCandidates.push($scope.candidates[i]);
			       }
		        }
		        $rootScope.selectedCandidates = selectedCandidates;
				return selectedCandidates.length>0;
            }
            
            
            
            
            //function rewriting to selected candidates object
			$scope.filteredCandidatesFn = function(filteredCandidates)
			{
			   if(!_.isEqual(currentFilteredCandidates, filteredCandidates))
			   {
			       currentFilteredCandidates = [];
			       _.extend(currentFilteredCandidates,filteredCandidates);
			   }
			   
			   return true;
			}
            
            // function to select/deselect all filtered candidates
			$rootScope.selectAllCandidates = function(){
			   if($rootScope.selectAllText === 'Select All')
			   {
			       for(var i = 0;i<currentFilteredCandidates.length;i++)
			       {
			           currentFilteredCandidates[i].isSelected = true;
			       }
			       _.extend($rootScope.selectedCandidates,currentFilteredCandidates); 
			   }
			   
			   if ($rootScope.selectAllText === 'Deselect All')
			   {
			       for(var i = 0;i<currentFilteredCandidates.length;i++)
			       {
			           currentFilteredCandidates[i].isSelected = false;
			       }
			       $rootScope.selectedCandidates = [];
			   }
			
			}
            
            $scope.customFilter = function(candidate){
	            if($stateParams.typeOfList == 'favorites'){
			        if(!$scope.isFavorite(candidate)) return false;
		        }
	            if($scope.activeFilters.length < 1) return true;
	            
	            var candidateReq = [];
	            for(var j=0;j<$scope.filters._categories.length;j++){
			        candidateReq[j] = 1;
		        }
		        
	            for(var i=0;i<$scope.activeFilters.length;i++){
		            for(var j=0;j<$scope.filters._categories.length;j++){
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && candidateReq[j]==1){
				            candidateReq[j] = 0;
			            }
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && $scope.activeFilters[i].filter === candidate[$scope.activeFilters[i]._category] && $scope.filters._categories[j] != "location"){
				            candidateReq[j] = 2;
			            }
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && $scope.filters._categories[j] == "location"){
				            if(($scope.activeFilters[i].filter == $scope.locationOptions[0] && candidate["zip"] != "00000") || 
				            ($scope.activeFilters[i].filter == $scope.locationOptions[1] && candidate["zip"] == "00000") ){
				            	candidateReq[j] = 2;
				            }
			            }
			        }
	            }
	            
				var intFilter = 1;
				for(var j=0;j<$scope.filters._categories.length;j++){
			        intFilter *= candidateReq[j];
		        }
		        
	            return intFilter>0;
            }
            $scope.checkForOutSideResume = function(candidate){
		        if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != "" && $rootScope.outsideResumeId != candidate.id){
					return false;
				}
				return true;
            }
            $scope.$on('$destroy', function() {
			  	$rootScope.outsideResumeId = "";
			})
            $scope.isSelectSecondFilter = false;
            $scope.onCategoryChange = function(){
	            
	            $scope.isSelectSecondFilter = true;
	            $scope.emptyFilter = "Select Filter";
	            
	            if($scope.filterBy.category === $scope.filters.categories[1]){
		            $scope.filters.filters = $scope.professions;
		            $scope.filterBy._category = $scope.filters._categories[0];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[2]){
		            $scope.filters.filters = $scope.experiences;
		            $scope.filterBy._category = $scope.filters._categories[1];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[3]){
		            $scope.filters.filters = $scope.educations;
		            $scope.filterBy._category = $scope.filters._categories[2];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[4]){
		            $scope.filters.filters = $scope.professions;
		            $scope.filterBy._category = $scope.filters._categories[3];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[5]){
		            $scope.filters.filters = $scope.abletorelocateOptions;
		            $scope.filterBy._category = $scope.filters._categories[4];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[6]){
		            $scope.filters.filters = $scope.locationOptions;
		            $scope.filterBy._category = $scope.filters._categories[5];
	            }
	            else{
		            $scope.emptyFilter = "";
		            $scope.filterBy.filter = "";
		            $scope.isSelectSecondFilter = false;
	            }
            }
            
            $scope.onFilterChange = function(){
	            if($scope.filterBy.filter != $scope.emptyFilter && $scope.filterBy.filter != null){
		            $scope.newFilter();
		            $scope.isSelectSecondFilter = false;
		            $scope.filterBy.category = $scope.filters.categories[0];
		            $scope.filters.filters= [];
		            $scope.emptyFilter = "";
		        }
	        }
	        
	        $scope.newFilter = function(){
		        var activeFilter = {};
		        activeFilter._category = ""+$scope.filterBy._category;
		        activeFilter.category = ""+$scope.filterBy.category;
		        activeFilter.filter = ""+$scope.filterBy.filter;
		        
		        $scope.activeFilters.unshift(activeFilter);
	        }
	        
	        $scope.deleteFilter = function(filter){
		         for(var i=0;i<$scope.activeFilters.length;i++){
		            if(JSON.stringify($scope.activeFilters[i]) === JSON.stringify(filter) ){
			            $scope.activeFilters.splice(i, 1);
			            break;
		            }
	            }		
	        }
	        
            $http.get('app/resources/professions.json').success(function(data) {
                $scope.professions = data;
            });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No']; 
            $scope.locationOptions = ['US Candidates','Non US Candidates'];
            
            $scope.open = function(){
				window.open($scope.linkToAnket);
			};   
        }]);
})();