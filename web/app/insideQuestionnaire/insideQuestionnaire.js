(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('insideQuestionnaire', {
                        url: '/insideQuestionnaire',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
                $stateProvider
                    .state('searchAll', {
                        url: '/searchAll',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        params:{
				            typeOfList: 'searchAll',
			            },
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                       
	                        }
                        }
                    });    
                $stateProvider
                    .state('favorites', {
                        url: '/favorites',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        params:{
				            typeOfList: 'favorites',
			            },
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                       
	                        }
                        }
                    });    
            }
        ]);
})();