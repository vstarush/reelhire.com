(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('resumeEdit', {
            url: '/resume/edit',
            templateUrl: 'app/resumeEdit/resumeEdit.html',
            controller: 'ResumeEditCtrl'
          });
      }]);
      
})();
