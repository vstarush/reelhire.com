var cookiePublicResume = 'publicResumeObject';
 
 (function(win) {

    'use strict';

    angular.module('seeMeHireMeApp')
           .controller('ResumeEditCtrl', ['$scope', '$timeout', '$state', '$window', '$sce','cookieManager','resumeFactory','systemFactory','$http','videoUploadFactory','photoUploadFactory','popUpManager','$rootScope','questionnarieFactory', function($scope, $timeout, $state, $window, $sce, cookieManager,resumeFactory,systemFactory,$http,videoUploadFactory,photoUploadFactory,popUpManager,$rootScope,questionnarieFactory) {
	        
               $scope.$watch('firstLoginType',function(newValue,oldValue){
                   
                   if(newValue!== null && newValue != undefined)
                        {
                            popUpManager.welcomeJobSeeker();
                        }
               });
               
               
	        videoUploadFactory.setCurrentFactory(resumeFactory);
	        photoUploadFactory.setCurrentFactory(resumeFactory);

	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
            
			var allResumes = resumeFactory.allResume();
			var systemAnketId = systemFactory.systemAnketID();
            $scope.serverResume = resumeFactory.serverResume();
            $scope.resume = resumeFactory.myResume();
            $scope.isNoneUS = {};
			
            $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>0;
            }
            $scope.checkZip = function(){
                if($scope.zipCodeBase.hasOwnProperty($scope.resume.zipcode.text)){
                    var location = $scope.zipCodeBase[$scope.resume.zipcode.text].split(',');
                    $scope.resume.city = location[0];
                    $scope.resume.state = location[1];
                    
                }
                else{
                    $scope.resume.zipcode.error = true;
                    $scope.resume.city = '';
                    $scope.resume.state = '';
                }
                if($scope.resume.zipcode.text == "00000"){
	                $scope.isNoneUS.checked = true;
	                $scope.resume.zipcode.error = false;
	                $scope.resume.city = 'City';
                    $scope.resume.state = 'State';
                }
            }
            
            $scope.changeNoneUS = function(){
	            
	            if($scope.isNoneUS.checked){
		            $scope.resume.zipcode.text = "00000";
		            $scope.resume.zipcode.error = false;
		            $scope.resume.city = 'City';
                    $scope.resume.state = 'State';
	            }else{
		            $scope.resume.zipcode.text = "";
		            $scope.resume.zipcode.error = false;
		            $scope.resume.city = '';
                    $scope.resume.state = '';
	            }
            }
            $scope.isZipUS = function(){
                return  !$scope.resume.zipcode.error && $scope.resume.zipcode.text!=undefined && $scope.resume.zipcode.text.length>0 && $scope.resume.zipcode.text!="00000";
            }
             
            $scope.checkEmail = function(){
                if($scope.resume.email.text===undefined){
                    $scope.resume.email.error = true;   
                }
            }
            $scope.setCurrentVideoName = function(name,link,objName,videoTitle){
	           $scope.resume[objName].error = false;
	           var videoIndex = objName.replace('video', "");
	           $rootScope.currentVideoIndex = videoIndex;
	           $rootScope.currentVideoTitle = videoTitle;
	           if(!$scope.serverResume.hasOwnProperty('id')){
		           resumeFactory.publishResume(true);
	           }else{
		           $rootScope.currentResumeId = $scope.serverResume.id;
	           }
	           
                resumeFactory.setCurrentVideo({name:name,link:link});
            }
            $scope.setCurrentPhoto = function(name,link){
	            $scope.resume.photo.error = false;
                resumeFactory.setCurrentPhoto({name:name,link:link});
            }
            $scope.deleteMergedVideo = function(){
                $scope.resume.videoMerged = {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                };
                resumeFactory.setMyResume({video_merged_url:{
                    link:'',
                    text:'',
                    html:'',
                    error:false
                }});
               
            }
            
            
          
                $scope.resume = resumeFactory.myResume();
                $scope.resume.video1.text = systemFactory.systemAnket().video1_title;
                $scope.resume.video2.text = systemFactory.systemAnket().video2_title;
                $scope.resume.video3.text = systemFactory.systemAnket().video3_title;
                $scope.resume.video4.text = systemFactory.systemAnket().video4_title;

			
			$scope.restore = (function(){
				var popupMessage = {
					header:'Are you sure you want to restore your resume?',
					body:'All your changes will be lost!',
					btnOneName:"Restore",
					btnTwoName:"Cancel",
					btnOneClick:function(){
						resumeFactory.restoreResume();
					}
				}

				popUpManager.popupModal(popupMessage);
                
            });
            $scope.checkForCurrentVideo = (function(){
                if(resumeFactory.getCurrentVideo().link.length>2){
                    
                    if(resumeFactory.getCurrentVideo().name === 'resume1'){
                        $scope.resume.video1.html = $sce.trustAsHtml("<video id='video' controls>"+
                        "<source id='vid-source'  src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video1.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume2'){
                        $scope.resume.video2.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video2.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume3'){
                        $scope.resume.video3.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video3.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume4'){
                        $scope.resume.video4.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video4.link = resumeFactory.getCurrentVideo().link;
                    }
                    resumeFactory.resetCurrentVideo();
                }
            });
             $scope.checkForCurrentPhoto = (function(){
                if(resumeFactory.getCurrentPhoto().link.length>2){
                    if(resumeFactory.getCurrentPhoto().name === 'userPhoto'){
                        $scope.resume.photo.html = "<div>---</div>";
                        $scope.resume.photo.link = resumeFactory.getCurrentPhoto().link;
                    }
                    resumeFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentVideo();
            $scope.localVideoTemplate = function(link){
				if(link.indexOf("http") == 0){
					var linkName = link.replace("https://www.youtube.com/v/","")
                    return $sce.trustAsHtml('<iframe id="video" src="http://www.youtube.com/embed/'+linkName+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  frameborder="0" ></iframe>');
				}
				else {
                	return $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+link+"?rel=0&"+"' type='video/mp4' > </video>");
                }      
            }
            
            $scope.setVideoHtml = function(){
	            
	            for(var i=1;i<5;i++){
		            if($scope.resume["video"+i].link.length > 2){
			            $scope.resume["video"+i].html = $scope.localVideoTemplate($scope.resume["video"+i].link);
		            }
	            }
	        }
			$scope.setVideoHtml();
            
            $scope.checkForCurrentPhoto();
             $http.get('app/resources/professions.json').success(function(data) {
                  $scope.professions = data;
             });
             $http.get('app/resources/zip.json').success(function(data) {
                  $scope.zipCodeBase = data;
                  if($scope.isFilled($scope.resume.zipcode.text)) $scope.checkZip();
             });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No'];
			$scope.isRequestCanceled = false;
			$scope.publishclick = function(){	
	        	$rootScope.$on('$stateChangeStart', 
					function(event, toState, toParams, fromState, fromParams){ 
						if(toState.name != "loading") 
				    	$scope.isRequestCanceled = true;
				}) 
				
                if(resumeFactory.checkResume($scope.resume) && resumeFactory.checkRegistration()){
	                
	                var requestDataMerge = {
						method: 'GET',
	                    url: "http://apevovideo.cloudapp.net/v/service1.svc/ffmpegmerge?resume_id="+$scope.serverResume.id
					};
					
	                $state.go('loading');
	                
					if($scope.resume.video1.link.length >0 ){
						$http(requestDataMerge).then(function(respond){ console.log(respond)});
						systemFactory.waitForChangeOnServer("resume/"+$scope.serverResume.id,"video_merged_url",$scope).then(
							function(newField){
								$scope.resume.videoMerged.link = newField;
								resumeFactory.publishPhoto().then(
					                function(result){
						                $scope.isRequestCanceled = false;
						                $scope.resume.photo.link = result;
							            resumeFactory.publishResume();
							            $state.go('resumeEdit');
						            },
						            function(err){
							            
						            }
					            ); 
			                },function(fail){
				                $scope.waitForVideo = false;
				                if(fail == '0'){
					                console.log('canceled req');
				                }else{
					                var popupMessage = {
										header:'Something went wrong...',
										body:'Please try again',
										btnOneName:'OK'
									}
				
									popUpManager.popupModal(popupMessage);
								}
			            });
			        }else{
				        resumeFactory.publishPhoto().then(
			                function(result){
				                $scope.isRequestCanceled = false;
				                $scope.resume.photo.link = result;
					            resumeFactory.publishResume();
					            $state.go('resumeEdit');
				            },
				            function(err){
					            
				            }
			            ); 
			        }
                }
			}

            //variables to check for the changes
            $rootScope.resumeScopeShare='';
			
			$scope.isEqualWithServer = function(isPublishButton){
                //constantly checking for changing parameters of the resume
                $rootScope.resumeScopeShare = $scope.resume;
                if (typeof(isPublishButton)==='undefined') isPublishButton=false;
                
                return resumeFactory.isEqualWithServer(isPublishButton);
            };
            
        }]
    )
})();
