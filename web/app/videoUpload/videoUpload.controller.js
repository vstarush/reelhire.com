(function () {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('videoUploadCtrl', ['$location','$rootScope','$scope', '$timeout', '$state', '$window','$sce','videoUploadFactory','popUpManager','systemFactory', function($location,$rootScope,$scope, $timeout, $state, $window,$sce,videoUploadFactory,popUpManager,systemFactory) {
            
            
            
	        VideoFileName = $rootScope.currentResumeId+"_"+$rootScope.currentVideoIndex+".mp4";
            $scope.video = {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                };
            $scope.zig = $sce.trustAsHtml("");
            $scope.currentVideoQuestion = $rootScope.currentVideoTitle;    
            $scope.isRecording = false;
            $scope.loadVideoFromFile  = function(event){
		        var fsize = event.target.files[0].size;        
		        if(fsize>1048576*20) //do something if file size more than 1 mb (1048576)
		        {
		            var popupMessage = {
						header:'',
						body:'Selected video too big. Only videos less than 20Mb supported. Please choose another video',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
		            return;
		        }else{
		            //alert(fsize +" bites\nYou are good to go!");
		        }
	           
                var files = event.target.files;
                var reader = new FileReader();
		        reader.onload = function(event){
			        $scope.isCameraMode = true;
			        $scope.waitForVideo = true;
			        var blob = new Blob([event.target.result], {type: 'video/mp4'});
                    uploadBlobsToServer(blob,'.mp4');
					waitForYoutubeVideo();
                }
            
                reader.readAsArrayBuffer(files[0]);
            };
            
            $scope.pushButton = function(buttonID){
                 document.getElementById(buttonID).click();
            };
             $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>2;
            };
            $scope.checkForCurrentVideo = function(){
	            
	            var videoLink = videoUploadFactory.currentFactory().getCurrentVideo().link;
                if(videoLink.length>2){
                    videoLink = videoLink.replace("https://www.youtube.com/v/","http://www.youtube.com/embed/");
					if(videoLink.indexOf("http") == 0){
						$scope.video.html = $sce.trustAsHtml('<iframe src="'+videoLink+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="640" height="360" frameborder="0" id="videoAnswer"></iframe>');
					}else {
                    	$scope.video.html = $sce.trustAsHtml("<video class='video' controls><source id='vid-source' src='"+videoLink+"?rel=0&"+"' type='video/mp4' onerror='fallback(this)'> </video>");
                    }
                }
            };
            $scope.checkForCurrentVideo();
            $scope.goBack = function(){
	            $window.history.back();
            };

            $scope.fallbackS = function(video){
	            var the_url = "";
                $scope.video.html = $sce.trustAsHtml("");
                videoUploadFactory.currentFactory().setCurrentVideo({
                    link:the_url
                });
                $scope.$apply();
                
                var popupMessage = {
					header:'Wrong video codec format',
					body:'Only H.264 codec supported. Please choose another video',
					btnOneName:'OK'
				}
				popUpManager.popupModal(popupMessage);
            }
            var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
            $scope.isChrome = false;//!!window.chrome && !isOpera;
            $scope.isCameraMode = false;
            $scope.waitForVideo = false;
            $scope.isRequestCanceled = false;
            
            $rootScope.$on('$stateChangeStart', 
						function(event, toState, toParams, fromState, fromParams){
							if(toState.name != "loading") 
						    $scope.isRequestCanceled = true;
						})
            function waitForYoutubeVideo(){
	            systemFactory.waitForChangeOnServer("resume/"+$rootScope.currentResumeId,"video"+$rootScope.currentVideoIndex+"_url",$scope).then(
				function(newField){
					setTimeout(function() {
						
						$scope.isCameraMode = false;
		                newField = newField.replace("watch?v=", "v/");
		                $scope.video.html = $sce.trustAsHtml('<iframe src="'+newField+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="640" height="360" frameborder="0" id="videoAnswer"></iframe>');
		                videoUploadFactory.currentFactory().setCurrentVideo({
		                    link:newField
		                });
		                
						$scope.waitForVideo = false;
						$scope.isRecording = false;
		                videoUploadFactory.currentFactory().publishResume(true);
		                var popupMessage = {
							header:'',
							body:'Your video has successfully uploaded and will be available in a minute.',
							btnOneName:'OK'
						}
							
						popUpManager.popupModal(popupMessage);
						$scope.goBack();
			    	}, 3000);
                },function(fail){
	                if(fail == '0'){
// 		                console.log('canceled req');
	                }else{
		                var popupMessage = {
							header:'Something went wrong...',
							body:'Please try again',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
					}
	                $scope.waitForVideo = false;
	                $scope.isRecording = false;
					$scope.isRequestCanceled = false;
                });
	        }
            
			$scope.scriptCamInit = function(){
				//console.log('scriptCamInit');
				$("#webcam").scriptcam({ 
					fileReady:$scope.scopeFileReady,
					country: 'usa',
					cornerRadius:0,
					cornerColor:'e3e5e2',
					onError:onError,
					promptWillShow:promptWillShow,
					showMicrophoneErrors:false,
					onWebcamReady:onWebcamReady,
					setVolume:100,
					timeLeft:timeLeft,
					fileName:'demofilename',
					connected:showRecord,
					width: 640,
					height: 360,
					//zoom: 512.0/640
				});

			}
		
			var recordingDIV = document.querySelector('.containerWebRTC');          
            var webRTCObj = recordingDIV;
            var commonConfig = {
                onMediaCaptured: function(stream) {
                    webRTCObj.stream = stream;
                    if(webRTCObj.mediaCapturedCallback) {
                        webRTCObj.mediaCapturedCallback();
                    }
                },
                onMediaStopped: function() {

                },
                onMediaCapturingFailed: function(error) {
                    
                    commonConfig.onMediaStopped();
                }
            };
            var mimeType = 'video/webm';
            var audioRecorder;
            var videoRecorder; 
            function stopRecording(){
	            function stopStream() {
	                if(webRTCObj.stream && webRTCObj.stream.stop) {
	                    webRTCObj.stream.stop();
	                    webRTCObj.stream = null;
	                }
	            }

	            if(webRTCObj.recordRTC) {
                    webRTCObj.recordRTC[0].stopRecording(function(url) {
                        webRTCObj.recordRTC[1].stopRecording(function(url) {
                            webRTCObj.recordingEndedCallback(url);
                            stopStream();
                        });
                    });
                }
	        }
	        function startRecording(){
	                videoRecorder.initRecorder(function() {
                        audioRecorder.initRecorder(function() {
                            audioRecorder.startRecording();
                            videoRecorder.startRecording();
                        });
                    });
                    webRTCObj.recordRTC.push(audioRecorder, videoRecorder);
                    webRTCObj.recordingEndedCallback = function() {							
						uploadVideoToServer(videoRecorder.blob,audioRecorder.blob);
                    };
	        }
	        function uploadVideoToServer(videoBlob,audioBlob){
		        uploadBlobsToServer(videoBlob,'.webm');
		        uploadBlobsToServer(audioBlob,'.wav');
		        waitForYoutubeVideo();
	        }
	        function uploadBlobsToServer(blob,extension){
		        
	            var uri = "http://apevovideo.cloudapp.net/v/service1.svc/uploadfile";	
				var fd = new FormData();
				fd.append('extension', extension);
				fd.append('data', blob);
				fd.append('resume_id', $rootScope.currentResumeId);
				fd.append('question_order_id', $rootScope.currentVideoIndex);
				fd.append('text',$rootScope.currentVideoTitle);
				
	            $.ajax({
	                type: "POST",
	                url: uri,
	                data: fd,
	                contentType: false,//"multipart/form-data",
	                processData: false,
	
	                success: function (_data) {
// 		                console.log(_data);
	                },
	                failure: function (fail) {
// 	                    console.log(fail);
	                }
	            });
	        }

     
            function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
            }
			$scope.webRTCInit = function(){
				showRecord();
				captureAudioPlusVideo(commonConfig);
                
                webRTCObj.mediaCapturedCallback = function() {
                    webRTCObj.recordRTC = [];
                    if(!window.params.bufferSize) {
                        // it fixes audio issues whilst recording 720p
                        window.params.bufferSize = 16384;
                    }
                    audioRecorder = RecordRTC(webRTCObj.stream, {
                        type: 'audio',
                        bufferSize: typeof window.params.bufferSize == 'undefined' ? 0 : parseInt(window.params.bufferSize),
                        sampleRate: typeof window.params.sampleRate == 'undefined' ? 44100 : parseInt(window.params.sampleRate),
                        leftChannel: window.params.leftChannel || false,
                        disableLogs: window.params.disableLogs || false,
                        recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                    });
                    
                    videoRecorder = RecordRTC(webRTCObj.stream, {
                        type: 'video',
                        // recorderType: isChrome && typeof MediaRecorder !== 'undefined' ? MediaStreamRecorder : null,
                        disableLogs: window.params.disableLogs || false,
                        canvas: {
                            width: 512,
                            height: 288
                        },
                        frameInterval: typeof window.params.frameInterval !== 'undefined' ? parseInt(window.params.frameInterval) : 33 // minimum time between pushing frames to Whammy (in milliseconds)
                    });
                }; 
				
			}
		
			$scope.browserDetect = function(){
				 $scope.isCameraMode = true;
				if($scope.isChrome){
					$scope.webRTCInit();
				}else{
					$scope.scriptCamInit();
				}
			}
			
			$scope.videoFromCam = function(){
				$scope.zig = $sce.trustAsHtml(
				"<ziggeo ziggeo-limit=30 ziggeo-width=640 ziggeo-height=360 ziggeo-recording_width=640 ziggeo-recording_height=360 ziggeo-disable_first_screen=true ziggeo-disable_snapshots=true ziggeo-disable_device_test=true ziggeo-perms='forbidrerecord,forbidswitch'> </ziggeo>");
				$scope.browserDetect();
			}
			
			 var DetectRTC = {};
                (function () {
                    
                    
                    var screenCallback;
                    
                    DetectRTC.screen = {
                        chromeMediaSource: 'screen',
                        getSourceId: function(callback) {
                            if(!callback) throw '"callback" parameter is mandatory.';
                            screenCallback = callback;
                            window.postMessage('get-sourceId', '*');
                        },
                        isChromeExtensionAvailable: function(callback) {
                            if(!callback) return;
                            
                            if(DetectRTC.screen.chromeMediaSource == 'desktop') return callback(true);
                            
                            // ask extension if it is available
                            window.postMessage('are-you-there', '*');
                            
                            setTimeout(function() {
                                if(DetectRTC.screen.chromeMediaSource == 'screen') {
                                    callback(false);
                                }
                                else callback(true);
                            }, 2000);
                        },
                        onMessageCallback: function(data) {
                            if (!(typeof data == 'string' || !!data.sourceId)) return;
                            
                            // "cancel" button is clicked
                            if(data == 'PermissionDeniedError') {
                                DetectRTC.screen.chromeMediaSource = 'PermissionDeniedError';
                                if(screenCallback) return screenCallback('PermissionDeniedError');
                                else throw new Error('PermissionDeniedError');
                            }
                            
                            // extension notified his presence
                            if(data == 'rtcmulticonnection-extension-loaded') {
                                if(document.getElementById('install-button')) {
                                    document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
                                }
                                DetectRTC.screen.chromeMediaSource = 'desktop';
                            }
                            
                            // extension shared temp sourceId
                            if(data.sourceId) {
                                DetectRTC.screen.sourceId = data.sourceId;
                                if(screenCallback) screenCallback( DetectRTC.screen.sourceId );
                            }
                        },
                        getChromeExtensionStatus: function (callback) {
                            if (!!navigator.mozGetUserMedia) return callback('not-chrome');
                            
                            var extensionid = 'ajhifddimkapgcifgcodmmfdlknahffk';
                            var image = document.createElement('img');
                            image.src = 'chrome-extension://' + extensionid + '/icon.png';
                            image.onload = function () {
                                DetectRTC.screen.chromeMediaSource = 'screen';
                                window.postMessage('are-you-there', '*');
                                setTimeout(function () {
                                    if (!DetectRTC.screen.notInstalled) {
                                        callback('installed-enabled');
                                    }
                                }, 2000);
                            };
                            image.onerror = function () {
                                DetectRTC.screen.notInstalled = true;
                                callback('not-installed');
                            };
                        }
                    };
                    
                    // check if desktop-capture extension installed.
                    if(window.postMessage && $scope.isChrome) {
                        DetectRTC.screen.isChromeExtensionAvailable();
                    }
                })();
                
                DetectRTC.screen.getChromeExtensionStatus(function(status) {
                    if(status == 'installed-enabled') {
                        if(document.getElementById('install-button')) {
                            document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
                        }
                        DetectRTC.screen.chromeMediaSource = 'desktop';
                    }
                });
                
                window.addEventListener('message', function (event) {
                    if (event.origin != window.location.origin) {
                        return;
                    }
                    
                    DetectRTC.screen.onMessageCallback(event.data);
                });
			
			
			function showRecord() {
				$( "#recordStartButton" ).attr( "disabled", false );
			}
			
			
			$scope.startRecording = function() {

                $( "#timeLeft" ).css('color','red');
                $( "#timeLeft" ).css('font-weight','bold');

				$( "#recordStartButton" ).attr( "disabled", true );
				$( "#recordStopButton" ).attr( "disabled", false );
				$( "#recordPauseResumeButton" ).attr( "disabled", false );
				if($scope.isChrome){
					
					startRecording();
				}else{
					$("#webcam" ).show();
					$.scriptcam.startRecording();
				}

				timeLeft = 30;
				timerOn = true;
			}
			$scope.cancelCamera = function() {
				$scope.isCameraMode = false;
				$scope.waitForVideo = false;
			}
			$scope.closeCamera = function() {
                $( "#timeLeft" ).css('color','white');
                $( "#timeLeft" ).css('font-weight','normal');
				$scope.isCameraMode = true;
				$scope.waitForVideo = true;
			}
			$scope.scopeFileReady = function(fileName){
				
                var fileNameNoExtension = fileName.replace(".mp4", "");
	
				var uri = 'http://apevovideo.cloudapp.net/v/service1.svc/grabvideo?url='+fileName+'&resume_id='+$rootScope.currentResumeId+'&question_order_id='+$rootScope.currentVideoIndex+'&text='+$rootScope.currentVideoTitle;
				$.ajax({
				    type: "GET",
				    url: uri,
				    data:null ,
				    contentType: "application/json",
				    DataType: "json", varProcessData: true,
				   
				    success: function (data) {
					    			
				    },
				    failure: function (fail) {

				    }
				});
				waitForYoutubeVideo();
				
			}
			
			 $rootScope.$on('$locationChangeSuccess', function() {
		        $rootScope.actualLocation = $location.path();
		    });        
		
		   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
		        if($rootScope.actualLocation === newLocation) {
		            if($scope.isChrome){
			            $scope.waitForVideo = true;
						$("#recordPauseResumeButton" ).attr( "disabled", true );
						$("#recordStopButton" ).attr( "disabled", true );
						$('#message').html('Please wait for the video conversion to finish...');
						timeLeft = 30;
						timerOn = false;
			            if(webRTCObj.recordRTC) {
		                    webRTCObj.recordRTC[0].stopRecording(function(url) {
		                        webRTCObj.recordRTC[1].stopRecording(function(url) {
		                            if(webRTCObj.stream && webRTCObj.stream.stop) {
					                    webRTCObj.stream.stop();
					                    webRTCObj.stream = null;
					                }
		                        });
		                    });
		                }
					}
		        }
		    });
		    $scope.ziggeoBegin = function(){
                $scope.isRecording = true;
                $scope.$apply();

		   };
		   $scope.ziggeoFinish = function(){
			   $scope.closeCamera();
			   $scope.$apply();
		   };
		   $scope.ziggeoSubmitted = function(data){
			   $scope.scopeFileReady(ZiggeoApi.Videos.source(data.video.token ));
			   $scope.zig = $sce.trustAsHtml("");
			   
		   }
	        
        }]
    )
    .factory('videoUploadFactory', function(){
	    var currentFactory = "";
	    return{
		    setCurrentFactory: function(factory){
			    currentFactory = factory;
		    },
		    currentFactory: function(){
			    return currentFactory;
		    }
		};
	});


    
	
	
})();
ZiggeoApi.token = "1f24b920b57a81c253ff93266f0afce3";
ZiggeoApi.Events.on("recording", function (data) {
	angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoBegin();
});
ZiggeoApi.Events.on("finished", function (data) {
	angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoFinish();
});
ZiggeoApi.Events.on("submitted", function (data) {
    angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoSubmitted(data);
});
ZiggeoApi.Events.on("countdown", function (time, data) {
	if(time < 700) document.getElementById("recordingStartAudio").play(); 
});        
        

var VideoFileName = "";
var fallback = function(video){
    angular.element(document.getElementById('workAreaContainer')).scope().fallbackS();
}
function onError(errorId,errorMsg) {
	$scope.waitForVideo = false;
	angular.element(document.getElementById('videoUploadCtrl')).scope().goBack();
}
function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
	$.each(cameraNames, function(index, text) {
		$('#cameraNames').append( $('<option></option>').val(index).html(text) )
	}); 
	$('#cameraNames').val(camera);
	$.each(microphoneNames, function(index, text) {
		$('#microphoneNames').append( $('<option></option>').val(index).html(text) )
	}); 
	$('#microphoneNames').val(microphone);
}
function promptWillShow() {
	//alert('A security dialog will be shown. Please click on ALLOW.');
}

function timeLeft(value) {

}
var timeLeft = 30;
var timerOn = false;
function timer() {
	if(timerOn && timeLeft>0){
		timeLeft = timeLeft-1;
	}else if(timeLeft<=0){
		angular.element(document.getElementById('videoUploadCtrl')).scope().closeCamera();
		angular.element(document.getElementById('videoUploadCtrl')).scope().$apply();
	}
	$('#timeLeft').text("Time Left "+timeLeft+" sec");
}
setInterval(timer,1000);
function changeCamera() {
	$.scriptcam.changeCamera($('#cameraNames').val());
}
function changeMicrophone() {
	$.scriptcam.changeMicrophone($('#microphoneNames').val());
}
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1))) {
        params[d(match[1])] = d(match[2]);
        if(d(match[2]) === 'true' || d(match[2]) === 'false') {
            params[d(match[1])] = d(match[2]) === 'true' ? true : false;
        }
    }
    window.params = params;
    
})();
