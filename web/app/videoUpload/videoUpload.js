(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('videoUpload', {
            url: '/videoUpload',
            templateUrl: 'app/videoUpload/videoUpload.html',
            controller: 'videoUploadCtrl'
          });
      }]);
      
})();
