(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('photoUpload', {
            url: '/photoUpload',
            templateUrl: 'app/photoUpload/photoUpload.html',
            controller: 'photoUploadCtrl'
          });
          $stateProvider
          .state('photoUploadRecruiter', {
            url: '/photoUpload',
            templateUrl: 'app/photoUpload/photoUpload.html',
            controller: 'photoUploadCtrl',
            params:{
	            recruiterIcon:true
            }
          });
      }]);
      
})();
