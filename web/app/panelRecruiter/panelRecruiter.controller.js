(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('panelRecruiterCtrl', 
        ['$scope','$rootScope', '$location', '$window','$state','resumeFactory','questionnarieFactory','popUpManager','recruiterFactory', function($scope, $rootScope, $location, $window, $state,resumeFactory,questionnarieFactory,popUpManager,recruiterFactory) { 
            
            console.log($rootScope.favorites);
            
			$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			
			$scope.updateNotifications = function(){
				$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			}
			
			//side butttons checks if they are disabled or not
			$scope.goToLink = function(className, state, emailType)
			{
				if(recruiterFactory.checkRegistration() && !$('.'+ className).hasClass( "disabled" ))
				{
					$state.go(state);
					$rootScope.emailType = emailType;
				}
			}
			
			//select All rootscope variable
			$rootScope.selectAllText = 'Select All';
			
			//On change of state zero selected candidates and select all button
			$rootScope.$on('$stateChangeStart', function(){ 
			    $rootScope.selectAllText = 'Select All';
			    //$rootScope.selectedCandidates = [];
			})
			
			// function that changes select all to deselect all
			$scope.selectAll = function(){
				
				//function from inside questionnaires
				$rootScope.selectAllCandidates();
				
				if($rootScope.selectAllText === 'Select All')
				{
					$rootScope.selectAllText = 'Deselect All';
				}
				else
				{
					$rootScope.selectAllText = 'Select All';
				}
			}
			
			$scope.onPage = function (states) {
				
				if(states.constructor===Array)
				{
					for(var i=0;i<states.length;i++){
					if(states[i] === $location.path())
					return states[i] === $location.path();
					}
				}
				else
				return states === $location.path();
            };
            
            $scope.onState = function(newState){
	            if(newState === "questionnairesPreview"){
		            if(questionnarieFactory.checkCurrentQuestionnarie()){
		           		$state.go(newState);
		           	}
	            }
                else if(newState === 'recruiterQuestionnaires'){
                    $rootScope.selectedCandidates = [];
                    $state.go(newState);
                }
	            else if(newState === 'favorites'){
		            if($rootScope.favorites.length>0)
		            $state.go(newState);
	            }
	            else if(newState === "resumePreview"){
		            if(resumeFactory.checkResume(resumeFactory.myResume())){
		           		$state.go(newState);
		           	}
	            }else if(newState === "getLink"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }else if(newState === "sendTo"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           	var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }
	            
            }
        }]
    )
    .service('toggleSubButtons', function(){
	    return {
		    Disable:function(className){
			    $('.' + className).addClass("disabled");
		    },
		    Enable:function(className){
			    $('.' + className).removeClass("disabled");
		    }
	    };
    });

})();
