(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('TopbarCtrl', 
        ['$scope', '$location', '$window','$state','systemFactory','$rootScope', function($scope, $location, $window, $state, systemFactory,$rootScope) {  
            
            //$scope.account = 'Login';
            $scope.inMenu = function(){
                if(systemFactory.md5().length>5)
                {
                    
                    if($location.path()==='/about' || $location.path()==='/main'){
                        $scope.account = 'My Account';
                    }
                    else{
                        return false;
                    }
                }
                else{
                    
                    $scope.account = 'Log In';
                }
                
                return true;
            };
            
            $scope.btnToState = function(state){
                if(!$rootScope.checkIfMobile()){
                    $state.go(state);
                }   
            };
            
            
        }]
    );

})();
