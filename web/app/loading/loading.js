(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('loading', {
                        url: '/loading',
                        templateUrl: 'app/loading/loading.html',
                        controller: 'LoadingCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
            }
        ]);
})();