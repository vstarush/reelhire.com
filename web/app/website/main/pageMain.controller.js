(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('PageMainCtrl', ['$scope',  '$state', 'resourceLogin','$window','resourceRegister','$rootScope','$location', function($scope, $state, resourceLogin,$window,resourceRegister,$rootScope,$location)
        {
            $scope.videoIsCollapsed = true;
            
            $scope.btnToState = function(state,userType){
                if(!$rootScope.checkIfMobile()){
	                $rootScope.userType = userType;
	                $rootScope.userEmail = "anonimus";
                    $state.go(state);
                }   
            };
            $scope.contactUs = function(){
                $state.go('contactUs')
            }
			
            
            $('.linkToMainVideo').bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1000, 'easeInOutExpo');
                event.preventDefault();
            });
        }
    ]);

    
    
})();
