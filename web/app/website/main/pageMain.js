(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('main', {
            url: '/main',
            templateUrl: 'app/website/main/pageMain.html',
            controller: 'PageMainCtrl'
          });
      }]);
      
})();
