(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('contactUs', {
            url: '/contactUs',
            templateUrl: 'app/website/pages/contactUs.html'
          });
      }]);
    
    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('termsAndConditions', {
            url: '/termsAndConditions',
            templateUrl: 'app/website/pages/termsAndConditions.html'
          });
      }]);
     angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('privacyPolicy', {
            url: '/privacyPolicy',
            templateUrl: 'app/website/pages/privacyPolicy.html'
          });
      }]);
      angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('help', {
            url: '/help',
            templateUrl: 'app/website/pages/help.html'
          });
      }]);
})();