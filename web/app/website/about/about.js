(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('about', {
            url: '/about',
            templateUrl: 'app/website/about/about.html',
            controller: 'AboutCtrl',
            animation: 'third'
          });
      }]);
})();
