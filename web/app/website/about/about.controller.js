(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('AboutCtrl', ['$scope', '$state', 'resourceLogin','$window','resourceRegister','$stateParams','$rootScope', function($scope, $state, resourceLogin,$window,resourceRegister,$stateParams,$rootScope)
        {
		   if($stateParams.isJobSeeker===true)
               $scope.aboutState = 'jobseeker';
               
            if($scope.aboutState === 'jobseeker'){
                $scope.companyInfo = 'Hello Seeker';
                }
            else{
                $scope.companyInfo = 'Hello You'
            }
            
            $rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute){
                $rootScope.animation = currRoute.animation;
            });
            
            $scope.btnToState = function(state,userType){
                if(!$rootScope.checkIfMobile()){
	                $rootScope.userType = userType;
	                $rootScope.userEmail = "anonimus";
                    $state.go(state);
                }   
            };
                
        }]
    );

    
})();