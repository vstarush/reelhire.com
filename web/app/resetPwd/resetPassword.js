(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('resetPwd', {
            url: '/resetPassword',
            templateUrl: 'app/resetPwd/resetPassword.html',
            controller: 'resetPasswordCtrl'
          });
      }]);
      
})();
