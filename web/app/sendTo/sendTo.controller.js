(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('sendToCtrl', ['$rootScope','$scope', '$timeout', '$state', '$http', '$window', 'resumeFactory','emailFactory','systemFactory','popUpManager', function($rootScope, $scope, $timeout, $state, $http, $window,resumeFactory,emailFactory,systemFactory,popUpManager ) {
	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
	        $scope.manyEmailsPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9},? *)+$';
	        
	        $scope.emailToPattern = $scope.emailPattern;
            var maxNumberOfRecipients = 10;
			
			$scope.serverResume = resumeFactory.serverResume();
			$scope.emailFrom = {text:$rootScope.userEmail,error:false};
			$scope.emailTo = {text:"",error:false};
			$scope.subject = {text:"",error:false};
			$scope.emailBody = "";
			$scope.candidateList = emailFactory.candidates();
			$scope.emailToDisable = false;
			
			if($rootScope.emailType == "emailToSelected"){
				$scope.subject.text = "Job Application";
				$scope.emailBody = "I would like to talk to you further about the position.";
			}else if($rootScope.emailType == "emailSelected"){
				$scope.subject.text = "A candidate of Interest";
				$scope.emailBody = "Hi there, please check out the following candidate’s Video Questionnaire. They might be a good fit for the company!";
			}else if(systemFactory.userType() == "seeker"){
				$scope.subject.text = "Please check out my Video Resume";
				$scope.emailBody = "";
			}
			$scope.emailTitle = emailFactory.emailTitle();
			
			$scope.checkEmailFrom = function(){
                if($scope.emailFrom.text===undefined){
                    $scope.emailFrom.error = true;   
                }
            }
            $scope.checkEmailTo = function(){
                if($scope.emailTo.text===undefined){
                    $scope.emailTo.error = true;   
                }
            }

            $scope.showCandidates = function(){
	            return $rootScope.emailType=="emailSelected" || systemFactory.userType() == "seeker";
            }
            if(!$scope.showCandidates()){
	            $scope.emailToPattern = "";
	            for(var i=0;i<Math.min($scope.candidateList.length,maxNumberOfRecipients);i++){
		            if($scope.emailTo.text.length == 0){
		            	$scope.emailTo.text = ""+$scope.candidateList[i].email; 
		            }else{
			            $scope.emailTo.text = $scope.candidateList[i].email + ', ' + $scope.emailTo.text;
		            }
	            }

	            $scope.emailToDisable = true;
	            
            }
            $scope.sendEmail = function(){
					         
	            var isCorrect = emailFactory.checkEmail($scope);
	          
	            if(!isCorrect){
		            
		            var popupMessage = {
						header:'',
						body:'Please, check fields',
						btnOneName:"OK"
					}
	
					popUpManager.popupModal(popupMessage);
						
		            //alert("Please check fields");
	            }else{
		            $state.go('loading');
		            while($scope.emailTo.text.indexOf("  ") != -1){
			            $scope.emailTo.text = $scope.emailTo.text.replace("  "," ").replace(", "," ").replace(" ,"," ");
		            }
		            var request = $http({
					    method: "post",
					    url: "/components/sendgrid-php/myMail.php",
					    data: {
						    sm: systemFactory.mc().id,
						    sp: systemFactory.mc().value,
					        emailTo: $scope.emailTo.text.replace(" ",",").replace(",",", "),
					        emailFrom: $scope.emailFrom.text,
					        subject : $scope.subject.text,
							message : message()
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					/* Check whether the HTTP Request is successful or not. */

					request.success(function (data) {
						
						$state.go('sendTo');
						if(data.indexOf('error') == -1 && data.indexOf('Error') == -1 && data.indexOf('ERROR') == -1){
							
							var popupMessage = {
								header:'',
								body:'Your email was sent successfully!',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}else{
							var popupMessage = {
								header:'',
								body:'Your email was not sent. Try again.',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}	
						
					    //alert("Your email was sent successfully!");
					});

	            }

            }
            
            
            var client = new XMLHttpRequest();
            var emailTemplate = "";
			client.open('GET', '/app/serverRequests/emailTemplate.html');
			client.onreadystatechange = function() {
			  	emailTemplate = client.responseText;
			}
			client.send();
			
			
            var message = function(){
				var textTitle = "";//"This is a candidate of interest. Please check out their Video Resume";
				
				var startOfCandidateSection = emailTemplate.indexOf("<br><div style=\"b");
				var lengthOfCandidateSection = emailTemplate.indexOf('---->')-startOfCandidateSection+5;
				var sectionCandidate = emailTemplate.slice(emailTemplate.indexOf("<br><div style=\"b"),emailTemplate.indexOf('<!---->'));
				emailTemplate = emailTemplate.replace(sectionCandidate,"");
				var sectionsCandidate = [];
				var selectedCandidate = [];
				var candidateCount = 1;
				
				if(systemFactory.userType() == "seeker"){
					selectedCandidate[0] = $scope.serverResume;
					selectedCandidate[0].resumeId = selectedCandidate[0].id;
					selectedCandidate[0].firstName = selectedCandidate[0].first_name;
					selectedCandidate[0].lastName = selectedCandidate[0].last_name;
					selectedCandidate[0].photoLink = selectedCandidate[0].photo_url;
				}else if($rootScope.emailType == 'emailSelected'){
					candidateCount = $scope.candidateList.length;
					selectedCandidate = $scope.candidateList;
					maxNumberOfRecipients = candidateCount;
				}else{
					candidateCount = 0;
				}
				
				for(var i=0;i<Math.min(candidateCount,maxNumberOfRecipients);i++){
					sectionsCandidate[i] = sectionCandidate;
					var decriptedId = $rootScope.idToHash(selectedCandidate[i].resumeId,'webResume');
					sectionsCandidate[i] = sectionsCandidate[i].replace("{resumeId}",decriptedId);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{firstName}",selectedCandidate[i].firstName);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{lastName}",selectedCandidate[i].lastName);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{photo}",selectedCandidate[i].photoLink);
					var tempString = emailTemplate.slice(startOfCandidateSection);
					emailTemplate = emailTemplate.slice(0, startOfCandidateSection) + sectionsCandidate[i] + tempString;
					startOfCandidateSection += sectionsCandidate[i].length;
				}
				emailTemplate = emailTemplate.replace("{textTitle}",textTitle)
					.replace("{textBody}",$scope.emailBody );
				
	            return emailTemplate;
            }
        }]
    );

    
})();
