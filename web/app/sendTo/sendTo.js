(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('sendTo', {
            url: '/sendTo',
            templateUrl: 'app/sendTo/sendTo.html',
            controller: 'sendToCtrl'
          });
      }]);
      
})();
