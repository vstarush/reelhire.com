(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('QuestionnairesCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window','questionnarieFactory','popUpManager', function($rootScope,$scope, $timeout, $state, $window,questionnarieFactory, popUpManager) {
			$scope.questionnaries = questionnarieFactory.questionnaries();
			
			(function updateQuestionnaries(){
                setTimeout(function() {
	                if($scope)
	                updateQuestionnaries();
	                
					$scope.questionnaries = questionnarieFactory.questionnaries();
					
					$scope.$apply();
		    	}, 10000);
            })(); 
            
            
			$scope.checkForQuestionnarie = function( ) {
			  return function( questionnarie ) {
				  if(!questionnarie.isHasAnket) return false;
				   return questionnarie.companyName.length>2;
			  };
			};
			$scope.status = function(questionnarie){
				if(questionnarie.isNew) return "New";
				if(questionnarie.ispublished) return "Sent";
				return "";
			},
			$scope.deleteAnket = function(questionnarie){
 				var popupMessage = {
					header:'Are you sure?',
					body:'Do you want to delete the Questionnaire for the position ' + questionnarie.companyPosition + '?',
					btnOneName:'Delete',
					btnTwoName:'Cancel',
					btnOneClick:function(){
						for(var i=0; i<$scope.questionnaries.length; i++){
							if($scope.questionnaries[i].anket_id === questionnarie.anket_id){
								questionnarieFactory.deleteNotification(questionnarie.notificationId);
								questionnarieFactory.deleteAnket(questionnarie.anket_id);
								$scope.questionnaries.splice(i,1);
							}
						}
					},
					btnTwoClick:function(){
						
					}
				}

				popUpManager.popupModal(popupMessage);
			},
			$scope.goToQuestionnarie = function(questionnarie){
				questionnarie.isNew = false;
				questionnarieFactory.deleteNotification(questionnarie.notificationId);
				if(questionnarie.ispublished){
					questionnarieFactory.setCurrentQuestionnarie(questionnarie.id);
					$state.go('publishedQuestionnaire');
				}else{
					questionnarieFactory.setCurrentQuestionnarie(questionnarie.anket_id);

					if(!questionnarie.hasOwnProperty('id')){
						questionnarieFactory.publishQuestionnarie(true);
					}else{
						$rootScope.currentResumeId = questionnarie.id;
					}
					$state.go('questionnairesEdit');
				}
			}
			$scope.anketId = function(anket){
	            return -Number(anket.anket_id);
            }
        }]
    );

    
})();
