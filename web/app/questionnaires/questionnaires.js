(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('questionnaires', {
            url: '/questionnaires',
            templateUrl: 'app/questionnaires/questionnaires.html',
            controller: 'QuestionnairesCtrl'
          });
          
      }]);
      
})();
