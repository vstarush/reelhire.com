(function(win) {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('QuestionnairesEditCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window', '$sce','cookieManager','resumeFactory','questionnarieFactory','systemFactory','$http','videoUploadFactory','photoUploadFactory', function($rootScope,$scope, $timeout, $state, $window, $sce, cookieManager,resumeFactory,questionnarieFactory,systemFactory,$http,videoUploadFactory,photoUploadFactory) {
	        
	        videoUploadFactory.setCurrentFactory(questionnarieFactory);
	        photoUploadFactory.setCurrentFactory(questionnarieFactory);
	        $scope.isNoneUS = {};
	        
	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
            
            $scope.questionnarie = questionnarieFactory.currentQuestionnarie();
            
            $scope.localVideoTemplate = function(link){
				if(link.indexOf("http") == 0){
                    var linkName = link.replace("https://www.youtube.com/v/","")
                    return $sce.trustAsHtml('<iframe id="video" src="http://www.youtube.com/embed/'+linkName+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  frameborder="0" ></iframe>');
				}else {
                	return $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+link+"?rel=0&"+"' type='video/mp4' > </video>");
                }      
            }
            
            $scope.setVideoHtml = function(){
	            for(var i=1;i<5;i++){
		            if($scope.questionnarie["video"+i].link.length > 2){
			            $scope.questionnarie["video"+i].html = $scope.localVideoTemplate($scope.questionnarie["video"+i].link);
		            }
	            }
	        }
			$scope.setVideoHtml();
            $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>0;
            };
            $scope.checkZip = function(){
                if($scope.zipCodeBase.hasOwnProperty($scope.questionnarie.zipcode.text)){
                    var location = $scope.zipCodeBase[$scope.questionnarie.zipcode.text].split(',');
                    $scope.questionnarie.city = location[0];
                    $scope.questionnarie.state = location[1];
                }
                else{
                    $scope.questionnarie.zipcode.error = true;
                     $scope.questionnarie.city = '';
                    $scope.questionnarie.state = '';
                }
                if($scope.questionnarie.zipcode.text == "00000"){
	                $scope.isNoneUS.checked = true;
	                $scope.questionnarie.zipcode.error = false;
	                $scope.questionnarie.city = 'City';
                    $scope.questionnarie.state = 'State';
                }
            };
             $scope.changeNoneUS = function(){
	            if($scope.isNoneUS.checked){
		            $scope.questionnarie.zipcode.text = "00000";
		            $scope.questionnarie.zipcode.error = false;
		            $scope.questionnarie.city = 'City';
                    $scope.questionnarie.state = 'State';
	            }else{
		            $scope.questionnarie.zipcode.text = "";
		            $scope.questionnarie.zipcode.error = false;
		            $scope.questionnarie.city = '';
                    $scope.questionnarie.state = '';
	            }
            }
            $scope.isZipUS = function(){
                return  !$scope.questionnarie.zipcode.error && $scope.questionnarie.zipcode.text!=undefined && $scope.questionnarie.zipcode.text.length>0 && $scope.questionnarie.zipcode.text!="00000";
            }
            $scope.checkEmail = function(){
                if($scope.questionnarie.email.text===undefined){
                    $scope.questionnarie.email.error = true;   
                }
            }
            $scope.setCurrentVideoName = function(name,link,objName,videoTitle){
	            var videoIndex = objName.replace('video', "");
	            $rootScope.currentVideoIndex = videoIndex;
	            $rootScope.currentVideoTitle = videoTitle;
	            $scope.questionnarie[objName].error = false;
                questionnarieFactory.setCurrentVideo({name:name,link:link});
            }
            $scope.setCurrentPhoto = function(name,link){
	            $scope.questionnarie.photo.error = false;
                questionnarieFactory.setCurrentPhoto({name:name,link:link});
            }
            

			
            $scope.checkForCurrentVideo = (function(){
                if(questionnarieFactory.getCurrentVideo().link.length>2){
                    
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie1'){
                        $scope.questionnarie.video1.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video1.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie2'){
                        $scope.questionnarie.video2.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video2.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie3'){
                        $scope.questionnarie.video3.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video3.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie4'){
                        $scope.questionnarie.video4.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video4.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    questionnarieFactory.resetCurrentVideo();
                }
            });
            
             $scope.checkForCurrentPhoto = (function(){
                if(questionnarieFactory.getCurrentPhoto().link.length>2){
                    if(questionnarieFactory.getCurrentPhoto().name === 'userPhoto'){
                        $scope.questionnarie.photo.html = "<div>---</div>";
                        $scope.questionnarie.photo.link = questionnarieFactory.getCurrentPhoto().link;
                    }
                    questionnarieFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentVideo();
            
            $scope.checkForCurrentPhoto();
	        $http.get('app/resources/professions.json').success(function(data) {
	             $scope.professions = data;
	        });
	        $http.get('app/resources/zip.json').success(function(data) {
	            $scope.zipCodeBase = data;
	            if($scope.isFilled($scope.questionnarie.zipcode.text)) $scope.checkZip();
	        });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No'];
			
            $scope.publishclick = function(){
	            if(resumeFactory.checkResume($scope.questionnarie) && resumeFactory.checkRegistration()){
	                questionnarieFactory.publishPhoto().then(
		                function(result){
			                $scope.questionnarie.photo.link = result;
			                $scope.questionnarie.ispublished = true;
				            questionnarieFactory.publishQuestionnarie();
			            },
			            function(err){
				            
				            var popupMessage = {
								header:'Something went wrong',
								body:'Your questionnarie was not published. Try again',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
				            $window.history.back();
			            }
		            ); 
                }
                
			}
			    

        }]
    );
    
	

    
})();

