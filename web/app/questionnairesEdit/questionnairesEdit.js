(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('questionnairesEdit', {
            url: '/questionnaires/edit',
            templateUrl: 'app/questionnairesEdit/questionnairesEdit.html',
            controller: 'QuestionnairesEditCtrl'
          });
	    
      }]);
      
})();
