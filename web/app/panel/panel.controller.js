(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('PanelCtrl', 
        ['$scope', '$location', '$window','$state','resumeFactory','questionnarieFactory','popUpManager','toggleSubButtons','$rootScope', function($scope, $location, $window, $state,resumeFactory,questionnarieFactory,popUpManager,toggleSubButtons,$rootScope) {
			$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			$scope.updateNotifications = function(){
				$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			}
            
            // Checks for the second time enterto page
            $scope.$watch('userType', function(newValue){
                if(newValue === undefined || newValue === null || newValue === '')
                    {
                        var temp = $location.path();
                        
                        if(temp !== '/main' && temp !== '/about' && temp !== '/webResume' && temp !== '/contactUs' && temp !== '/termsAndConditions' && temp !== '/privacyPolicy' && temp !== '/resetPwd' && temp !== '/'  && temp !== '/help'){
                            $state.go('login');
                        }
                    }
	       });

			$scope.onPage = function (states) {
				
				if(states.constructor===Array)
				{
					for(var i=0;i<states.length;i++){
					if(states[i] === $location.path())
					return states[i] === $location.path();
					}
				}
				else
				return states === $location.path();
            };
            
            $scope.onState = function(newState){
	            if(newState === "questionnairesPreview"){
		            if(questionnarieFactory.checkCurrentQuestionnarie()){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "resumePreview"){
		            if(resumeFactory.checkResume(resumeFactory.myResume())){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "questionnaires"){
		            if(!_.isEmpty(questionnarieFactory.ankets())){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "getLink"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }else if(newState === "sendTo"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           	var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }
	            
            }
        }]
    );

})();
