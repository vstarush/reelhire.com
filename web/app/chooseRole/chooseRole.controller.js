(function () {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    .controller('ChooseRoleCTRL', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','resourceRegister','$window',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,resourceRegister,$window) {
            $scope.funcSeeker = function(){
                $scope.userType = 'seeker';                
                resourceRegister.RegisterWithPassword('anonimus'+Date.now(),Date.now(),$scope.userType,"anonimus")
            }
            $scope.funcRecruiter = function(){
                $scope.userType = 'recruter';
                resourceRegister.RegisterWithPassword('anonimus'+Date.now(),Date.now(),$scope.userType,"anonimus")
            }
            $scope.goBack = function(){
	            $window.history.back();
            };
			
        }
      
    ]);
    angular.module('seeMeHireMeApp')
    .controller('ChooseRoleSocialCTRL', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','resourceRegister','$window',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,resourceRegister,$window) {
            $scope.funcSeeker = function(){
                $rootScope.userType = 'seeker';
                resourceRegister.RegisterSocial();
            }
            $scope.funcRecruiter = function(){
                $rootScope.userType = 'recruter';
                resourceRegister.RegisterSocial();
            }
            $scope.goBack = function(){
		        resourceLogin.logOut();	
	        };
			
        }
      
    ])
})();