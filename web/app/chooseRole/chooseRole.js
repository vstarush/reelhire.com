(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('chooseRole', {
                        url: '/chooseRole',
                        templateUrl: 'app/chooseRole/chooseRole.html',
                        controller: 'ChooseRoleCTRL',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
                $stateProvider
                    .state('chooseRoleSocial', {
                        url: '/chooseRole',
                        templateUrl: 'app/chooseRole/chooseRole.html',
                        controller: 'ChooseRoleSocialCTRL',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
            }
        ]);
})();