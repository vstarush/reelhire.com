(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('preview', {
            url: '/preview',
            templateUrl: 'app/preview/preview.html',
            controller: 'PreviewCtrl'
          });
      }]);
      
})();
