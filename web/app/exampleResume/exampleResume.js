(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('exampleResume', {
            url: '/exampleResume',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:true
            },
            resolve: {
	            prepareData: function(resumeFactory)
				{
					return resumeFactory.getExampleData();
				}
            }
          });
      }]);
      
})();
