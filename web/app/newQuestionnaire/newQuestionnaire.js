(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('newQuestionnaire', {
                        url: '/newQuestionnaire',
                        templateUrl: 'app/newQuestionnaire/newQuestionnaire.html',
                        controller: 'NewQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    })
                    .state('previewQuestionnaire', {
                        url: '/previewQuestionnaire',
                        templateUrl: 'app/newQuestionnaire/newQuestionnaire.html',
                        controller: 'NewQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
            }
        ]);
})();