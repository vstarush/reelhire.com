(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('NewQuestionnaireCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','popUpManager','photoUploadFactory',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,popUpManager,photoUploadFactory) {
	        photoUploadFactory.setCurrentFactory(recruiterFactory);
	    	$scope.anket = $rootScope.currentAnket;
	    	$scope.error = recruiterFactory.errorField();
	    	$scope.isFilled = function(textToCheck){
                return textToCheck != undefined && textToCheck.toString().length>0;
            };
            $scope.setCurrentPhoto = function(name,link){
	            if($scope.anket.ispublished == "True") return;
	            $state.go("photoUploadRecruiter");
	            $scope.error.companyLogo = false;
                recruiterFactory.setCurrentPhoto({link:$scope.anket.companyLogo});
            }
            
            $scope.publishclick = function(){
// 	            console.log($scope.error);
	            if(recruiterFactory.checkAnket($scope.anket,$scope.error)  && recruiterFactory.checkRegistration()){  
	                recruiterFactory.publishPhoto($scope.anket.companyLogo).then(
		                function(result){
			                $scope.anket.companyLogo = result;
				            recruiterFactory.publishAnket($scope.anket);
			            },
			            function(err){
				            
			            }
		            ); 
                }
            };
            
            $scope.checkForCurrentPhoto = (function(){
                if(recruiterFactory.getCurrentPhoto().link.length>2){
                    if(recruiterFactory.getCurrentPhoto().name === 'companyLogo'){
                        $scope.anket.companyLogo = recruiterFactory.getCurrentPhoto().link;
                    }
                    recruiterFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentPhoto();
            $scope.textSend = function(){
	            if($rootScope.selectedCandidates != undefined && $rootScope.selectedCandidates.length > 0){
		            return "Save and Send";
	            }
	            return "Save And Get Link";
	        }
	        $scope.isSelectedCandidates = function(){
				if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
					return false;
				}else{
					return true;
				}
			}    
            $scope.send = function(){
	            recruiterFactory.sendAnket();
            } 
	            
        }]);
})();