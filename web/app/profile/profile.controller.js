(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('ProfileCtrl', ['$scope','$rootScope', '$timeout', '$state', '$window', 'resourceLogin','popUpManager','recruiterFactory','questionnarieFactory', function($scope,$rootScope, $timeout, $state, $window, resourceLogin,popUpManager,recruiterFactory,questionnarieFactory) {
                $scope.funcLogOut = function()
				{	
				   resourceLogin.logOut();	
				}
				
				$scope.changePwd = function(){
					$state.go('changePassword')
				}
                $scope.termsAndConditions = function(){
	                $state.go('termsAndConditions')
                }
                $scope.contactUs = function(){
	                $state.go('contactUs')
                }
                $scope.deleteMyProfileName = '';
                $scope.deleteMyProfilePopup = '';
                
                
                if($rootScope.userType === 'seeker'){
                    $scope.deleteMyProfileName = 'I Got Hired';
                    $scope.deleteMyProfilePopup = 'Clicking "OK" will permanently delete your web resume, all your videos, questionnaires and personal data. Are you Sure?';
                }
                else{
                    $scope.deleteMyProfileName = 'Delete My Profile';
                    $scope.deleteMyProfilePopup = 'Clicking "OK" will permanently delete all your questionnaires and video answers to your questionnaires. Are you sure?';
                };
                
                function deleteProfile(){
	                if($rootScope.userType === 'seeker'){
		                questionnarieFactory.deleteAllQuestionnaries();
	                }else{
		                recruiterFactory.deleteAllAnkets();
	                }
	                 var popupMessage = {
						header:'',
						body:'Your data was deleted.',
						btnOneName:"OK"
					};
					
                    popUpManager.popupModal(popupMessage);
                };
                
                $scope.deleteMyProfile = function(){
                    var popupMessage = {
						header:'',
						body:$scope.deleteMyProfilePopup,
						btnOneName:"OK",
						btnTwoName:"Cancel",
						btnOneClick:deleteProfile
					};
					
                    popUpManager.popupModal(popupMessage);
                };
                 
               
        }]
    );

    
})();
