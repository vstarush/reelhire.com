(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('getLinkCtrl', ['$scope','resumeFactory','$rootScope' , function($scope,resumeFactory,$rootScope ) {

			$scope.linkToResume = 'http://reelhire.com/'+$rootScope.idToHash(resumeFactory.serverResume().id,'webResume');
			
			
            $scope.open = function(){
				window.open($scope.linkToResume);
			};
        }]
    );

    
})();
