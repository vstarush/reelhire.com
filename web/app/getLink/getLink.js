(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('getLink', {
            url: '/getLink',
            templateUrl: 'app/getLink/getLink.html',
            controller: 'getLinkCtrl'
          });
      }]);
      
})();
