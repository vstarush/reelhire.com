(function() {

    'use strict';

    var appTutorial = angular.module('seeMeHireMeApp')
    .controller('TutorialCtrl', function($scope,$localStorage,$rootScope,$state,questionnarieFactory,toggleSubButtons,resumeFactory,recruiterFactory){

            $( window ).scroll(function() {
                $('.popover').hide();
            });

            //Check if tutorial was previously turned on or off
            if($localStorage.tutorialIsActive===undefined)
            {
                $localStorage.tutorialIsActive = true;
                $scope.tutorialStatus = 'Off';
            }
            else if ($localStorage.tutorialIsActive===true)
            {
                $scope.tutorialStatus = 'Off';
            }
            else
            {
                $scope.tutorialStatus = 'On';
            }

            //function to turn off or on tutorial
            $scope.toggleTutorial = function(){
                if($localStorage.tutorialIsActive)
                {
                    $localStorage.tutorialIsActive = false;
                    $rootScope.tutorialStatusChanged = $localStorage.tutorialIsActive;
                    $scope.tutorialStatus = 'On';
                }
                else {
                    $localStorage.tutorialIsActive = true;
                    $rootScope.tutorialStatusChanged = $localStorage.tutorialIsActive;
                    $scope.tutorialStatus = 'Off';
                }
            }
            
            //Questionnaire dependency if there are questionnaires or not
            $scope.checkSeekerQuestionnaires = function()
            { 
	            if(!_.isEmpty(questionnarieFactory.ankets()))
                {
                    toggleSubButtons.Enable('seekerQuestionnaires');
                    
                    if($scope.tutorialSeekerQuestionnaires !== '')
                    $scope.tutorialSeekerQuestionnaires = 'Click to see your questionnaires';
                }
                else
                {
                    toggleSubButtons.Disable('seekerQuestionnaires');
                    
					if($scope.tutorialSeekerQuestionnaires !== '')
                    {
	                    $scope.tutorialSeekerQuestionnaires = 'This will become available once you receive a questionnaire from a recruiter';
                    }
                    
                }
                return true;
            }
            
            // Get Link and Send To dependency
            //Enables and disables get Link and Send To based on the published or unpublished resume
            $scope.checkSeekerInformation = function(){
                if(resumeFactory.isEqualWithServer() && resumeFactory.isCurrentVersionPublished())
                {
                    toggleSubButtons.Enable('myResumeGetLink');
                    toggleSubButtons.Enable('myResumeSendTo');
                    if($scope.tutorialSeekerGetLink !== '')
                    {
	                    $scope.tutorialSeekerGetLink = 'Click to get link to your resume';
						$scope.tutorialSeekerSendTo = 'Click to send your resume via e-mail';
                    }
                }
                else
                {
                    toggleSubButtons.Disable('myResumeGetLink');
                    toggleSubButtons.Disable('myResumeSendTo');
                    if($scope.tutorialSeekerGetLink !== '')
                    {
                    	$scope.tutorialSeekerGetLink = 'Publish your resume to get a link to it';
						$scope.tutorialSeekerSendTo = 'Publish your resume to send it via e-mail';
                    }
                }

                return true;
            }
            
            
            
            
            // Watches for is resume is filled or not to show Preview
            $scope.$watch('resumeScopeShare',function(newValue,oldValue){

                if(resumeFactory.checkResumeIsFilled(newValue))
                {
                    toggleSubButtons.Enable('myResumePreview');
                    if($scope.tutorialSeekerPreview !== '')
                    {
	                    $scope.tutorialSeekerPreview = 'Click to preview your Resume';
                    }
                }
                else
                {
                    toggleSubButtons.Disable('myResumePreview');
                    if($scope.tutorialSeekerPreview !== '')
                    {
	                    $scope.tutorialSeekerPreview = 'Fill all fields to preview your Resume';
                    }
                }
            },true);
            
            
            //checks if any candidates are selected
            $scope.$watch('selectedCandidates',function(newValue,oldValue){
	            if(!_.isEmpty(newValue))
	            {
		            toggleSubButtons.Enable('emailToSelectedClass');
		            toggleSubButtons.Enable('sendSelectedClass');
					toggleSubButtons.Enable('newQuestionnaireClass');
					if($scope.tutorialRecruiterEmailToSelected !== '') 
					{
						$scope.tutorialRecruiterEmailToSelected = 'Select candidates to send them an email';
						$scope.tutorialRecruiterSendSelected = 'Select candidates and include them in your email';
						$scope.tutorialRecruiterSendNewQuestionnaire = 'Select candidates to send them a new questionnaires';
					}
	            }
	            else
	            {
		           toggleSubButtons.Disable('emailToSelectedClass');
		           toggleSubButtons.Disable('sendSelectedClass');
		           toggleSubButtons.Disable('newQuestionnaireClass');
		           if($scope.tutorialRecruiterEmailToSelected !== '')
		           {
			           $scope.tutorialRecruiterEmailToSelected = 'Select atleast one candidate';
					   $scope.tutorialRecruiterSendSelected = 'Select atleast one candidate';
					   $scope.tutorialRecruiterSendNewQuestionnaire = 'Select atleast one candidate';
		           }
	            }
            },true);
            
            //check for favourites are there any
            //PROBLEM I need to call favorites here
            $scope.$watch('favorites',function(newValue,oldValue){
	        	if($rootScope.favorites.length>0)
	        	{
		        	toggleSubButtons.Enable('favouritesClass');
		        	$scope.tutorialRecruiterFavorites = 'See all the people you checked as favorites';
	        	}
	        	else
	        	{
		        	toggleSubButtons.Disable('favouritesClass');
		        	$scope.tutorialRecruiterFavorites = 'Add candidates to favourites to activate this folder';
	        	}
	        },true);
            

            //watching if tutorial has been turned off or on
            $scope.$watch('tutorialStatusChanged',function(){

                if ($localStorage.tutorialIsActive) {
                    $scope.tutorialOnBtnContinue = 'Continue and register later';
                    $scope.tutorialOnChoosingSeeker = 'Choose, if you are a job seeker';
                    $scope.tutorialOnChoosingRecruiter = 'Or, are you a recruiter?';
                    $scope.tutorialOnAddingAPhoto = 'Click here to add a photo';
                    $scope.tutorialOnAddingAVideo = 'Click here to answer the record an answer';
                    $scope.tutorialMergedVideo = 'All your videos are merged to ';
                    $scope.tutorialOnPublish = 'This will make a public version of your resume';
                    $scope.tutorialOnRestore = 'This will take your resume back to the last published version';
                    $scope.tutorialSeekerQuestionnaires = 'testfield';
                    $scope.tutorialSeekerPreview = 'testfield';
                    $scope.tutorialSeekerGetLink = 'testfield';
					$scope.tutorialSeekerSendTo = 'testfield';
                    $scope.tutorialSeekerExample = 'Click to see what you will receive after publishing you resume';
                    $scope.tutorialSeekerNewQuestionnaire = 'Click here to see your questionnaire';
                    $scope.tutorialQuestionnairePosition = 'This is the position you are applying for';
                    $scope.tutorialQuestionnaireVideo = 'This a a question asked by recruiter. Click to add a video answer';
                    $scope.tutorialQuestionnaireSend = 'This will send your resume to a recruiter. Once sent, it can not be changed';
                    $scope.tutorialMergedVideo = 'Your videos were combined for recruiter convenience';
                    $scope.tutorialRecruiterCreateNewQuestionnaire = 'Click to create new questionnaire';
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to see responses to your questionnaire';
                    $scope.tutorialRecruiterCopyQuestionnaire = 'Click to copy your questionnaire';
                    $scope.tutorialRecruiterOnAddingAPhoto = 'Click here to add your company photo';
                    $scope.tutorialRecruiterBasicInformation = 'You will receive all this info about a candidate automatically';
                    $scope.tutorialRecruiterQuestions = 'Ask your question and receive a video answer';
                    $scope.tutorialRecruiterQuestionairePublish = 'This will publish your questionnaire and you will be able to send it';
                    $scope.tutorialRecruiterSeeCandidateQuestionnaire = 'Click to see job seeker';
                    $scope.tutorialRecruiterQuestionairePreview = 'Click to view your questionnaire';
                    $scope.tutorialRecruiterAddToFavourites = 'Add candidate to Favourites';
                    $scope.tutorialRecruiterEmailToSelected = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSendSelected = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSendNewQuestionnaire = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSeeCandidate = 'Click to see candidate\'s resume';
                    $scope.tutorialRecruiterSearchAll = 'Search all ReelHire base of job seekers';
                    $scope.tutorialRecruiterFavorites = 'Add candidates to favourites to activate this folder';
                    $scope.tutorialRecruiterPreviewQuestionnaire = 'Click to preview your questionnaire';
                    $scope.tutorialRecruiterLinkQuestionnaire = 'This is an external link to your questionnaire';
                    $scope.tutorialRecruiterSearchJobseekers = 'Search ReelHire user base and send them this questionnaire';
                    $scope.tutorialRecruiterQuestionaireInstantSend = 'Click to send questionnaire to job seekers';
                }
                else{
                    $scope.tutorialOnBtnContinue = '';
                    $scope.tutorialOnChoosingSeeker = '';
                    $scope.tutorialOnChoosingRecruiter = '';
                    $scope.tutorialOnAddingAPhoto = '';
                    $scope.tutorialOnAddingAVideo = '';
                    $scope.tutorialOnPublish = '';
                    $scope.tutorialOnRestore = '';
                    $scope.tutorialSeekerQuestionnaires = '';
                    $scope.tutorialSeekerPreview = '';
                    $scope.tutorialSeekerGetLink = '';
                    $scope.tutorialSeekerSendTo = '';
                    $scope.tutorialSeekerExample = '';
                    $scope.tutorialSeekerNewQuestionnaire = '';
                    $scope.tutorialQuestionnairePosition = '';
                    $scope.tutorialQuestionnaireVideo = '';
                    $scope.tutorialQuestionnaireSend = '';
                    $scope.tutorialMergedVideo = '';
                    $scope.tutorialRecruiterCreateNewQuestionnaire = '';
                    $scope.tutorialRecruiterGoInsideQuestionnaire = '';
                    $scope.tutorialRecruiterCopyQuestionnaire = '';
                    $scope.tutorialRecruiterOnAddingAPhoto = '';
                    $scope.tutorialRecruiterBasicInformation = '';
                    $scope.tutorialRecruiterQuestions = '';
                    $scope.tutorialRecruiterQuestionairePublish = '';
                    $scope.tutorialRecruiterSeeCandidateQuestionnaire = '';
                    $scope.tutorialRecruiterQuestionairePreview = '';
                    $scope.tutorialRecruiterAddToFavourites = '';
                    $scope.tutorialRecruiterEmailToSelected = '';
                    $scope.tutorialRecruiterSendSelected = '';
                    $scope.tutorialRecruiterSendNewQuestionnaire = '';
                    $scope.tutorialRecruiterSeeCandidate = '';
                    $scope.tutorialRecruiterSearchAll = '';
                    $scope.tutorialRecruiterFavorites = '';
                    $scope.tutorialRecruiterPreviewQuestionnaire = '';
                    $scope.tutorialRecruiterLinkQuestionnaire = '';
                    $scope.tutorialRecruiterSearchJobseekers = '';
                    $scope.tutorialRecruiterQuestionaireInstantSend = '';
                }
            });
        
        $scope.$watch('selectedCandidates',function(newValue,oldValue){
                if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to see responses to your questionnaire';
                }
                else{
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to preview your questionnaire';
                }
            });


    });
})();


