(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('RecruiterQuestionnairesCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','popUpManager',
            function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,popUpManager) {
	            
                $scope.$watch('firstLoginType',function(newValue,oldValue){
                   if(newValue!== null && newValue != undefined)
                        {
                           popUpManager.welcomeRecruiter();
                        }
               });
                
	            $scope.ankets = recruiterFactory.ankets();
	            $scope.anketId = function(anket){
		            return -Number(anket.id);
	            }
				$scope.copyAnket = function(anket){
					recruiterFactory.copyAnket(anket);
				};
                
				$scope.isSelectedCandidates = function(){
					if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
						return false;
					}else{
						return true;
					}
				}
				$scope.sendAnket = function(anket){
// 					console.log(anket);
					$rootScope.currentAnket = anket;
					recruiterFactory.sendAnket();
				};
				$scope.deleteAnket = function(anket){
					var popupMessage = {
						header:'Are you sure?',
						body:'Do you want to delete the Questionnaire for the position "' + anket.companyPosition + '"?',
						btnOneName:'Delete',
						btnTwoName:'Cancel',
						btnOneClick:function(){
							recruiterFactory.deleteAnket(anket);
						},
						btnTwoClick:function(){
							
						}
					}
	
					popUpManager.popupModal(popupMessage);
					
				};
				$scope.newAnket = function(){
					$rootScope.currentAnket = recruiterFactory.newAnket();
					$state.go("newQuestionnaire");
				};
				$scope.ifEmpty = function(text,ifEmpty){
					if(text.length > 0) return "";
					return ifEmpty;
				};
				$scope.openAnket = function(anket){
					$rootScope.currentAnket = anket;
					
					if(anket.ispublished === "True"){
						if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
							$state.go("insideQuestionnaire");
						}else{
							$state.go("previewQuestionnaire");
						}
					}else{
						$state.go("newQuestionnaire");
					}
				}
        }]);
})();