(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('recruiterQuestionnaires', {
                        url: '/recruiterQuestionnaires',
                        templateUrl: 'app/recruiterQuestionnaires/recruiterQuestionnaires.html',
                        controller: 'RecruiterQuestionnairesCtrl',
                        resolve: {
	                        stateOutput: function ($state)
	                        {
		                      
	                        }
                        }
                    });
            }
        ]);
})();