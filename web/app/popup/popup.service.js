(function(){
	'use strict';
	
	angular.module('seeMeHireMeApp')
	.service('popUpManager', function($uibModal){
		return{
			popupModal: function(modalMessageObject){
			    $uibModal.open({
			      templateUrl: 'app/popup/popup.html',
			      controller: 'testModalCtrl',
			      backdrop: 'static',
				  keyboard: false,
			      resolve: {
				    popupMessage:function(){
					    return modalMessageObject;
				    }
			      }
			    });   
		    },
            welcomeJobSeeker:function()
            {
                $uibModal.open({
                    templateUrl:'app/popup/welcomeJobSeeker.html',
                    controller: 'WelcomePopUpController',
                    backdrop: 'static',
                    keyboard: false
                });
            },
            welcomeRecruiter:function()
            {
                $uibModal.open({
                    templateUrl:'app/popup/welcomeRecruiter.html',
                    controller: 'WelcomePopUpController',
                    backdrop: 'static',
                    keyboard: false
                });
            }
		};
	})
    .controller('WelcomePopUpController',function($scope, $rootScope, $uibModalInstance){
        $scope.btnClickOK = function(){
            $rootScope.firstLoginType = null;
            $uibModalInstance.close();
        }
    })
	.controller('testModalCtrl',['$scope','$uibModalInstance','popupMessage',function($scope, $uibModalInstance, popupMessage){

// template for creating a modal popup
// by default btnOne closes popup
		$scope.popupText = {
			header:'',
			body:'',
			btnOneName:'',
			btnTwoName:'',
			btnOneClick:'function',
			btnTwoClick:'function'
		}
		
		//displaying second button
		$scope.secondButton = true;
		
		$scope.popupText.header = popupMessage.header;
		$scope.popupText.body = popupMessage.body;
		$scope.popupText.btnOneName = popupMessage.btnOneName;
		$scope.popupText.btnTwoName = popupMessage.btnTwoName;
		
		if($scope.popupText.btnTwoName===undefined)
		{
			$scope.secondButton = false;
		}


		$scope.btnOneClick = function(){
			
			if(typeof popupMessage.btnOneClick === 'function')
			popupMessage.btnOneClick();
			
			$uibModalInstance.close();
		}
		
		$scope.btnTwoClick = function(){
			
			if(typeof popupMessage.btnTwoClick === 'function')
			popupMessage.btnTwoClick();
			
			$uibModalInstance.close();
		}
	}])
})();






