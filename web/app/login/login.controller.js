(function () {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    
	    .run(function(systemFactory,cookieManager,$state,resourceLogin,resumeFactory,recruiterFactory){
            systemFactory.getSystemAnketID().then(function(result){
                    systemFactory.getSystemAnket().then(function(result2){
	                    
	                });
	        });
		    systemFactory.getMailConfig();
	    })
		 .factory('resourceLogin', function($resource,$http,$state,cookieManager,resumeFactory,systemFactory,questionnarieFactory,popUpManager,$q,$rootScope,recruiterFactory){
			    
			    $rootScope.userType = '';
			    $rootScope.userEmail = '';

			    var afterLoginActions = function(responce){
				    
				    systemFactory.setUserType(responce.data.user_type);
				    systemFactory.setMd5(responce.data.md5);
				    
				    
				    $rootScope.userType = responce.data.user_type;
				    $rootScope.userId = responce.data.id;
				    $rootScope.userEmail = responce.data.email.replace("_g", "");
				    if($rootScope.userEmail.indexOf("@") == -1) $rootScope.userEmail = "";
				    
				    if(responce.data.user_type == "seeker" || responce.data.user_type == ""){
					    $rootScope.userType = "seeker";
                        resumeFactory.getSysAnketID().then(function(){
                           resumeFactory.getAllResume().then(
                               function(responce){
	                               questionnarieFactory.setAnketsAnswers();
	                               updateDataFromServer();
								   $state.go('resumeEdit');
                               }
                           );
                        });
					}
					else if(responce.data.user_type == "recruter" || responce.data.user_type == "recruiter" ){ //recruiter -- tak u seregi, tak chto ne meniaj
						$rootScope.userType = "recruter";
						recruiterFactory.getListOfFavorites();
						recruiterFactory.getRecruiterFolder().then(function(){
							recruiterFactory.getListOfAnkets().then(function(){
								if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
									
									$state.go('searchAll');
								}else{
									$state.go('recruiterQuestionnaires');
								}
							});
						});
						
					}
				};
				
                var md5 = "none";
                var _email = "";
                function updateDataFromServer(){
	                setTimeout(function() {
		                if($rootScope.userType != "seeker") return;
		                updateDataFromServer();
						resumeFactory.updateAllResume().then(
                               function(responce){
	                               questionnarieFactory.setAnketsAnswers();
                               }

                           );
			    	}, 90000);
                }
                function resetPwdConfirm(){
	                
	                var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'resetsend/' + _email
					}; 
				    return $http(requestData).then(function(responce){
					    if(responce.data == "1"){
							var popupMessage = {
								header:'',
								body:'Your password was succsessfuly send to your email.',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
							$state.go('login');
						}else{
							var popupMessage = {
								header:'Something went wrong',
								body:'Your reset code was not sent. Try again',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}
					});
	            }
                function resetPwdPopup(){
	                var popupMessage = {
						header:'',
						body:'Do you want to reset the password for account '+_email+'? We will send you new password by email.',
						btnTwoName:'Cancel',
						btnOneName:'Reset',
						btnOneClick: resetPwdConfirm
					}
	
					popUpManager.popupModal(popupMessage);
                }
                var currentPwd = "";
                var currentEmail = "";
		    return{
			    currentPwd : function(){ return currentPwd; },
			    currentEmail : function(){ return currentEmail; },
			 	loginWithPassword: function(email,pwd,isSocial){
				 	
				    var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'user/?email=' + email + '&pwd=' + pwd + '&md5=',
						headers: {
						   'skey': 123
						}
					}; 
				    return $http(requestData).then(function(responce){
					    if(responce.data.md5){
                            md5 = responce.data.md5;
							cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							//console.log(responce);
							
							afterLoginActions(responce);
						}else if(isSocial){
							currentPwd = pwd;
							currentEmail = email;
							$state.go('chooseRoleSocial');
						}
						else{
							
							var popupMessage = {
								header:'Have you already registered?',
								body:'Wrong login data. Try again',
								btnTwoName:'OK',
								btnOneName:'Forgot Password',
								btnOneClick: resetPwdPopup
							}
			
							popUpManager.popupModal(popupMessage);
							_email = email;
							
							$state.go('login');
						}
					});
				},
			    
			    loginWithMd5: function(md5){
				    var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'user/?email=&pwd=&md5=' + md5,
						headers: {
						   'skey': 123
						}
					};
					//console.log(requestData); 
				    return $http(requestData).then(function(responce){
					    if(responce.data.md5){
                            md5 = responce.data.md5;
                            cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							afterLoginActions(responce);
						}else{
							md5 = "";
							systemFactory.setMd5("");
							cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							var popupMessage = {
								header:'Have you already registered?',
								body:'Wrong login data. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
							
							
							$state.go('login');
						}
				    });
			    },
			    logOut: function(){
                    var auth2 = gapi.auth2.getAuthInstance();
                    gapi.auth.signOut();
                    auth2.signOut().then(function () {
                      	cookieManager.putCookies('personObject', JSON.stringify(""));
	                    systemFactory.setMd5("");
	                    currentPwd = "";
						currentEmail = "";
	                    $rootScope.userType = "";
	                    $rootScope.userId = "";
	                    $rootScope.userEmail = '';
	                    
	                    resumeFactory.resetResume();
                         $state.go('login');
                    });
			    },
                md5: function(){
                    return md5;
                },
                md5Pwd: function(pwd){
                    var text = pwd.replace("@","huy").replace("b","u4O").replace("v","pD9d")
                    .replace("h","Hud8").replace("e","p0D").replace("o","M").replace("p","fD9d").
                    replace("1","dE").replace("9","Oi87").replace("t","3@");
                    var hash = CryptoJS.MD5(text);
                    var pwdGoogle = hash.toString();
                    var length = pwdGoogle.length>12?12:pwdGoogle.length ;
		            pwdGoogle = pwdGoogle.substring(0,length);
                    return pwdGoogle;
                },
                
		    
		    }; 
	    
    	})
        .controller('LoginCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','systemFactory',
            function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,systemFactory) {
                 if($rootScope.checkIfMobileNoPopup()){
	                $state.go('main');
	                return;
                }
                var data = JSON.parse(cookieManager.getCookies('personObject'));
                //wait till system anket id will load and then check if user already logged in
				
                systemFactory.getSystemAnketID().then(function(result){
                    systemFactory.getSystemAnket().then(function(result2){
                        if(checkPreLogin()){
                            // if user logged in, then we load resumeEdit for seeker and questionnarie menu for recruter 
                            $state.go('loading');//---------------------------------------------------------------(activate)
                            resourceLogin.loginWithMd5(data.md5);

                        }else{
                            //user is not logged in, so we do nothing and login state will load
                            //console.log('here');
                        }
                    });
                });

                var checkPreLogin = function(){
	                
	                if(data.md5){
		                if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId != ""){
			                if(data.user_type == 'seeker'){
				                return true;
			                }else{
				                //popup login with seeker account
				                var popupMessage = {
									header:'',
									body:'To fill in the Questionnaire, log in with your Job Seeker account and click the link again.',
									btnOneName:'OK'
								}
								popUpManager.popupModal(popupMessage);
								$rootScope.outSideAnketId = "";
				                return false;
			                }
			            };
			            if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
				            if(data.user_type == 'seeker'){
					            var popupMessage = {
									header:'',
									body:'To open Resume in the ReelHire app, log in with your Recruiter account and click the link again.',
									btnOneName:'OK'
								}
								popUpManager.popupModal(popupMessage);
								$rootScope.outsideResumeId = "";
				                return false;
			                }else{				                
				                return true;
			                }
			            }
	                }else{
		                if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId != ""){
			                var popupMessage = {
								header:'',
								body:'To fill in the Questionnaire, log in with your Job Seeker account and click the link again.',
								btnOneName:'OK'
							}
							popUpManager.popupModal(popupMessage);
							$rootScope.outSideAnketId = "";
						}
						if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
				            var popupMessage = {
								header:'',
								body:'To open Resume in the ReelHire app, log in with your Recruiter account and click the link again.',
								btnOneName:'OK'
							}
							popUpManager.popupModal(popupMessage);
							$rootScope.outsideResumeId = "";
			            }
		                return false;
	                }
	                return true;
                }
                
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
				$scope.email = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
				$scope.cookieObjectLogin = 'personObject';
				$scope.cookiePublicResume = cookieManager.systemResumeObject(); //makes sure we have the same cookie object in every file
				$scope.publicResume = '';
				
				
				$('#loginFieldEmail').keypress(function(e){
			      if(e.keyCode==13)
			      	$('.loginBtnLogin').click();
			    });
			    
			    $('#loginFieldPassword').keypress(function(e){
			      if(e.keyCode==13)
			      	$('.loginBtnLogin').click();
			    });
				
				$scope.funcLogin = function()
				{	
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Please, check your email',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
                        
                        //alert('Please check your email');
                        return;
                    }
                    if($scope.password.text.length<4){
                        $scope.password.text = '';
                        $scope.password.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
                        
                        //alert('Your password should be at least 5 characters');
                        return;
                    }
					$state.go('loading');//-----------------------------------------------------------------------------(activate)
					resourceLogin.loginWithPassword($scope.email.text,$scope.password.text);
				}
				$scope.funcRegister = function()
				{	
					$rootScope.userEmail = "";
					$state.go('register');
				}
				$scope.funcChooseRole = function()
				{	
					$state.go('chooseRole');
				}
				$scope.cookieCheck = function()
				{
					//$scope.temp = JSON.parse(cookieManager.getCookies($scope.cookieObjectLogin));
					$scope.temp = JSON.parse(cookieManager.getCookies($scope.cookieObjectLogin));
//					console.log($scope.temp);
				}
                
                
                function statusChangeCallback(response) {
                    if (response.status === 'connected') {
                        resourceLogin.loginWithPassword(response.authResponse.userID,response.authResponse.userID,true);
                    } else if (response.status === 'not_authorized') {
//                        console.log('Not Authorized');
                    } else {
//                        console.log('Not Logged-in');
                    }
                }
                       
                $scope.fblogin = function(){
						FB.login(function(response) {
						 	window.checkLoginState(response);
						}, {scope: 'public_profile,email'});
					}

				window.checkLoginState = function(){
                    FB.getLoginStatus(function(response) {
                        statusChangeCallback(response);
                    });
                };

                window.fbAsyncInit = function() {
                    if ($scope.fbAsyncInit || window.FB === undefined) 
                        return;

                    FB.init({
                        appId      : '461446334062271',
                        cookie     : true, 
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    $scope.fbAsyncInit = true;
                };
                $scope.fbAsyncInit = false;
                fbAsyncInit();
               
                // Load the SDK asynchronously
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                
                $scope.options = {
	                'width':175,
	                'longtitle': true,
	                'theme': 'dark',
	                'cookie_policy': 'single_host_origin',
                    'onsuccess': function(response) {
                      	onSignInGoogle(response.getBasicProfile().getEmail());
                    }
                  }
                function onSignInGoogle(googleEmail) {
	                var pwdGoogle = resourceLogin.md5Pwd(googleEmail); 
	                //console.log("onSignUpGoogle1");
	                resourceLogin.loginWithPassword(googleEmail+"_g",pwdGoogle,true);
	            }
	            function storageEnabled() {
				    try {
				        localStorage.setItem("__test", "data");
				    } catch (e) {
				        if (/QUOTA_?EXCEEDED/i.test(e.name)) {
					        
					        var popupMessage = {
								header:'Service ReelHire will not work in private mode!',
								body:'Please use normal web browser mode',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
				            
				            //alert("Service ReelHire will not work in private mode. Please use normal web browser mode.");
				        }
				    }
				}
				storageEnabled();
            }
          
        ])
        .directive('googleSignInButton',['resourceLogin', function(resourceLogin) {

            return {
              scope: {
                buttonId: '@',
                options: '&'
              },
              template: '<div></div>',
              link: function(scope, element, attrs) {
                var div = element.find('div')[0];
                div.id = attrs.buttonId;
                gapi.signin2.render(div.id, scope.options()

	            ); //render a google button, first argument is an id, second options
              }
            };
          }]);
})();