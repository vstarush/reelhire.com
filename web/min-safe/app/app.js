(function() {
	
    'use strict';
    
    angular.element(document).ready([function() {
    	angular.bootstrap(document, ['seeMeHireMeApp']);
    }]);
    
    var appApp = angular.module(
        'seeMeHireMeApp', [
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ui.router',
            'ui.bootstrap',
            'angularMoment',
            'ngAnimate',
            'ngStorage'
        ]
    )

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$urlMatcherFactoryProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $urlMatcherFactoryProvider) {
        $urlRouterProvider
        	.otherwise('/');

        $stateProvider
          	.state(
            'checkpoint', {
                url: '/',
                controller: ['$state', function( $state ) {
                    var defaultState = 'main';
                        $state.transitionTo(defaultState);
                }]
            }
        );

        $urlMatcherFactoryProvider.strictMode(false);
        $locationProvider.html5Mode(true);
    }])

    .controller('AppCtrl', ['$rootScope','$scope','$state','resourceLogin','$location','popUpManager', function($rootScope, $scope, $state,resourceLogin,$location,popUpManager){
        
	   
	    var urlParamsHandler = function(){ 
		    var urlParams = $location.path().replace('/','').split('/');
	        if(urlParams[0] == "resetPwd"){
				$rootScope.resetEmail = urlParams[1];
				$rootScope.resetCode = urlParams[2];
			}
	        if($rootScope.hashToId(urlParams[0],'webResume').length > 0){
			    $rootScope.outSideResumeHash = urlParams[0];
			    $rootScope.outSideResumeMd5 = urlParams[1];
			    $scope.ifWebResume = true;
		    }else if($rootScope.hashToId(urlParams[0],'webAnket').length > 0){
			    $rootScope.outSideAnketId = $rootScope.hashToId(urlParams[0],'webAnket');
		    }
	        
        	setTimeout(function () {
	        	if($rootScope.resetEmail != undefined && $rootScope.resetEmail != ""){
					$state.go("resetPwd");
				}
			},300);
            
            if($rootScope.outSideResumeHash != undefined && $rootScope.outSideResumeHash!=""){
            	$location.path('/webResume').search($rootScope.outSideResumeHash+'&'+$rootScope.outSideResumeMd5);
				$state.go("webResume");
			    $rootScope.outSideResumeHash = "";
			    $scope.ifWebResume = true;
			}
            if(urlParams === 'help'){
	            setTimeout(function(){
	            	$state.go("help");
	            },0);
            }
			if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId!=""){
				if( $rootScope.checkIfMobile()){
					$window.location.href = "reelhire://?webanket="+$rootScope.outSideAnketId;
					$rootScope.outSideAnketId = "";
				}else{
					setTimeout(function(){
                        $state.go("login");
                    },0);
				}
			}
		}(); 
        
        
        
        $scope.ifWebResume = function()
        {
            return $rootScope.isWebResume;
        };
        
	    
        $scope.checkIfMobile = function()
        {
            return $rootScope.checkIfMobileNoPopup();
        };
        $scope.ifMobile = $rootScope.checkIfMobileNoPopup();   
	    
	    $scope.$watch('userType', function(newValue){
		    $scope.type = newValue;
	    });
	    
    }]);
	
	appApp.service('cookieManager', ['$cookies', function($cookies){
		
		this.getCookies = function(sObj){
                if(!$cookies.get(sObj)) return '{}';
				return $cookies.get(sObj);
			},
		this.putCookies = function(sObj, data){
				$cookies.put(sObj, data);
			},
        this.clearCookies = function(sObj){
				$cookies.remove(sObj);
			},
		this.systemResumeObject = function(){
				return 'publicResumeObject';
			}
		
	}]);


})();
(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('LoadingCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout',
            function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout) {

        }]);
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('loading', {
                        url: '/loading',
                        templateUrl: 'app/loading/loading.html',
                        controller: 'LoadingCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
            }
        ]);
})();
(function () {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    
	    .run(['systemFactory', 'cookieManager', '$state', 'resourceLogin', 'resumeFactory', 'recruiterFactory', function(systemFactory,cookieManager,$state,resourceLogin,resumeFactory,recruiterFactory){
            systemFactory.getSystemAnketID().then(function(result){
                    systemFactory.getSystemAnket().then(function(result2){
	                    
	                });
	        });
		    systemFactory.getMailConfig();
	    }])
		 .factory('resourceLogin', ['$resource', '$http', '$state', 'cookieManager', 'resumeFactory', 'systemFactory', 'questionnarieFactory', 'popUpManager', '$q', '$rootScope', 'recruiterFactory', function($resource,$http,$state,cookieManager,resumeFactory,systemFactory,questionnarieFactory,popUpManager,$q,$rootScope,recruiterFactory){
			    
			    $rootScope.userType = '';
			    $rootScope.userEmail = '';

			    var afterLoginActions = function(responce){
				    
				    systemFactory.setUserType(responce.data.user_type);
				    systemFactory.setMd5(responce.data.md5);
				    
				    
				    $rootScope.userType = responce.data.user_type;
				    $rootScope.userId = responce.data.id;
				    $rootScope.userEmail = responce.data.email.replace("_g", "");
				    if($rootScope.userEmail.indexOf("@") == -1) $rootScope.userEmail = "";
				    
				    if(responce.data.user_type == "seeker" || responce.data.user_type == ""){
					    $rootScope.userType = "seeker";
                        resumeFactory.getSysAnketID().then(function(){
                           resumeFactory.getAllResume().then(
                               function(responce){
	                               questionnarieFactory.setAnketsAnswers();
	                               updateDataFromServer();
								   $state.go('resumeEdit');
                               }
                           );
                        });
					}
					else if(responce.data.user_type == "recruter" || responce.data.user_type == "recruiter" ){ //recruiter -- tak u seregi, tak chto ne meniaj
						$rootScope.userType = "recruter";
						recruiterFactory.getListOfFavorites();
						recruiterFactory.getRecruiterFolder().then(function(){
							recruiterFactory.getListOfAnkets().then(function(){
								if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
									
									$state.go('searchAll');
								}else{
									$state.go('recruiterQuestionnaires');
								}
							});
						});
						
					}
				};
				
                var md5 = "none";
                var _email = "";
                function updateDataFromServer(){
	                setTimeout(function() {
		                if($rootScope.userType != "seeker") return;
		                updateDataFromServer();
						resumeFactory.updateAllResume().then(
                               function(responce){
	                               questionnarieFactory.setAnketsAnswers();
                               }

                           );
			    	}, 90000);
                }
                function resetPwdConfirm(){
	                
	                var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'resetsend/' + _email
					}; 
				    return $http(requestData).then(function(responce){
					    if(responce.data == "1"){
							var popupMessage = {
								header:'',
								body:'Your password was succsessfuly send to your email.',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
							$state.go('login');
						}else{
							var popupMessage = {
								header:'Something went wrong',
								body:'Your reset code was not sent. Try again',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}
					});
	            }
                function resetPwdPopup(){
	                var popupMessage = {
						header:'',
						body:'Do you want to reset the password for account '+_email+'? We will send you new password by email.',
						btnTwoName:'Cancel',
						btnOneName:'Reset',
						btnOneClick: resetPwdConfirm
					}
	
					popUpManager.popupModal(popupMessage);
                }
                var currentPwd = "";
                var currentEmail = "";
		    return{
			    currentPwd : function(){ return currentPwd; },
			    currentEmail : function(){ return currentEmail; },
			 	loginWithPassword: function(email,pwd,isSocial){
				 	
				    var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'user/?email=' + email + '&pwd=' + pwd + '&md5=',
						headers: {
						   'skey': 123
						}
					}; 
				    return $http(requestData).then(function(responce){
					    if(responce.data.md5){
                            md5 = responce.data.md5;
							cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							//console.log(responce);
							
							afterLoginActions(responce);
						}else if(isSocial){
							currentPwd = pwd;
							currentEmail = email;
							$state.go('chooseRoleSocial');
						}
						else{
							
							var popupMessage = {
								header:'Have you already registered?',
								body:'Wrong login data. Try again',
								btnTwoName:'OK',
								btnOneName:'Forgot Password',
								btnOneClick: resetPwdPopup
							}
			
							popUpManager.popupModal(popupMessage);
							_email = email;
							
							$state.go('login');
						}
					});
				},
			    
			    loginWithMd5: function(md5){
				    var requestData = {
					    method: 'GET',
						url: systemFactory.serverUrl()+'user/?email=&pwd=&md5=' + md5,
						headers: {
						   'skey': 123
						}
					};
					//console.log(requestData); 
				    return $http(requestData).then(function(responce){
					    if(responce.data.md5){
                            md5 = responce.data.md5;
                            cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							afterLoginActions(responce);
						}else{
							md5 = "";
							systemFactory.setMd5("");
							cookieManager.putCookies('personObject', JSON.stringify(responce.data));
							var popupMessage = {
								header:'Have you already registered?',
								body:'Wrong login data. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
							
							
							$state.go('login');
						}
				    });
			    },
			    logOut: function(){
                    var auth2 = gapi.auth2.getAuthInstance();
                    gapi.auth.signOut();
                    auth2.signOut().then(function () {
                      	cookieManager.putCookies('personObject', JSON.stringify(""));
	                    systemFactory.setMd5("");
	                    currentPwd = "";
						currentEmail = "";
	                    $rootScope.userType = "";
	                    $rootScope.userId = "";
	                    $rootScope.userEmail = '';
	                    
	                    resumeFactory.resetResume();
                         $state.go('login');
                    });
			    },
                md5: function(){
                    return md5;
                },
                md5Pwd: function(pwd){
                    var text = pwd.replace("@","huy").replace("b","u4O").replace("v","pD9d")
                    .replace("h","Hud8").replace("e","p0D").replace("o","M").replace("p","fD9d").
                    replace("1","dE").replace("9","Oi87").replace("t","3@");
                    var hash = CryptoJS.MD5(text);
                    var pwdGoogle = hash.toString();
                    var length = pwdGoogle.length>12?12:pwdGoogle.length ;
		            pwdGoogle = pwdGoogle.substring(0,length);
                    return pwdGoogle;
                },
                
		    
		    }; 
	    
    	}])
        .controller('LoginCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','systemFactory',
            function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,systemFactory) {
                 if($rootScope.checkIfMobileNoPopup()){
	                $state.go('main');
	                return;
                }
                var data = JSON.parse(cookieManager.getCookies('personObject'));
                //wait till system anket id will load and then check if user already logged in
				
                systemFactory.getSystemAnketID().then(function(result){
                    systemFactory.getSystemAnket().then(function(result2){
                        if(checkPreLogin()){
                            // if user logged in, then we load resumeEdit for seeker and questionnarie menu for recruter 
                            $state.go('loading');//---------------------------------------------------------------(activate)
                            resourceLogin.loginWithMd5(data.md5);

                        }else{
                            //user is not logged in, so we do nothing and login state will load
                            //console.log('here');
                        }
                    });
                });

                var checkPreLogin = function(){
	                
	                if(data.md5){
		                if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId != ""){
			                if(data.user_type == 'seeker'){
				                return true;
			                }else{
				                //popup login with seeker account
				                var popupMessage = {
									header:'',
									body:'To fill in the Questionnaire, log in with your Job Seeker account and click the link again.',
									btnOneName:'OK'
								}
								popUpManager.popupModal(popupMessage);
								$rootScope.outSideAnketId = "";
				                return false;
			                }
			            };
			            if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
				            if(data.user_type == 'seeker'){
					            var popupMessage = {
									header:'',
									body:'To open Resume in the ReelHire app, log in with your Recruiter account and click the link again.',
									btnOneName:'OK'
								}
								popUpManager.popupModal(popupMessage);
								$rootScope.outsideResumeId = "";
				                return false;
			                }else{				                
				                return true;
			                }
			            }
	                }else{
		                if($rootScope.outSideAnketId != undefined && $rootScope.outSideAnketId != ""){
			                var popupMessage = {
								header:'',
								body:'To fill in the Questionnaire, log in with your Job Seeker account and click the link again.',
								btnOneName:'OK'
							}
							popUpManager.popupModal(popupMessage);
							$rootScope.outSideAnketId = "";
						}
						if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != ""){
				            var popupMessage = {
								header:'',
								body:'To open Resume in the ReelHire app, log in with your Recruiter account and click the link again.',
								btnOneName:'OK'
							}
							popUpManager.popupModal(popupMessage);
							$rootScope.outsideResumeId = "";
			            }
		                return false;
	                }
	                return true;
                }
                
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
				$scope.email = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
				$scope.cookieObjectLogin = 'personObject';
				$scope.cookiePublicResume = cookieManager.systemResumeObject(); //makes sure we have the same cookie object in every file
				$scope.publicResume = '';
				
				
				$('#loginFieldEmail').keypress(function(e){
			      if(e.keyCode==13)
			      	$('.loginBtnLogin').click();
			    });
			    
			    $('#loginFieldPassword').keypress(function(e){
			      if(e.keyCode==13)
			      	$('.loginBtnLogin').click();
			    });
				
				$scope.funcLogin = function()
				{	
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Please, check your email',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
                        
                        //alert('Please check your email');
                        return;
                    }
                    if($scope.password.text.length<4){
                        $scope.password.text = '';
                        $scope.password.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
                        
                        //alert('Your password should be at least 5 characters');
                        return;
                    }
					$state.go('loading');//-----------------------------------------------------------------------------(activate)
					resourceLogin.loginWithPassword($scope.email.text,$scope.password.text);
				}
				$scope.funcRegister = function()
				{	
					$rootScope.userEmail = "";
					$state.go('register');
				}
				$scope.funcChooseRole = function()
				{	
					$state.go('chooseRole');
				}
				$scope.cookieCheck = function()
				{
					//$scope.temp = JSON.parse(cookieManager.getCookies($scope.cookieObjectLogin));
					$scope.temp = JSON.parse(cookieManager.getCookies($scope.cookieObjectLogin));
//					console.log($scope.temp);
				}
                
                
                function statusChangeCallback(response) {
                    if (response.status === 'connected') {
                        resourceLogin.loginWithPassword(response.authResponse.userID,response.authResponse.userID,true);
                    } else if (response.status === 'not_authorized') {
//                        console.log('Not Authorized');
                    } else {
//                        console.log('Not Logged-in');
                    }
                }
                       
                $scope.fblogin = function(){
						FB.login(function(response) {
						 	window.checkLoginState(response);
						}, {scope: 'public_profile,email'});
					}

				window.checkLoginState = function(){
                    FB.getLoginStatus(function(response) {
                        statusChangeCallback(response);
                    });
                };

                window.fbAsyncInit = function() {
                    if ($scope.fbAsyncInit || window.FB === undefined) 
                        return;

                    FB.init({
                        appId      : '461446334062271',
                        cookie     : true, 
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    $scope.fbAsyncInit = true;
                };
                $scope.fbAsyncInit = false;
                fbAsyncInit();
               
                // Load the SDK asynchronously
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                
                $scope.options = {
	                'width':175,
	                'longtitle': true,
	                'theme': 'dark',
	                'cookie_policy': 'single_host_origin',
                    'onsuccess': function(response) {
                      	onSignInGoogle(response.getBasicProfile().getEmail());
                    }
                  }
                function onSignInGoogle(googleEmail) {
	                var pwdGoogle = resourceLogin.md5Pwd(googleEmail); 
	                //console.log("onSignUpGoogle1");
	                resourceLogin.loginWithPassword(googleEmail+"_g",pwdGoogle,true);
	            }
	            function storageEnabled() {
				    try {
				        localStorage.setItem("__test", "data");
				    } catch (e) {
				        if (/QUOTA_?EXCEEDED/i.test(e.name)) {
					        
					        var popupMessage = {
								header:'Service ReelHire will not work in private mode!',
								body:'Please use normal web browser mode',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
				            
				            //alert("Service ReelHire will not work in private mode. Please use normal web browser mode.");
				        }
				    }
				}
				storageEnabled();
            }
          
        ])
        .directive('googleSignInButton',['resourceLogin', function(resourceLogin) {

            return {
              scope: {
                buttonId: '@',
                options: '&'
              },
              template: '<div></div>',
              link: function(scope, element, attrs) {
                var div = element.find('div')[0];
                div.id = attrs.buttonId;
                gapi.signin2.render(div.id, scope.options()

	            ); //render a google button, first argument is an id, second options
              }
            };
          }]);
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('login', {
                        url: '/login',
                        templateUrl: 'app/login/login.html',
                        controller: 'LoginCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
            }
        ]);
})();
(function () {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    .controller('ChooseRoleCTRL', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','resourceRegister','$window',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,resourceRegister,$window) {
            $scope.funcSeeker = function(){
                $scope.userType = 'seeker';                
                resourceRegister.RegisterWithPassword('anonimus'+Date.now(),Date.now(),$scope.userType,"anonimus")
            }
            $scope.funcRecruiter = function(){
                $scope.userType = 'recruter';
                resourceRegister.RegisterWithPassword('anonimus'+Date.now(),Date.now(),$scope.userType,"anonimus")
            }
            $scope.goBack = function(){
	            $window.history.back();
            };
			
        }
      
    ]);
    angular.module('seeMeHireMeApp')
    .controller('ChooseRoleSocialCTRL', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin','$resource','cookieManager','$http','popUpManager','resourceRegister','$window',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, cookieManager,$http,popUpManager,resourceRegister,$window) {
            $scope.funcSeeker = function(){
                $rootScope.userType = 'seeker';
                resourceRegister.RegisterSocial();
            }
            $scope.funcRecruiter = function(){
                $rootScope.userType = 'recruter';
                resourceRegister.RegisterSocial();
            }
            $scope.goBack = function(){
		        resourceLogin.logOut();	
	        };
			
        }
      
    ])
})();
(function() {

    'use strict';

    var appLogin = angular.module('seeMeHireMeApp')
    .factory('resourceRegister', ['$resource', '$http', '$state', 'systemFactory', 'resourceLogin', '$q', 'popUpManager', '$rootScope', function($resource,$http,$state,systemFactory,resourceLogin,$q,popUpManager,$rootScope){
	    var updateUser = function(email,pwd,userType){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'resetcode/'+$rootScope.userEmail,
			}; 
            return $http(requestData).then(function(responce){
                var requestData3 = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'user/update',
					data: {
                        'email':email,
                        'pwd':pwd,
                        'md5':responce.data,
                        'name': email,
                        'use_type': $rootScope.userType
                    },
                    headers: {
					   'skey': systemFactory.md5()
					},
				}; 

                return $http(requestData3).then(function(responce3){
                   if(responce3 != undefined){
                       resourceLogin.loginWithMd5(responce3.data);
                   }else{
                       reject('Sorry :( Something went wrong. Please try again');
                   }
                   
               });
	    	});
	    };	
	    return{
			    RegisterSocial: function(){
				   this.RegisterWithPassword(resourceLogin.currentEmail(),resourceLogin.currentPwd(),$rootScope.userType,$rootScope.userType);
			    },
			 	RegisterWithPassword: function(email,pwd,userType,name){
				 	if($rootScope.userEmail.indexOf("anonimus") == 0){
					 	updateUser(email,pwd,userType);
					}else{
					    var requestData = {
						    method: 'POST',
							url: systemFactory.serverUrl()+'user',
							data: {
	                            'email':email,
	                            'pwd':pwd,
	                            'user_type':userType
	                        },
	                        headers: {
							   'skey': 123
							}
						};
						if(name != undefined){
							requestData.data.name = name;
						} 
					    return $http(requestData).then(function(responce){
						    if(responce.data == '-1'){
							    
							    var popupMessage = {
									header:'',
									body:'The user is already registered',
									btnOneName:"OK"
								}
				
								popUpManager.popupModal(popupMessage);
							    
	                            gapi.auth.signOut();
	                            var auth2 = gapi.auth2.getAuthInstance();
	                            auth2.signOut();
								$state.go('register');
							}else{
                                $rootScope.firstLoginType = userType;
	                            resourceLogin.loginWithMd5(responce.data);
	                            systemFactory.sendWelcomeEmail(email,userType);
							}
						});
					}
				},
				changePasswordCode:function(email,code,newPwd){
					$state.go('loading');
					return $q(function(resolve, reject) {
                            var requestData3 = {
							    method: 'Post',
								url: systemFactory.serverUrl()+'user/update',
								data: {
		                            'email':email,
		                            'name':newPwd,
		                            'pwd':newPwd,
		                            'md5': code
		                        }
		                        
							}; 
                            return $http(requestData3).then(function(responce3){
	                           if(responce3 != undefined){
		                           resolve(responce3.data);
	                           }else{
		                           reject('Sorry :( Something went wrong. Please try again');
	                           }
	                           
                           });

					});
				},
				changePassword:function(email,oldPwd,newPwd){
					$state.go('loading');
					return $q(function(resolve, reject) {
					    var requestData = {
						    method: 'GET',
							url: systemFactory.serverUrl()+'user?email='+email+'&pwd='+oldPwd,
						}; 
					    return $http(requestData).then(function(responce){
						    
						    if(responce.data.md5 != undefined){
	                            var requestData2 = {
								    method: 'GET',
									url: systemFactory.serverUrl()+'resetcode/'+email,
								}; 
	                           return $http(requestData2).then(function(responce2){
		                            var requestData3 = {
									    method: 'Post',
										url: systemFactory.serverUrl()+'user/update',
										data: {
				                            'email':email,
				                            'name':newPwd,
				                            'pwd':newPwd,
				                            'md5':responce2.data
				                        }
									}; 
		                            return $http(requestData3).then(function(responce3){
			                           if(responce3 != undefined){
				                           resolve(responce3.data);
				                           systemFactory.setMd5(responce3.data);
			                           }else{
				                           reject('Sorry :( Something went wrong. Please try again');
			                           }
			                           
		                           });
	                           });
							}else{
	                            reject("Wrong login data");
							}
						});
							
					});
				},
				
		    
		    publishPhoto:function(){
				$state.go('loading');
				return $q(function(resolve, reject) {
				        return $http.post(systemFactory.serverUrl() + "uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	$window.history.back();
					        	alert("Something went wrong. Your photo was not uploaded. Try again.");
								reject(info);
				        	}
				        })
				        .error(function(err){
					        $window.history.back();
					        alert("Something went wrong. Your photo was not uploaded. Try again.");
				        	reject(err);
				        });
				    
				});
			},
		    
		    
		    
		    
		    
		    }; 
	    
    	}])
        .controller('registerCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceRegister', '$resource','cookieManager','resourceLogin','popUpManager','$window',
            function($scope, $state, $rootScope, $cookies, resourceRegister, $resource, cookieManager,resourceLogin,popUpManager,$window) {
                if($rootScope.checkIfMobileNoPopup()){
	                $state.go('main');
	                return;
                }
                $scope.userType = 'undefinite';
				$scope.selectTypeBtnActive = true;
                $scope.funcSeeker = function(){
                    $scope.userType = 'seeker';
                }
                $scope.funcRecruiter = function(){
                    $scope.userType = 'recruter';
                }
                if($rootScope.userEmail.indexOf("anonimus") == 0){
	                if($rootScope.userType == "seeker"){
		                $scope.funcSeeker();
	                }else{
		                $scope.funcRecruiter(); 
	                }
	                $scope.selectTypeBtnActive = false;
                }
                $scope.goBack = function(){
		            $window.history.back();
	            };
                
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
             
                $scope.email = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
                $scope.passwordConfirm = {
                    text:'',
                    error : false
                };
                
                $scope.funcRegister = function(){
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Please check your email',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.password.text.length<5){
                        $scope.password.text = '';
                        $scope.password.error = true;
                        $scope.passwordConfirm.error = true;
                        
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.password.text != $scope.passwordConfirm.text){
                        $scope.passwordConfirm.error = true;
                        $scope.password.error = true;
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        
                        var popupMessage = {
							header:'',
							body:'Please, check your password',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    if($scope.userType == 'undefinite'){
	                    
	                    var popupMessage = {
							header:'',
							body:'Please, choose who you are',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        return;
                    }
                    resourceRegister.RegisterWithPassword($scope.email.text,$scope.password.text,$scope.userType)
                    
                }
                $scope.fbAsyncInit = false;

                function statusChangeCallback(response) {
                    if (response.status === 'connected') {
                        if($scope.userType == 'undefinite'){
	                        
	                        var popupMessage = {
								header:'',
								body:'Please, choose who you are',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
                            return;
                        }
                        resourceRegister.RegisterWithPassword(
                            response.authResponse.userID,
                            response.authResponse.userID,
                            $scope.userType
                        );
                    } else if (response.status === 'not_authorized') {
//                        console.log('Not Authorized');
                    } else {
//                        console.log('Not Logged-in');
                    }
                }
                
                $scope.fblogin = function(){
						FB.login(function(response) {
						 	window.checkLoginState(response);
						}, {scope: 'public_profile,email'});
					}

                window.checkLoginState = function(){
                    FB.getLoginStatus(function(response) {
                        statusChangeCallback(response);
                    });
                };

                window.fbAsyncInit = function() {
                    if ($scope.fbAsyncInit || window.FB === undefined) 
                        return;

                    FB.init({
                        appId      : '461446334062271',
                        cookie     : true, 
                        xfbml      : true,
                        version    : 'v2.1'
                    });

                    $scope.fbAsyncInit = true;
                }();

                
                $scope.options = {
	                'width':175,
	                'longtitle': true,
	                'theme': 'dark',
                    'onsuccess': function(response) {
                      onSignUpGoogle(response.getBasicProfile().getEmail());
                    }
                  }
                function onSignUpGoogle(googleEmail) {
                    var pwdGoogle = resourceLogin.md5Pwd(googleEmail); 
                    if($scope.userType == 'undefinite'){
	                    
	                    var popupMessage = {
							header:'',
							body:'Please choose who you are',
							btnOneName:"OK"
						}
		
						popUpManager.popupModal(popupMessage);
                        gapi.auth.signOut();
                        var auth2 = gapi.auth2.getAuthInstance();
                        auth2.signOut();
                        return;
                    }
                    resourceRegister.RegisterWithPassword(googleEmail+"_g",pwdGoogle,$scope.userType);
                }
            }
	       
	    ])
        .directive('googleSignUpButton', function() {
            return {
              scope: {
                buttonId: '@',
                options: '&'
              },
              template: '<div></div>',
              link: function(scope, element, attrs) {
                var div = element.find('div')[0];
                div.id = attrs.buttonId;
                gapi.signin2.render(div.id, scope.options()); //render a google button, first argument is an id, second options
              }
            };
          });
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('register', {
                        url: '/register',
                        templateUrl: 'app/register/register.html',
                        controller: 'registerCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
            }
        ]);
})();


(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('ProfileCtrl', ['$scope','$rootScope', '$timeout', '$state', '$window', 'resourceLogin','popUpManager','recruiterFactory','questionnarieFactory', function($scope,$rootScope, $timeout, $state, $window, resourceLogin,popUpManager,recruiterFactory,questionnarieFactory) {
                $scope.funcLogOut = function()
				{	
				   resourceLogin.logOut();	
				}
				
				$scope.changePwd = function(){
					$state.go('changePassword')
				}
                $scope.termsAndConditions = function(){
	                $state.go('termsAndConditions')
                }
                $scope.contactUs = function(){
	                $state.go('contactUs')
                }
                $scope.deleteMyProfileName = '';
                $scope.deleteMyProfilePopup = '';
                
                
                if($rootScope.userType === 'seeker'){
                    $scope.deleteMyProfileName = 'I Got Hired';
                    $scope.deleteMyProfilePopup = 'Clicking "OK" will permanently delete your web resume, all your videos, questionnaires and personal data. Are you Sure?';
                }
                else{
                    $scope.deleteMyProfileName = 'Delete My Profile';
                    $scope.deleteMyProfilePopup = 'Clicking "OK" will permanently delete all your questionnaires and video answers to your questionnaires. Are you sure?';
                };
                
                function deleteProfile(){
	                if($rootScope.userType === 'seeker'){
		                questionnarieFactory.deleteAllQuestionnaries();
	                }else{
		                recruiterFactory.deleteAllAnkets();
	                }
	                 var popupMessage = {
						header:'',
						body:'Your data was deleted.',
						btnOneName:"OK"
					};
					
                    popUpManager.popupModal(popupMessage);
                };
                
                $scope.deleteMyProfile = function(){
                    var popupMessage = {
						header:'',
						body:$scope.deleteMyProfilePopup,
						btnOneName:"OK",
						btnTwoName:"Cancel",
						btnOneClick:deleteProfile
					};
					
                    popUpManager.popupModal(popupMessage);
                };
                 
               
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('profile', {
            url: '/profile',
            templateUrl: 'app/profile/profile.html',
            controller: 'ProfileCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('ChangePasswordCtrl', ['$scope',  '$state', 'resourceLogin','$window','resourceRegister','popUpManager', function($scope, $state, resourceLogin,$window,resourceRegister,popUpManager) {
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
             
                $scope.email = {
                    text:'',
                    error : false
                };
                $scope.oldPassword = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
                $scope.passwordConfirm = {
                    text:'',
                    error : false
                };
                $scope.goBack = function(){
	                 $window.history.back();
                };
                $scope.checkEmail = function(){
	                if(!$scope.email.text){
                        $scope.email.error = true;
                    }
                }
                 $scope.changePwd = function(){
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        var popupMessage = {
							header:'',
							body:'Please check your email',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                       
                        return;
                    }
                    if($scope.password.text.length<5){
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        $scope.password.error = true;
                        $scope.passwordConfirm.error = true;
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    if($scope.password.text != $scope.passwordConfirm.text){
                        $scope.passwordConfirm.error = true;
                        $scope.password.error = true;
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        var popupMessage = {
							header:'',
							body:'Please check your password',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    
                    resourceRegister.changePassword($scope.email.text,$scope.oldPassword.text,$scope.password.text).then(
	                    function(respond){
		                    var popupMessage = {
								header:'',
								body:'Your password was successfuly changed!',
								btnOneName:'OK'
							}
		
							popUpManager.popupModal(popupMessage);
							
		                    $state.go("profile");
	                    },
	                    function(err){
		                    //alert(err);
		                    $window.history.back();
	                    }
                    );
                    
                }
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('changePassword', {
            url: '/changePassword',
            templateUrl: 'app/changePassword/changePassword.html',
            controller: 'ChangePasswordCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('resetPasswordCtrl', ['$rootScope','$scope',  '$state', 'resourceLogin','$window','resourceRegister','popUpManager', function($rootScope, $scope, $state, resourceLogin,$window,resourceRegister,popUpManager) {
                $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
             
                $scope.email = {
                    text:'',
                    error : false
                };
				$scope.password = {
                    text:'',
                    error : false
                };
                $scope.passwordConfirm = {
                    text:'',
                    error : false
                };

                $scope.email.text = $rootScope.resetEmail;
                
                 $scope.changePwd = function(){
                    if(!$scope.email.text){
                        $scope.email.error = true;
                        var popupMessage = {
							header:'',
							body:'Please check your email',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    if($scope.password.text.length<5){
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        $scope.password.error = true;
                        $scope.passwordConfirm.error = true;
                        var popupMessage = {
							header:'',
							body:'Your password should be at least 5 characters',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    if($scope.password.text != $scope.passwordConfirm.text){
                        $scope.passwordConfirm.error = true;
                        $scope.password.error = true;
                        $scope.password.text = '';
                        $scope.passwordConfirm.text = '';
                        var popupMessage = {
							header:'',
							body:'Please check your password',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
                        
                        return;
                    }
                    
                    resourceRegister.changePasswordCode($scope.email.text,$rootScope.resetCode,$scope.password.text).then(
	                    function(respond){
		                    $rootScope.resetCode = "";
		                    $rootScope.resetEmail = "";
		                    var popupMessage = {
								header:'',
								body:'Your password was successfuly changed!',
								btnOneName:'OK'
							}
		
							popUpManager.popupModal(popupMessage);
				
		                    $state.go("main");
	                    },
	                    function(err){
		                    alert(err);
	                    }
                    );
                    
                }
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('resetPwd', {
            url: '/resetPassword',
            templateUrl: 'app/resetPwd/resetPassword.html',
            controller: 'resetPasswordCtrl'
          });
      }]);
      
})();

var cookiePublicResume = 'publicResumeObject';
 
 (function(win) {

    'use strict';

    angular.module('seeMeHireMeApp')
           .controller('ResumeEditCtrl', ['$scope', '$timeout', '$state', '$window', '$sce','cookieManager','resumeFactory','systemFactory','$http','videoUploadFactory','photoUploadFactory','popUpManager','$rootScope','questionnarieFactory', function($scope, $timeout, $state, $window, $sce, cookieManager,resumeFactory,systemFactory,$http,videoUploadFactory,photoUploadFactory,popUpManager,$rootScope,questionnarieFactory) {
	        
               $scope.$watch('firstLoginType',function(newValue,oldValue){
                   
                   if(newValue!== null && newValue != undefined)
                        {
                            popUpManager.welcomeJobSeeker();
                        }
               });
               
               
	        videoUploadFactory.setCurrentFactory(resumeFactory);
	        photoUploadFactory.setCurrentFactory(resumeFactory);

	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
            
			var allResumes = resumeFactory.allResume();
			var systemAnketId = systemFactory.systemAnketID();
            $scope.serverResume = resumeFactory.serverResume();
            $scope.resume = resumeFactory.myResume();
            $scope.isNoneUS = {};
			
            $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>0;
            }
            $scope.checkZip = function(){
                if($scope.zipCodeBase.hasOwnProperty($scope.resume.zipcode.text)){
                    var location = $scope.zipCodeBase[$scope.resume.zipcode.text].split(',');
                    $scope.resume.city = location[0];
                    $scope.resume.state = location[1];
                    
                }
                else{
                    $scope.resume.zipcode.error = true;
                    $scope.resume.city = '';
                    $scope.resume.state = '';
                }
                if($scope.resume.zipcode.text == "00000"){
	                $scope.isNoneUS.checked = true;
	                $scope.resume.zipcode.error = false;
	                $scope.resume.city = 'City';
                    $scope.resume.state = 'State';
                }
            }
            
            $scope.changeNoneUS = function(){
	            
	            if($scope.isNoneUS.checked){
		            $scope.resume.zipcode.text = "00000";
		            $scope.resume.zipcode.error = false;
		            $scope.resume.city = 'City';
                    $scope.resume.state = 'State';
	            }else{
		            $scope.resume.zipcode.text = "";
		            $scope.resume.zipcode.error = false;
		            $scope.resume.city = '';
                    $scope.resume.state = '';
	            }
            }
            $scope.isZipUS = function(){
                return  !$scope.resume.zipcode.error && $scope.resume.zipcode.text!=undefined && $scope.resume.zipcode.text.length>0 && $scope.resume.zipcode.text!="00000";
            }
             
            $scope.checkEmail = function(){
                if($scope.resume.email.text===undefined){
                    $scope.resume.email.error = true;   
                }
            }
            $scope.setCurrentVideoName = function(name,link,objName,videoTitle){
	           $scope.resume[objName].error = false;
	           var videoIndex = objName.replace('video', "");
	           $rootScope.currentVideoIndex = videoIndex;
	           $rootScope.currentVideoTitle = videoTitle;
	           if(!$scope.serverResume.hasOwnProperty('id')){
		           resumeFactory.publishResume(true);
	           }else{
		           $rootScope.currentResumeId = $scope.serverResume.id;
	           }
	           
                resumeFactory.setCurrentVideo({name:name,link:link});
            }
            $scope.setCurrentPhoto = function(name,link){
	            $scope.resume.photo.error = false;
                resumeFactory.setCurrentPhoto({name:name,link:link});
            }
            $scope.deleteMergedVideo = function(){
                $scope.resume.videoMerged = {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                };
                resumeFactory.setMyResume({video_merged_url:{
                    link:'',
                    text:'',
                    html:'',
                    error:false
                }});
               
            }
            
            
          
                $scope.resume = resumeFactory.myResume();
                $scope.resume.video1.text = systemFactory.systemAnket().video1_title;
                $scope.resume.video2.text = systemFactory.systemAnket().video2_title;
                $scope.resume.video3.text = systemFactory.systemAnket().video3_title;
                $scope.resume.video4.text = systemFactory.systemAnket().video4_title;

			
			$scope.restore = (function(){
				var popupMessage = {
					header:'Are you sure you want to restore your resume?',
					body:'All your changes will be lost!',
					btnOneName:"Restore",
					btnTwoName:"Cancel",
					btnOneClick:function(){
						resumeFactory.restoreResume();
					}
				}

				popUpManager.popupModal(popupMessage);
                
            });
            $scope.checkForCurrentVideo = (function(){
                if(resumeFactory.getCurrentVideo().link.length>2){
                    
                    if(resumeFactory.getCurrentVideo().name === 'resume1'){
                        $scope.resume.video1.html = $sce.trustAsHtml("<video id='video' controls>"+
                        "<source id='vid-source'  src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video1.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume2'){
                        $scope.resume.video2.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video2.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume3'){
                        $scope.resume.video3.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video3.link = resumeFactory.getCurrentVideo().link;
                    }
                    if(resumeFactory.getCurrentVideo().name === 'resume4'){
                        $scope.resume.video4.html = $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+resumeFactory.getCurrentVideo().link+"' type='video/mp4'></video>");
                        $scope.resume.video4.link = resumeFactory.getCurrentVideo().link;
                    }
                    resumeFactory.resetCurrentVideo();
                }
            });
             $scope.checkForCurrentPhoto = (function(){
                if(resumeFactory.getCurrentPhoto().link.length>2){
                    if(resumeFactory.getCurrentPhoto().name === 'userPhoto'){
                        $scope.resume.photo.html = "<div>---</div>";
                        $scope.resume.photo.link = resumeFactory.getCurrentPhoto().link;
                    }
                    resumeFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentVideo();
            $scope.localVideoTemplate = function(link){
				if(link.indexOf("http") == 0){
					var linkName = link.replace("https://www.youtube.com/v/","")
                    return $sce.trustAsHtml('<iframe id="video" src="http://www.youtube.com/embed/'+linkName+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  frameborder="0" ></iframe>');
				}
				else {
                	return $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+link+"?rel=0&"+"' type='video/mp4' > </video>");
                }      
            }
            
            $scope.setVideoHtml = function(){
	            
	            for(var i=1;i<5;i++){
		            if($scope.resume["video"+i].link.length > 2){
			            $scope.resume["video"+i].html = $scope.localVideoTemplate($scope.resume["video"+i].link);
		            }
	            }
	        }
			$scope.setVideoHtml();
            
            $scope.checkForCurrentPhoto();
             $http.get('app/resources/professions.json').success(function(data) {
                  $scope.professions = data;
             });
             $http.get('app/resources/zip.json').success(function(data) {
                  $scope.zipCodeBase = data;
                  if($scope.isFilled($scope.resume.zipcode.text)) $scope.checkZip();
             });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No'];
			$scope.isRequestCanceled = false;
			$scope.publishclick = function(){	
	        	$rootScope.$on('$stateChangeStart', 
					function(event, toState, toParams, fromState, fromParams){ 
						if(toState.name != "loading") 
				    	$scope.isRequestCanceled = true;
				}) 
				
                if(resumeFactory.checkResume($scope.resume) && resumeFactory.checkRegistration()){
	                
	                var requestDataMerge = {
						method: 'GET',
	                    url: "http://apevovideo.cloudapp.net/v/service1.svc/ffmpegmerge?resume_id="+$scope.serverResume.id
					};
					
	                $state.go('loading');
	                
					if($scope.resume.video1.link.length >0 ){
						$http(requestDataMerge).then(function(respond){ console.log(respond)});
						systemFactory.waitForChangeOnServer("resume/"+$scope.serverResume.id,"video_merged_url",$scope).then(
							function(newField){
								$scope.resume.videoMerged.link = newField;
								resumeFactory.publishPhoto().then(
					                function(result){
						                $scope.isRequestCanceled = false;
						                $scope.resume.photo.link = result;
							            resumeFactory.publishResume();
							            $state.go('resumeEdit');
						            },
						            function(err){
							            
						            }
					            ); 
			                },function(fail){
				                $scope.waitForVideo = false;
				                if(fail == '0'){
					                console.log('canceled req');
				                }else{
					                var popupMessage = {
										header:'Something went wrong...',
										body:'Please try again',
										btnOneName:'OK'
									}
				
									popUpManager.popupModal(popupMessage);
								}
			            });
			        }else{
				        resumeFactory.publishPhoto().then(
			                function(result){
				                $scope.isRequestCanceled = false;
				                $scope.resume.photo.link = result;
					            resumeFactory.publishResume();
					            $state.go('resumeEdit');
				            },
				            function(err){
					            
				            }
			            ); 
			        }
                }
			}

            //variables to check for the changes
            $rootScope.resumeScopeShare='';
			
			$scope.isEqualWithServer = function(isPublishButton){
                //constantly checking for changing parameters of the resume
                $rootScope.resumeScopeShare = $scope.resume;
                if (typeof(isPublishButton)==='undefined') isPublishButton=false;
                
                return resumeFactory.isEqualWithServer(isPublishButton);
            };
            
        }]
    )
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('resumeEdit', {
            url: '/resume/edit',
            templateUrl: 'app/resumeEdit/resumeEdit.html',
            controller: 'ResumeEditCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('QuestionnairesCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window','questionnarieFactory','popUpManager', function($rootScope,$scope, $timeout, $state, $window,questionnarieFactory, popUpManager) {
			$scope.questionnaries = questionnarieFactory.questionnaries();
			
			(function updateQuestionnaries(){
                setTimeout(function() {
	                if($scope)
	                updateQuestionnaries();
	                
					$scope.questionnaries = questionnarieFactory.questionnaries();
					
					$scope.$apply();
		    	}, 10000);
            })(); 
            
            
			$scope.checkForQuestionnarie = function( ) {
			  return function( questionnarie ) {
				  if(!questionnarie.isHasAnket) return false;
				   return questionnarie.companyName.length>2;
			  };
			};
			$scope.status = function(questionnarie){
				if(questionnarie.isNew) return "New";
				if(questionnarie.ispublished) return "Sent";
				return "";
			},
			$scope.deleteAnket = function(questionnarie){
 				var popupMessage = {
					header:'Are you sure?',
					body:'Do you want to delete the Questionnaire for the position ' + questionnarie.companyPosition + '?',
					btnOneName:'Delete',
					btnTwoName:'Cancel',
					btnOneClick:function(){
						for(var i=0; i<$scope.questionnaries.length; i++){
							if($scope.questionnaries[i].anket_id === questionnarie.anket_id){
								questionnarieFactory.deleteNotification(questionnarie.notificationId);
								questionnarieFactory.deleteAnket(questionnarie.anket_id);
								$scope.questionnaries.splice(i,1);
							}
						}
					},
					btnTwoClick:function(){
						
					}
				}

				popUpManager.popupModal(popupMessage);
			},
			$scope.goToQuestionnarie = function(questionnarie){
				questionnarie.isNew = false;
				questionnarieFactory.deleteNotification(questionnarie.notificationId);
				if(questionnarie.ispublished){
					questionnarieFactory.setCurrentQuestionnarie(questionnarie.id);
					$state.go('publishedQuestionnaire');
				}else{
					questionnarieFactory.setCurrentQuestionnarie(questionnarie.anket_id);

					if(!questionnarie.hasOwnProperty('id')){
						questionnarieFactory.publishQuestionnarie(true);
					}else{
						$rootScope.currentResumeId = questionnarie.id;
					}
					$state.go('questionnairesEdit');
				}
			}
			$scope.anketId = function(anket){
	            return -Number(anket.anket_id);
            }
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('questionnaires', {
            url: '/questionnaires',
            templateUrl: 'app/questionnaires/questionnaires.html',
            controller: 'QuestionnairesCtrl'
          });
          
      }]);
      
})();

(function(win) {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('QuestionnairesEditCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window', '$sce','cookieManager','resumeFactory','questionnarieFactory','systemFactory','$http','videoUploadFactory','photoUploadFactory', function($rootScope,$scope, $timeout, $state, $window, $sce, cookieManager,resumeFactory,questionnarieFactory,systemFactory,$http,videoUploadFactory,photoUploadFactory) {
	        
	        videoUploadFactory.setCurrentFactory(questionnarieFactory);
	        photoUploadFactory.setCurrentFactory(questionnarieFactory);
	        $scope.isNoneUS = {};
	        
	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
            
            $scope.questionnarie = questionnarieFactory.currentQuestionnarie();
            
            $scope.localVideoTemplate = function(link){
				if(link.indexOf("http") == 0){
                    var linkName = link.replace("https://www.youtube.com/v/","")
                    return $sce.trustAsHtml('<iframe id="video" src="http://www.youtube.com/embed/'+linkName+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  frameborder="0" ></iframe>');
				}else {
                	return $sce.trustAsHtml("<video id='video' controls><source id='vid-source' src='"+link+"?rel=0&"+"' type='video/mp4' > </video>");
                }      
            }
            
            $scope.setVideoHtml = function(){
	            for(var i=1;i<5;i++){
		            if($scope.questionnarie["video"+i].link.length > 2){
			            $scope.questionnarie["video"+i].html = $scope.localVideoTemplate($scope.questionnarie["video"+i].link);
		            }
	            }
	        }
			$scope.setVideoHtml();
            $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>0;
            };
            $scope.checkZip = function(){
                if($scope.zipCodeBase.hasOwnProperty($scope.questionnarie.zipcode.text)){
                    var location = $scope.zipCodeBase[$scope.questionnarie.zipcode.text].split(',');
                    $scope.questionnarie.city = location[0];
                    $scope.questionnarie.state = location[1];
                }
                else{
                    $scope.questionnarie.zipcode.error = true;
                     $scope.questionnarie.city = '';
                    $scope.questionnarie.state = '';
                }
                if($scope.questionnarie.zipcode.text == "00000"){
	                $scope.isNoneUS.checked = true;
	                $scope.questionnarie.zipcode.error = false;
	                $scope.questionnarie.city = 'City';
                    $scope.questionnarie.state = 'State';
                }
            };
             $scope.changeNoneUS = function(){
	            if($scope.isNoneUS.checked){
		            $scope.questionnarie.zipcode.text = "00000";
		            $scope.questionnarie.zipcode.error = false;
		            $scope.questionnarie.city = 'City';
                    $scope.questionnarie.state = 'State';
	            }else{
		            $scope.questionnarie.zipcode.text = "";
		            $scope.questionnarie.zipcode.error = false;
		            $scope.questionnarie.city = '';
                    $scope.questionnarie.state = '';
	            }
            }
            $scope.isZipUS = function(){
                return  !$scope.questionnarie.zipcode.error && $scope.questionnarie.zipcode.text!=undefined && $scope.questionnarie.zipcode.text.length>0 && $scope.questionnarie.zipcode.text!="00000";
            }
            $scope.checkEmail = function(){
                if($scope.questionnarie.email.text===undefined){
                    $scope.questionnarie.email.error = true;   
                }
            }
            $scope.setCurrentVideoName = function(name,link,objName,videoTitle){
	            var videoIndex = objName.replace('video', "");
	            $rootScope.currentVideoIndex = videoIndex;
	            $rootScope.currentVideoTitle = videoTitle;
	            $scope.questionnarie[objName].error = false;
                questionnarieFactory.setCurrentVideo({name:name,link:link});
            }
            $scope.setCurrentPhoto = function(name,link){
	            $scope.questionnarie.photo.error = false;
                questionnarieFactory.setCurrentPhoto({name:name,link:link});
            }
            

			
            $scope.checkForCurrentVideo = (function(){
                if(questionnarieFactory.getCurrentVideo().link.length>2){
                    
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie1'){
                        $scope.questionnarie.video1.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video1.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie2'){
                        $scope.questionnarie.video2.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video2.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie3'){
                        $scope.questionnarie.video3.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video3.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    if(questionnarieFactory.getCurrentVideo().name === 'questionnarie4'){
                        $scope.questionnarie.video4.html = $scope.localVideoTemplate(questionnarieFactory.getCurrentVideo().link);
                        $scope.questionnarie.video4.link = questionnarieFactory.getCurrentVideo().link;
                    }
                    questionnarieFactory.resetCurrentVideo();
                }
            });
            
             $scope.checkForCurrentPhoto = (function(){
                if(questionnarieFactory.getCurrentPhoto().link.length>2){
                    if(questionnarieFactory.getCurrentPhoto().name === 'userPhoto'){
                        $scope.questionnarie.photo.html = "<div>---</div>";
                        $scope.questionnarie.photo.link = questionnarieFactory.getCurrentPhoto().link;
                    }
                    questionnarieFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentVideo();
            
            $scope.checkForCurrentPhoto();
	        $http.get('app/resources/professions.json').success(function(data) {
	             $scope.professions = data;
	        });
	        $http.get('app/resources/zip.json').success(function(data) {
	            $scope.zipCodeBase = data;
	            if($scope.isFilled($scope.questionnarie.zipcode.text)) $scope.checkZip();
	        });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No'];
			
            $scope.publishclick = function(){
	            if(resumeFactory.checkResume($scope.questionnarie) && resumeFactory.checkRegistration()){
	                questionnarieFactory.publishPhoto().then(
		                function(result){
			                $scope.questionnarie.photo.link = result;
			                $scope.questionnarie.ispublished = true;
				            questionnarieFactory.publishQuestionnarie();
			            },
			            function(err){
				            
				            var popupMessage = {
								header:'Something went wrong',
								body:'Your questionnarie was not published. Try again',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
				            $window.history.back();
			            }
		            ); 
                }
                
			}
			    

        }]
    );
    
	

    
})();


(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('questionnairesEdit', {
            url: '/questionnaires/edit',
            templateUrl: 'app/questionnairesEdit/questionnairesEdit.html',
            controller: 'QuestionnairesEditCtrl'
          });
	    
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('PreviewCtrl', ['$scope', '$timeout', '$state', '$window', function($scope, $timeout, $state, $window) {

        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('preview', {
            url: '/preview',
            templateUrl: 'app/preview/preview.html',
            controller: 'PreviewCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('getLinkCtrl', ['$scope','resumeFactory','$rootScope' , function($scope,resumeFactory,$rootScope ) {

			$scope.linkToResume = 'http://reelhire.com/'+$rootScope.idToHash(resumeFactory.serverResume().id,'webResume');
			
			
            $scope.open = function(){
				window.open($scope.linkToResume);
			};
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('getLink', {
            url: '/getLink',
            templateUrl: 'app/getLink/getLink.html',
            controller: 'getLinkCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('sendToCtrl', ['$rootScope','$scope', '$timeout', '$state', '$http', '$window', 'resumeFactory','emailFactory','systemFactory','popUpManager', function($rootScope, $scope, $timeout, $state, $http, $window,resumeFactory,emailFactory,systemFactory,popUpManager ) {
	        $scope.emailPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$';
	        $scope.manyEmailsPattern = '^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z-])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9},? *)+$';
	        
	        $scope.emailToPattern = $scope.emailPattern;
            var maxNumberOfRecipients = 10;
			
			$scope.serverResume = resumeFactory.serverResume();
			$scope.emailFrom = {text:$rootScope.userEmail,error:false};
			$scope.emailTo = {text:"",error:false};
			$scope.subject = {text:"",error:false};
			$scope.emailBody = "";
			$scope.candidateList = emailFactory.candidates();
			$scope.emailToDisable = false;
			
			if($rootScope.emailType == "emailToSelected"){
				$scope.subject.text = "Job Application";
				$scope.emailBody = "I would like to talk to you further about the position.";
			}else if($rootScope.emailType == "emailSelected"){
				$scope.subject.text = "A candidate of Interest";
				$scope.emailBody = "Hi there, please check out the following candidate’s Video Questionnaire. They might be a good fit for the company!";
			}else if(systemFactory.userType() == "seeker"){
				$scope.subject.text = "Please check out my Video Resume";
				$scope.emailBody = "";
			}
			$scope.emailTitle = emailFactory.emailTitle();
			
			$scope.checkEmailFrom = function(){
                if($scope.emailFrom.text===undefined){
                    $scope.emailFrom.error = true;   
                }
            }
            $scope.checkEmailTo = function(){
                if($scope.emailTo.text===undefined){
                    $scope.emailTo.error = true;   
                }
            }

            $scope.showCandidates = function(){
	            return $rootScope.emailType=="emailSelected" || systemFactory.userType() == "seeker";
            }
            if(!$scope.showCandidates()){
	            $scope.emailToPattern = "";
	            for(var i=0;i<Math.min($scope.candidateList.length,maxNumberOfRecipients);i++){
		            if($scope.emailTo.text.length == 0){
		            	$scope.emailTo.text = ""+$scope.candidateList[i].email; 
		            }else{
			            $scope.emailTo.text = $scope.candidateList[i].email + ', ' + $scope.emailTo.text;
		            }
	            }

	            $scope.emailToDisable = true;
	            
            }
            $scope.sendEmail = function(){
					         
	            var isCorrect = emailFactory.checkEmail($scope);
	          
	            if(!isCorrect){
		            
		            var popupMessage = {
						header:'',
						body:'Please, check fields',
						btnOneName:"OK"
					}
	
					popUpManager.popupModal(popupMessage);
						
		            //alert("Please check fields");
	            }else{
		            $state.go('loading');
		            while($scope.emailTo.text.indexOf("  ") != -1){
			            $scope.emailTo.text = $scope.emailTo.text.replace("  "," ").replace(", "," ").replace(" ,"," ");
		            }
		            var request = $http({
					    method: "post",
					    url: "/components/sendgrid-php/myMail.php",
					    data: {
						    sm: systemFactory.mc().id,
						    sp: systemFactory.mc().value,
					        emailTo: $scope.emailTo.text.replace(" ",",").replace(",",", "),
					        emailFrom: $scope.emailFrom.text,
					        subject : $scope.subject.text,
							message : message()
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					/* Check whether the HTTP Request is successful or not. */

					request.success(function (data) {
						
						$state.go('sendTo');
						if(data.indexOf('error') == -1 && data.indexOf('Error') == -1 && data.indexOf('ERROR') == -1){
							
							var popupMessage = {
								header:'',
								body:'Your email was sent successfully!',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}else{
							var popupMessage = {
								header:'',
								body:'Your email was not sent. Try again.',
								btnOneName:"OK"
							}
			
							popUpManager.popupModal(popupMessage);
						}	
						
					    //alert("Your email was sent successfully!");
					});

	            }

            }
            
            
            var client = new XMLHttpRequest();
            var emailTemplate = "";
			client.open('GET', '/app/serverRequests/emailTemplate.html');
			client.onreadystatechange = function() {
			  	emailTemplate = client.responseText;
			}
			client.send();
			
			
            var message = function(){
				var textTitle = "";//"This is a candidate of interest. Please check out their Video Resume";
				
				var startOfCandidateSection = emailTemplate.indexOf("<br><div style=\"b");
				var lengthOfCandidateSection = emailTemplate.indexOf('---->')-startOfCandidateSection+5;
				var sectionCandidate = emailTemplate.slice(emailTemplate.indexOf("<br><div style=\"b"),emailTemplate.indexOf('<!---->'));
				emailTemplate = emailTemplate.replace(sectionCandidate,"");
				var sectionsCandidate = [];
				var selectedCandidate = [];
				var candidateCount = 1;
				
				if(systemFactory.userType() == "seeker"){
					selectedCandidate[0] = $scope.serverResume;
					selectedCandidate[0].resumeId = selectedCandidate[0].id;
					selectedCandidate[0].firstName = selectedCandidate[0].first_name;
					selectedCandidate[0].lastName = selectedCandidate[0].last_name;
					selectedCandidate[0].photoLink = selectedCandidate[0].photo_url;
				}else if($rootScope.emailType == 'emailSelected'){
					candidateCount = $scope.candidateList.length;
					selectedCandidate = $scope.candidateList;
					maxNumberOfRecipients = candidateCount;
				}else{
					candidateCount = 0;
				}
				
				for(var i=0;i<Math.min(candidateCount,maxNumberOfRecipients);i++){
					sectionsCandidate[i] = sectionCandidate;
					var decriptedId = $rootScope.idToHash(selectedCandidate[i].resumeId,'webResume');
					sectionsCandidate[i] = sectionsCandidate[i].replace("{resumeId}",decriptedId);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{firstName}",selectedCandidate[i].firstName);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{lastName}",selectedCandidate[i].lastName);
					sectionsCandidate[i] = sectionsCandidate[i].replace("{photo}",selectedCandidate[i].photoLink);
					var tempString = emailTemplate.slice(startOfCandidateSection);
					emailTemplate = emailTemplate.slice(0, startOfCandidateSection) + sectionsCandidate[i] + tempString;
					startOfCandidateSection += sectionsCandidate[i].length;
				}
				emailTemplate = emailTemplate.replace("{textTitle}",textTitle)
					.replace("{textBody}",$scope.emailBody );
				
	            return emailTemplate;
            }
        }]
    );

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('sendTo', {
            url: '/sendTo',
            templateUrl: 'app/sendTo/sendTo.html',
            controller: 'sendToCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('photoUploadCtrl', ['$scope', '$timeout', '$state', '$window','$sce','photoUploadFactory','$stateParams','popUpManager', function($scope, $timeout, $state, $window,$sce,photoUploadFactory,$stateParams,popUpManager) {
            $scope.photo = {
                link:'',
                text:'',
                html:'',
                error:false
            };
            
            //loading process state
            $scope.photoLoadingProcess = false;
            
            //cropper settings
            var cropper = undefined;
            var options =
		        {
		            imageBox: '.imageBox',
		            thumbBox: '.thumbBox',
		            spinner: '.spinner',
		            imgSrc: '',
		            ratio: undefined
		        }
		        
		    if($stateParams.recruiterIcon)
		    {
			    $('.userIconChoosePhoto i').removeClass('fa fa-user');
			    $('.userIconChoosePhoto i').addClass('fa fa-university');
			    $('.userIconChoosePhoto').css('left','55px');
		    }

            $scope.loadPhotoFromFile  = function(event){
	            
	         	if(event.target.files.length == 0) return;
	            $scope.photoLoadingProcess = true;
	            setTimeout(function(){
		          $scope.$apply($scope.photoLoadingProcess);  
	            }, 0);
	            
                var files = event.target.files;
                var reader = new FileReader();
		        reader.onload = function(event){
                    var the_url = event.target.result;
                    options.imgSrc = the_url;

                    
                    var loadedImage = new Image();
					loadedImage.src = the_url;
					
					loadedImage.onload = function(){
						
						var imageWidth = this.width;
						var imageHeight = this.height;
						
						var el = document.querySelector(options.imageBox);
						var boxWidth = el.clientWidth;
						var boxHeight = el.clientHeight;
						
						if(imageWidth>imageHeight)
						{
							options.ratio = boxWidth/imageWidth;
						}
						else
						{
							options.ratio = boxHeight/imageHeight;
						}
						
						// creating cropper
						cropper = new cropbox(options);
						// creating slider
						$scope.initSlider(options.ratio);
						
					};
					
                    
					
                }
                reader.readAsDataURL(files[0]);
                files = [];
            };
            $scope.prevPhotoLink = photoUploadFactory.currentFactory().getCurrentPhoto().link;
            $scope.cropCancel = function()
            {
	            var imgUrl = $scope.prevPhotoLink;
	            
	            $scope.photo.link = imgUrl;
                $scope.photo.html =  '';
                var el = document.querySelector(options.imageBox);
                el.removeAttribute('style');
                $scope.photoLoadingProcess = false;
//                  cropper.removeCrobBox();
            }
            
            $scope.cropImage = function(){
	            //getting croped picture
	            var imgUrl = cropper.getDataURL();
	            
	            $scope.photo.link = imgUrl;
                $scope.photo.html =  '<img class="thumbBox fullsize" src="'+imgUrl+'" ng-cloak>';
                photoUploadFactory.currentFactory().setCurrentPhoto({
                    link:imgUrl
                });
                $scope.prevPhotoLink = photoUploadFactory.currentFactory().getCurrentPhoto().link;
                $scope.photoLoadingProcess = false;
                publishPhotoAndResume();
                
                var popupMessage = {
					header:'',
					body:'Your photo was successfully uploaded!',
					btnOneName:"OK"
				}
				popUpManager.popupModal(popupMessage);
            };
			
            
            var publishPhotoAndResume = function(){
				photoUploadFactory.currentFactory().publishPhoto().then(
	                function(result){
		                if(result == undefined) return;
		                $scope.photo.link = result;
		                $scope.photo.html =  '<img class="thumbBox fullsize" src="'+result+'" ng-cloak>';
		                photoUploadFactory.currentFactory().setCurrentPhoto({
		                    link:result
		                });
		                $scope.prevPhotoLink = photoUploadFactory.currentFactory().getCurrentPhoto().link;
			            photoUploadFactory.currentFactory().publishResume(true);
		            },
		            function(err){
		            }
	            ); 



			}
            $scope.pushButton = function(buttonID){
                 document.getElementById(buttonID).click();
            };

             $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>2;
            };
            
            $scope.checkForCurrentPhoto = function(){
                if(photoUploadFactory.currentFactory().getCurrentPhoto().link != undefined && photoUploadFactory.currentFactory().getCurrentPhoto().link.length>2){
                    $scope.photo.link = photoUploadFactory.currentFactory().getCurrentPhoto().link;
                    $scope.photo.html = '<img id="photoChoose" src="'+photoUploadFactory.currentFactory().getCurrentPhoto().link+'" ng-cloak>';
                }
            };
            
            //creating slider function
		    $scope.initSlider = function(startPoint){

				    var max = startPoint*7;
				    var min = startPoint-(startPoint/1.5);
				    var step = startPoint/10;
				    var value = startPoint;
				    
				    $( '#slider' ).slider({
				    max:max,
				    min:min,
				    step:step,
				    value:value,
					slide: function( event, ui ) {			
						cropper.zoom(ui.value);
		           }
			    }); 
		    }
            
            $scope.checkForCurrentPhoto();
            
            $scope.goBack = function(){
	            $window.history.back();
            };
            
            $scope.init = function(){
	             Webcam.set({
			        width: 400,
					height: 300,
					
					// device capture size
					dest_width: 533,
					dest_height: 400,
					
					// final cropped size
					crop_width: 400,
					crop_height: 400,
					
					// format and quality
					image_format: 'png',
					jpeg_quality: 90
					
			    });
	            

		        
            };
            $scope.init();
            $scope.cameraMode = false;


            $scope.openCamera = function(){
	             $scope.cameraMode = true;           
	            Webcam.attach( '#cameraView' );
            }
            $scope.closeCamera = function(){
	            $scope.cameraMode = false;
	             Webcam.reset( '#cameraView' );
	             $scope.checkForCurrentPhoto();
            }
            $scope.takePhoto = function(){
	            Webcam.snap( function(data_uri) {
// 		            console.log(data_uri);
		            	$scope.photo.link = data_uri;
		                $scope.photo.html =  '<img class="thumbBox fullsize" src="'+data_uri+'" ng-cloak>';
		                photoUploadFactory.currentFactory().setCurrentPhoto({
		                    link:data_uri
		                });
		                $scope.closeCamera();
		                publishPhotoAndResume();
                        var popupMessage = {
                            header:'',
                            body:'Your photo was successfully saved!',
                            btnOneName:"OK"
                        }
                        popUpManager.popupModal(popupMessage);
                        $scope.goBack();
                    }
                )
            }
            
        }]
    ).factory('photoUploadFactory', function(){
	    var currentFactory = "";
	    return{
		    setCurrentFactory: function(factory){
			    currentFactory = factory;
		    },
		    currentFactory: function(){
			    return currentFactory;
		    }
		};
	});

    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('photoUpload', {
            url: '/photoUpload',
            templateUrl: 'app/photoUpload/photoUpload.html',
            controller: 'photoUploadCtrl'
          });
          $stateProvider
          .state('photoUploadRecruiter', {
            url: '/photoUpload',
            templateUrl: 'app/photoUpload/photoUpload.html',
            controller: 'photoUploadCtrl',
            params:{
	            recruiterIcon:true
            }
          });
      }]);
      
})();

(function () {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('videoUploadCtrl', ['$location','$rootScope','$scope', '$timeout', '$state', '$window','$sce','videoUploadFactory','popUpManager','systemFactory', function($location,$rootScope,$scope, $timeout, $state, $window,$sce,videoUploadFactory,popUpManager,systemFactory) {
            
            
            
	        VideoFileName = $rootScope.currentResumeId+"_"+$rootScope.currentVideoIndex+".mp4";
            $scope.video = {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                };
            $scope.zig = $sce.trustAsHtml("");
            $scope.currentVideoQuestion = $rootScope.currentVideoTitle;    
            $scope.isRecording = false;
            $scope.loadVideoFromFile  = function(event){
		        var fsize = event.target.files[0].size;        
		        if(fsize>1048576*20) //do something if file size more than 1 mb (1048576)
		        {
		            var popupMessage = {
						header:'',
						body:'Selected video too big. Only videos less than 20Mb supported. Please choose another video',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
		            return;
		        }else{
		            //alert(fsize +" bites\nYou are good to go!");
		        }
	           
                var files = event.target.files;
                var reader = new FileReader();
		        reader.onload = function(event){
			        $scope.isCameraMode = true;
			        $scope.waitForVideo = true;
			        var blob = new Blob([event.target.result], {type: 'video/mp4'});
                    uploadBlobsToServer(blob,'.mp4');
					waitForYoutubeVideo();
                }
            
                reader.readAsArrayBuffer(files[0]);
            };
            
            $scope.pushButton = function(buttonID){
                 document.getElementById(buttonID).click();
            };
             $scope.isFilled = function(textToCheck){
                return textToCheck.toString().length>2;
            };
            $scope.checkForCurrentVideo = function(){
	            
	            var videoLink = videoUploadFactory.currentFactory().getCurrentVideo().link;
                if(videoLink.length>2){
                    videoLink = videoLink.replace("https://www.youtube.com/v/","http://www.youtube.com/embed/");
					if(videoLink.indexOf("http") == 0){
						$scope.video.html = $sce.trustAsHtml('<iframe src="'+videoLink+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="640" height="360" frameborder="0" id="videoAnswer"></iframe>');
					}else {
                    	$scope.video.html = $sce.trustAsHtml("<video class='video' controls><source id='vid-source' src='"+videoLink+"?rel=0&"+"' type='video/mp4' onerror='fallback(this)'> </video>");
                    }
                }
            };
            $scope.checkForCurrentVideo();
            $scope.goBack = function(){
	            $window.history.back();
            };

            $scope.fallbackS = function(video){
	            var the_url = "";
                $scope.video.html = $sce.trustAsHtml("");
                videoUploadFactory.currentFactory().setCurrentVideo({
                    link:the_url
                });
                $scope.$apply();
                
                var popupMessage = {
					header:'Wrong video codec format',
					body:'Only H.264 codec supported. Please choose another video',
					btnOneName:'OK'
				}
				popUpManager.popupModal(popupMessage);
            }
            var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
            $scope.isChrome = false;//!!window.chrome && !isOpera;
            $scope.isCameraMode = false;
            $scope.waitForVideo = false;
            $scope.isRequestCanceled = false;
            
            $rootScope.$on('$stateChangeStart', 
						function(event, toState, toParams, fromState, fromParams){
							if(toState.name != "loading") 
						    $scope.isRequestCanceled = true;
						})
            function waitForYoutubeVideo(){
	            systemFactory.waitForChangeOnServer("resume/"+$rootScope.currentResumeId,"video"+$rootScope.currentVideoIndex+"_url",$scope).then(
				function(newField){
					setTimeout(function() {
						
						$scope.isCameraMode = false;
		                newField = newField.replace("watch?v=", "v/");
		                $scope.video.html = $sce.trustAsHtml('<iframe src="'+newField+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen" width="640" height="360" frameborder="0" id="videoAnswer"></iframe>');
		                videoUploadFactory.currentFactory().setCurrentVideo({
		                    link:newField
		                });
		                
						$scope.waitForVideo = false;
						$scope.isRecording = false;
		                videoUploadFactory.currentFactory().publishResume(true);
		                var popupMessage = {
							header:'',
							body:'Your video has successfully uploaded and will be available in a minute.',
							btnOneName:'OK'
						}
							
						popUpManager.popupModal(popupMessage);
						$scope.goBack();
			    	}, 3000);
                },function(fail){
	                if(fail == '0'){
// 		                console.log('canceled req');
	                }else{
		                var popupMessage = {
							header:'Something went wrong...',
							body:'Please try again',
							btnOneName:'OK'
						}
	
						popUpManager.popupModal(popupMessage);
					}
	                $scope.waitForVideo = false;
	                $scope.isRecording = false;
					$scope.isRequestCanceled = false;
                });
	        }
            
			$scope.scriptCamInit = function(){
				//console.log('scriptCamInit');
				$("#webcam").scriptcam({ 
					fileReady:$scope.scopeFileReady,
					country: 'usa',
					cornerRadius:0,
					cornerColor:'e3e5e2',
					onError:onError,
					promptWillShow:promptWillShow,
					showMicrophoneErrors:false,
					onWebcamReady:onWebcamReady,
					setVolume:100,
					timeLeft:timeLeft,
					fileName:'demofilename',
					connected:showRecord,
					width: 640,
					height: 360,
					//zoom: 512.0/640
				});

			}
		
			var recordingDIV = document.querySelector('.containerWebRTC');          
            var webRTCObj = recordingDIV;
            var commonConfig = {
                onMediaCaptured: function(stream) {
                    webRTCObj.stream = stream;
                    if(webRTCObj.mediaCapturedCallback) {
                        webRTCObj.mediaCapturedCallback();
                    }
                },
                onMediaStopped: function() {

                },
                onMediaCapturingFailed: function(error) {
                    
                    commonConfig.onMediaStopped();
                }
            };
            var mimeType = 'video/webm';
            var audioRecorder;
            var videoRecorder; 
            function stopRecording(){
	            function stopStream() {
	                if(webRTCObj.stream && webRTCObj.stream.stop) {
	                    webRTCObj.stream.stop();
	                    webRTCObj.stream = null;
	                }
	            }

	            if(webRTCObj.recordRTC) {
                    webRTCObj.recordRTC[0].stopRecording(function(url) {
                        webRTCObj.recordRTC[1].stopRecording(function(url) {
                            webRTCObj.recordingEndedCallback(url);
                            stopStream();
                        });
                    });
                }
	        }
	        function startRecording(){
	                videoRecorder.initRecorder(function() {
                        audioRecorder.initRecorder(function() {
                            audioRecorder.startRecording();
                            videoRecorder.startRecording();
                        });
                    });
                    webRTCObj.recordRTC.push(audioRecorder, videoRecorder);
                    webRTCObj.recordingEndedCallback = function() {							
						uploadVideoToServer(videoRecorder.blob,audioRecorder.blob);
                    };
	        }
	        function uploadVideoToServer(videoBlob,audioBlob){
		        uploadBlobsToServer(videoBlob,'.webm');
		        uploadBlobsToServer(audioBlob,'.wav');
		        waitForYoutubeVideo();
	        }
	        function uploadBlobsToServer(blob,extension){
		        
	            var uri = "http://apevovideo.cloudapp.net/v/service1.svc/uploadfile";	
				var fd = new FormData();
				fd.append('extension', extension);
				fd.append('data', blob);
				fd.append('resume_id', $rootScope.currentResumeId);
				fd.append('question_order_id', $rootScope.currentVideoIndex);
				fd.append('text',$rootScope.currentVideoTitle);
				
	            $.ajax({
	                type: "POST",
	                url: uri,
	                data: fd,
	                contentType: false,//"multipart/form-data",
	                processData: false,
	
	                success: function (_data) {
// 		                console.log(_data);
	                },
	                failure: function (fail) {
// 	                    console.log(fail);
	                }
	            });
	        }

     
            function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
            }
			$scope.webRTCInit = function(){
				showRecord();
				captureAudioPlusVideo(commonConfig);
                
                webRTCObj.mediaCapturedCallback = function() {
                    webRTCObj.recordRTC = [];
                    if(!window.params.bufferSize) {
                        // it fixes audio issues whilst recording 720p
                        window.params.bufferSize = 16384;
                    }
                    audioRecorder = RecordRTC(webRTCObj.stream, {
                        type: 'audio',
                        bufferSize: typeof window.params.bufferSize == 'undefined' ? 0 : parseInt(window.params.bufferSize),
                        sampleRate: typeof window.params.sampleRate == 'undefined' ? 44100 : parseInt(window.params.sampleRate),
                        leftChannel: window.params.leftChannel || false,
                        disableLogs: window.params.disableLogs || false,
                        recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                    });
                    
                    videoRecorder = RecordRTC(webRTCObj.stream, {
                        type: 'video',
                        // recorderType: isChrome && typeof MediaRecorder !== 'undefined' ? MediaStreamRecorder : null,
                        disableLogs: window.params.disableLogs || false,
                        canvas: {
                            width: 512,
                            height: 288
                        },
                        frameInterval: typeof window.params.frameInterval !== 'undefined' ? parseInt(window.params.frameInterval) : 33 // minimum time between pushing frames to Whammy (in milliseconds)
                    });
                }; 
				
			}
		
			$scope.browserDetect = function(){
				 $scope.isCameraMode = true;
				if($scope.isChrome){
					$scope.webRTCInit();
				}else{
					$scope.scriptCamInit();
				}
			}
			
			$scope.videoFromCam = function(){
				$scope.zig = $sce.trustAsHtml(
				"<ziggeo ziggeo-limit=30 ziggeo-width=640 ziggeo-height=360 ziggeo-recording_width=640 ziggeo-recording_height=360 ziggeo-disable_first_screen=true ziggeo-disable_snapshots=true ziggeo-disable_device_test=true ziggeo-perms='forbidrerecord,forbidswitch'> </ziggeo>");
				$scope.browserDetect();
			}
			
			 var DetectRTC = {};
                (function () {
                    
                    
                    var screenCallback;
                    
                    DetectRTC.screen = {
                        chromeMediaSource: 'screen',
                        getSourceId: function(callback) {
                            if(!callback) throw '"callback" parameter is mandatory.';
                            screenCallback = callback;
                            window.postMessage('get-sourceId', '*');
                        },
                        isChromeExtensionAvailable: function(callback) {
                            if(!callback) return;
                            
                            if(DetectRTC.screen.chromeMediaSource == 'desktop') return callback(true);
                            
                            // ask extension if it is available
                            window.postMessage('are-you-there', '*');
                            
                            setTimeout(function() {
                                if(DetectRTC.screen.chromeMediaSource == 'screen') {
                                    callback(false);
                                }
                                else callback(true);
                            }, 2000);
                        },
                        onMessageCallback: function(data) {
                            if (!(typeof data == 'string' || !!data.sourceId)) return;
                            
                            // "cancel" button is clicked
                            if(data == 'PermissionDeniedError') {
                                DetectRTC.screen.chromeMediaSource = 'PermissionDeniedError';
                                if(screenCallback) return screenCallback('PermissionDeniedError');
                                else throw new Error('PermissionDeniedError');
                            }
                            
                            // extension notified his presence
                            if(data == 'rtcmulticonnection-extension-loaded') {
                                if(document.getElementById('install-button')) {
                                    document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
                                }
                                DetectRTC.screen.chromeMediaSource = 'desktop';
                            }
                            
                            // extension shared temp sourceId
                            if(data.sourceId) {
                                DetectRTC.screen.sourceId = data.sourceId;
                                if(screenCallback) screenCallback( DetectRTC.screen.sourceId );
                            }
                        },
                        getChromeExtensionStatus: function (callback) {
                            if (!!navigator.mozGetUserMedia) return callback('not-chrome');
                            
                            var extensionid = 'ajhifddimkapgcifgcodmmfdlknahffk';
                            var image = document.createElement('img');
                            image.src = 'chrome-extension://' + extensionid + '/icon.png';
                            image.onload = function () {
                                DetectRTC.screen.chromeMediaSource = 'screen';
                                window.postMessage('are-you-there', '*');
                                setTimeout(function () {
                                    if (!DetectRTC.screen.notInstalled) {
                                        callback('installed-enabled');
                                    }
                                }, 2000);
                            };
                            image.onerror = function () {
                                DetectRTC.screen.notInstalled = true;
                                callback('not-installed');
                            };
                        }
                    };
                    
                    // check if desktop-capture extension installed.
                    if(window.postMessage && $scope.isChrome) {
                        DetectRTC.screen.isChromeExtensionAvailable();
                    }
                })();
                
                DetectRTC.screen.getChromeExtensionStatus(function(status) {
                    if(status == 'installed-enabled') {
                        if(document.getElementById('install-button')) {
                            document.getElementById('install-button').parentNode.innerHTML = '<strong>Great!</strong> <a href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk" target="_blank">Google chrome extension</a> is installed.';
                        }
                        DetectRTC.screen.chromeMediaSource = 'desktop';
                    }
                });
                
                window.addEventListener('message', function (event) {
                    if (event.origin != window.location.origin) {
                        return;
                    }
                    
                    DetectRTC.screen.onMessageCallback(event.data);
                });
			
			
			function showRecord() {
				$( "#recordStartButton" ).attr( "disabled", false );
			}
			
			
			$scope.startRecording = function() {

                $( "#timeLeft" ).css('color','red');
                $( "#timeLeft" ).css('font-weight','bold');

				$( "#recordStartButton" ).attr( "disabled", true );
				$( "#recordStopButton" ).attr( "disabled", false );
				$( "#recordPauseResumeButton" ).attr( "disabled", false );
				if($scope.isChrome){
					
					startRecording();
				}else{
					$("#webcam" ).show();
					$.scriptcam.startRecording();
				}

				timeLeft = 30;
				timerOn = true;
			}
			$scope.cancelCamera = function() {
				$scope.isCameraMode = false;
				$scope.waitForVideo = false;
			}
			$scope.closeCamera = function() {
                $( "#timeLeft" ).css('color','white');
                $( "#timeLeft" ).css('font-weight','normal');
				$scope.isCameraMode = true;
				$scope.waitForVideo = true;
			}
			$scope.scopeFileReady = function(fileName){
				
                var fileNameNoExtension = fileName.replace(".mp4", "");
	
				var uri = 'http://apevovideo.cloudapp.net/v/service1.svc/grabvideo?url='+fileName+'&resume_id='+$rootScope.currentResumeId+'&question_order_id='+$rootScope.currentVideoIndex+'&text='+$rootScope.currentVideoTitle;
				$.ajax({
				    type: "GET",
				    url: uri,
				    data:null ,
				    contentType: "application/json",
				    DataType: "json", varProcessData: true,
				   
				    success: function (data) {
					    			
				    },
				    failure: function (fail) {

				    }
				});
				waitForYoutubeVideo();
				
			}
			
			 $rootScope.$on('$locationChangeSuccess', function() {
		        $rootScope.actualLocation = $location.path();
		    });        
		
		   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
		        if($rootScope.actualLocation === newLocation) {
		            if($scope.isChrome){
			            $scope.waitForVideo = true;
						$("#recordPauseResumeButton" ).attr( "disabled", true );
						$("#recordStopButton" ).attr( "disabled", true );
						$('#message').html('Please wait for the video conversion to finish...');
						timeLeft = 30;
						timerOn = false;
			            if(webRTCObj.recordRTC) {
		                    webRTCObj.recordRTC[0].stopRecording(function(url) {
		                        webRTCObj.recordRTC[1].stopRecording(function(url) {
		                            if(webRTCObj.stream && webRTCObj.stream.stop) {
					                    webRTCObj.stream.stop();
					                    webRTCObj.stream = null;
					                }
		                        });
		                    });
		                }
					}
		        }
		    });
		    $scope.ziggeoBegin = function(){
                $scope.isRecording = true;
                $scope.$apply();

		   };
		   $scope.ziggeoFinish = function(){
			   $scope.closeCamera();
			   $scope.$apply();
		   };
		   $scope.ziggeoSubmitted = function(data){
			   $scope.scopeFileReady(ZiggeoApi.Videos.source(data.video.token ));
			   $scope.zig = $sce.trustAsHtml("");
			   
		   }
	        
        }]
    )
    .factory('videoUploadFactory', function(){
	    var currentFactory = "";
	    return{
		    setCurrentFactory: function(factory){
			    currentFactory = factory;
		    },
		    currentFactory: function(){
			    return currentFactory;
		    }
		};
	});


    
	
	
})();
ZiggeoApi.token = "1f24b920b57a81c253ff93266f0afce3";
ZiggeoApi.Events.on("recording", function (data) {
	angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoBegin();
});
ZiggeoApi.Events.on("finished", function (data) {
	angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoFinish();
});
ZiggeoApi.Events.on("submitted", function (data) {
    angular.element(document.getElementById('videoUploadCtrl')).scope().ziggeoSubmitted(data);
});
ZiggeoApi.Events.on("countdown", function (time, data) {
	if(time < 700) document.getElementById("recordingStartAudio").play(); 
});        
        

var VideoFileName = "";
var fallback = function(video){
    angular.element(document.getElementById('workAreaContainer')).scope().fallbackS();
}
function onError(errorId,errorMsg) {
	$scope.waitForVideo = false;
	angular.element(document.getElementById('videoUploadCtrl')).scope().goBack();
}
function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
	$.each(cameraNames, function(index, text) {
		$('#cameraNames').append( $('<option></option>').val(index).html(text) )
	}); 
	$('#cameraNames').val(camera);
	$.each(microphoneNames, function(index, text) {
		$('#microphoneNames').append( $('<option></option>').val(index).html(text) )
	}); 
	$('#microphoneNames').val(microphone);
}
function promptWillShow() {
	//alert('A security dialog will be shown. Please click on ALLOW.');
}

function timeLeft(value) {

}
var timeLeft = 30;
var timerOn = false;
function timer() {
	if(timerOn && timeLeft>0){
		timeLeft = timeLeft-1;
	}else if(timeLeft<=0){
		angular.element(document.getElementById('videoUploadCtrl')).scope().closeCamera();
		angular.element(document.getElementById('videoUploadCtrl')).scope().$apply();
	}
	$('#timeLeft').text("Time Left "+timeLeft+" sec");
}
setInterval(timer,1000);
function changeCamera() {
	$.scriptcam.changeCamera($('#cameraNames').val());
}
function changeMicrophone() {
	$.scriptcam.changeMicrophone($('#microphoneNames').val());
}
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1))) {
        params[d(match[1])] = d(match[2]);
        if(d(match[2]) === 'true' || d(match[2]) === 'false') {
            params[d(match[1])] = d(match[2]) === 'true' ? true : false;
        }
    }
    window.params = params;
    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('videoUpload', {
            url: '/videoUpload',
            templateUrl: 'app/videoUpload/videoUpload.html',
            controller: 'videoUploadCtrl'
          });
      }]);
      
})();

'use strict';


var questions=1;
var questionnaire = false;
var urlparams=[];
var videoSource = [];
var mergedVideo = "";

(function() {

    angular.module('seeMeHireMeApp')
        .controller('webResumeCtrl', ['$rootScope','$scope', '$timeout', '$state', '$window','$sce','prepareData','$stateParams','recruiterFactory','resumeFactory', function($rootScope,$scope, $timeout, $state, $window, $sce, prepareData,$stateParams,recruiterFactory,resumeFactory) {
            
//            COPY TO CLIPBOARD
//            new Clipboard('.clipBoardLinkCopy');
		$rootScope.isWebResume = true;
 		$rootScope.checkIfMobileNoPopup();
 		$scope.isMobile = $rootScope.isMobile;
             $scope.jobseekers = prepareData.data; //add json to object
            $scope.linkToCurrentResume = 'http://reelhire.com/'+$rootScope.idToHash($scope.jobseekers.id,'webResume');
            if(!$scope.jobseekers.ispublished){
	            $scope.linkToCurrentResume = "Publish resume to get link";
            }
	    
	    if($scope.jobseekers.rank<1) // Animation check 
	    {
		    	    	    // ----------   Animation for rate stars   ------------ //
	    var stars = document.getElementsByClassName('answerRatingStars')[0].getElementsByClassName('fa-star');
					
				var timeout;
				var ratingAnimation = setInterval(function(){
		            for(var i=stars.length;i>0;i--)
		            {
			            timeout = setTimeout(function(){
				           var temp = i++;
				           $(stars[temp]).animate({color:"white"});
				           $(stars[temp]).animate({color:"rgb(188,196,200)"}); 
			            }, i*200)
		            }  
	            }, 5000);
	            
	            var onStartClick = function(event){    
		            clearInterval(ratingAnimation);
		            clearTimeout(timeout);
		            
		            var index;
		            for(var i = 0;i<stars.length;i++)
		            {
			            if(stars[i]===event.target)
			            {
				            index = i;
				            break;
			            }
		            }
		            
		            for(var i = 0;i<stars.length;i++)
		            {
			           if(i<=index)
			           	$(stars[i]).css("color","rgb(0,176,80)");
			           else
			           	$(stars[i]).css("color","rgb(188,196,200)");
		            }
	            }
	            $(stars).click(onStartClick);
	            $(".invisible-star").click(onStartClick);
	    //---------------------------- animation end -------------//
		
	    }

		$scope.showBackButton = $stateParams.buttonBack;
		$scope.showRating = $stateParams.showRating;
		$scope.showLink = $stateParams.showLink;
		
		$scope.isFavorite = function(){
            return $.inArray($scope.jobseekers.id, $rootScope.favorites) != -1;
        }
        $scope.setFavorite = function(){
            recruiterFactory.setFavorites($scope.jobseekers.id, $.inArray($scope.jobseekers.id, $rootScope.favorites) == -1);
        }
		$scope.newRating = function(rate){
			$scope.jobseekers.rank = rate;
			recruiterFactory.setRank($scope.jobseekers.id,rate);
		}
		$scope.goBack = function(){
            $window.history.back();
        };
        
        $scope.openLinkedIn =  function(){
	        if(!$scope.jobseekers.linkedIn) return;
	        if($rootScope.isMobile){
				window.open("uniwebview://web?link=" + $scope.jobseekers.linkedIn, '_blank');
            }else{
	            window.open(addLink($scope.jobseekers.linkedIn), '_blank');
            }
        }
        $scope.openAdditionalLink =  function(){
	        if(!$scope.jobseekers.additionalLink) return;
	        if($rootScope.isMobile){
	            window.open("uniwebview://web?link=" + $scope.jobseekers.additionalLink, '_blank');
            }else{
	            window.open(addLink($scope.jobseekers.additionalLink), '_blank');
            }
        }
		
		var personName = $scope.jobseekers.first_name + ' ' + $scope.jobseekers.last_name;//connect name
		
		checkLength(personName);//add name
		
		$scope.jobseekers.location = $scope.jobseekers.city+", "+$scope.jobseekers.state;
		if($scope.jobseekers.zip == "00000"){
			$scope.jobseekers.location = "Non US";
		}
		
		var videoId = $sce.trustAsResourceUrl($scope.jobseekers.video_merged_url).toString().split("?v=");
		
		$scope.videoAnswer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1');//video answer
		
		if($scope.jobseekers.video_merged_url === "local"){
			mergedVideo = "";
			videoSource[0] = $scope.jobseekers.video1_url.replace("watch?v=", "v/");
			videoSource[1] = $scope.jobseekers.video2_url.replace("watch?v=", "v/");
			videoSource[2] = $scope.jobseekers.video3_url.replace("watch?v=", "v/");
			videoSource[3] = $scope.jobseekers.video4_url.replace("watch?v=", "v/");
			var videoId = videoSource[0].split("v/");

			$scope.videoAnswer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1');
		}else{
			mergedVideo = 'https://www.youtube.com/embed/' + videoId[1] + '?enablejsapi=1';
		}
		
		
		
		questions = 1;
		
		if($scope.jobseekers.video2_title.length>0)
			questions=2;
			
		if($scope.jobseekers.video3_title.length>0)
			questions=3;
		
		if($scope.jobseekers.video4_title.length>0)
			questions=4;
				
		numberOfAnswers(questions);//see if there are all video answers
			
		questionnaire = false;
		if($scope.jobseekers.companyLogo&&$scope.jobseekers.companyName&&$scope.jobseekers.companyPosition&&$scope.jobseekers.companyInfo)
			questionnaire = true;
					
		headerType(questionnaire);//if resume or questionnaire
		
		var atrl = 'Yes';
		
		if($scope.jobseekers.abletorelocate==0)
			atrl = 'No';
			
		$scope.abletorelocate = atrl;// able to relocate

		
		urlparams = undefined;

		if(player != undefined) onYouTubeIframeAPIReady();
		
		$scope.openApp = function(){
			if($rootScope.isMobile){
				window.open("http://reelhire.com/"+$rootScope.idToHash($scope.jobseekers.id,'webResume'));
// 				 window.open("uniwebview://web?link=" + "reelhire://?resume="+$scope.jobseekers.id, '_blank');
			}
			else{
				$rootScope.outsideResumeId = $scope.jobseekers.id;
				$state.go('login');
			}
		}
		$scope.sendEmail = function(){
	           $window.location = "mailto:" + $scope.jobseekers.email + "?" +
			"subject=I%20found%20your%20resume%20on%20ReelHire.com&body=Email%20Body"; 
		}
		
		$rootScope.$on('$stateChangeStart', function(){ 
		    $rootScope.isWebResume = false;
		})
    }]);
})();

function checkLength(a){
	
	var seekername = a;
	var nameLength = seekername.length;
	var standard = 30;
	var minusfont = 0;
	var nameFont = 0;
	var namepadding= 6;

	if(nameLength>standard)
	{
		for(var i = 1;i<=100;i++)
		{
			minusfont+=2;
			standard+=10;
			if(standard>nameLength)
			{
				nameFont = 30 - minusfont;
				namepadding+=minusfont;
				$("#name").append('<p class="boldFont">' + seekername + '</p>' );
				$("#name p").css({'font-size':nameFont});
				
				break;
			}
		}
	}
	else
	{
		$("#name").append('<p class="boldFont">' + seekername + '</p>' );
		$("#name p").css({'font-size':standard});
		
	}
	

}
function onStateChangeTest(){
// 	console.log("onStateChangeTest");
}
function numberOfAnswers(a){
	if(a==3)
	{
		$("#q4").hide();
	}
	else if(a==2)
	{
		$("#q3").hide();
		$("#q4").hide();
	}
	else if(a==1)
	{
		$("#q2").hide();
		$("#q3").hide();
		$("#q4").hide();
	}
}

function headerType(a){
	if(a)
		$("#resumeTop").remove();
	else
		$("#questionnaireTop").remove();
}

function addLink(teststring){
	if(!teststring) return "";
	var patternHttp = /^http(s)?:\/\//;
	var patternWww = /www./;
	var template = 'http://www.';
	
	if(patternHttp.test(teststring) && patternWww.test(teststring))
	{
		return teststring;
	}
	else if(patternHttp.test(teststring))
	{
		return teststring.replace(patternHttp, template);
	}
	else if(patternWww.test(teststring))
	{
		return teststring.replace(patternWww, template);
	}
	else
	{
		return teststring = template + teststring;
	}

}


function getUrlVars()
{
    //console.log('inside');

    var vars = [];
    var hash = '';
    var hashes = window.location.href.slice(window.location.href.indexOf('?')+1);
    
    vars = hashes.split('&');
    
    if(vars[1])
    {
		vars[1] = vars[1].slice(vars[1].indexOf('=')+1);
	}
    return vars;

}
var player;
var videoNumber = 1;
function onPlayerStateChange(event) {
	
    switch(event.data) {
      case 0:
      	if(mergedVideo.length > 0){
	      	player.cueVideoByUrl(mergedVideo, 0,"large");
		  	player.playVideo();
      	}else{
	      	videoNumber = videoNumber + 1;
	      	if(videoNumber > questions) videoNumber = 1;
	      	player.cueVideoByUrl(videoSource[videoNumber-1]+ '?enablejsapi=1', 0,"large");
	      	player.playVideo();
        }
        break;
    }
}
function onYouTubeIframeAPIReady() {
	player = new YT.Player( 'videoAnswer', {
	  	events: { 'onStateChange': onPlayerStateChange }
	});
}

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider,resumeFactory,questionnarieFactory,$rootScope) {
        $stateProvider
          .state('webResume', {
            url: '/webResume',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:false,
	            showRating:false,
	            showLink:true
            },
            resolve: {
	            prepareData: ['resumeFactory', function(resumeFactory)
				{
					return resumeFactory.getResumeFromUrlId();
				}]
            }
          });
      $stateProvider
          .state('publishedQuestionnaire', {
            url: '/publishedQuestionnaire',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:false
            },
            resolve: {
	            prepareData: ['questionnarieFactory', function(questionnarieFactory)
				{
					return questionnarieFactory.getPublishedQuestionnarieData();
				}]
            }
          });
      $stateProvider
      .state('questionnairesPreview', {
        url: '/questionnairesPreview',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:false
            },
        resolve: {
            prepareData: ['questionnarieFactory', function(questionnarieFactory)
			{
				return questionnarieFactory.getQuestionnairesPreviewData();
			}]
        }
      });
      $stateProvider
      .state('resumePreview', {
        url: '/resumePreview',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:false,
	            showLink:true
            },
        resolve: {
            prepareData: ['resumeFactory', function(resumeFactory)
			{
				return resumeFactory.resumeData();
			}]
        }
      });
      $stateProvider
      .state('resumeAnswer', {
        url: '/resumeAnswer',
        templateUrl: 'app/webResume/webResume.html',
        controller: 'webResumeCtrl',
        params:{
	            buttonBack:true,
	            showRating:true,
	            showLink:true
            },
        resolve: {
            prepareData: ['$rootScope', function($rootScope)
			{
				$rootScope.currentQuestionnarie.data = $rootScope.currentQuestionnarie;
				return $rootScope.currentQuestionnarie;
			}]
        }
      });
 	}]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('contactUs', {
            url: '/contactUs',
            templateUrl: 'app/website/pages/contactUs.html'
          });
      }]);
    
    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('termsAndConditions', {
            url: '/termsAndConditions',
            templateUrl: 'app/website/pages/termsAndConditions.html'
          });
      }]);
     angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('privacyPolicy', {
            url: '/privacyPolicy',
            templateUrl: 'app/website/pages/privacyPolicy.html'
          });
      }]);
      angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('help', {
            url: '/help',
            templateUrl: 'app/website/pages/help.html'
          });
      }]);
})();
(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('InsideQuestionnaireCtrl', ['$http','$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','$stateParams','$filter','$window','systemFactory',
        function($http,$scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,$stateParams,$filter,$window,systemFactory) {
	        
	        // Filtered Candidates
			var currentFilteredCandidates = [];
			$rootScope.selectedCandidates = [];
            
            $scope.goBack = function(){
	            $window.history.back();
            };
			
            $scope.isChecked = false;
            $scope.previewAnket = function(){
	            $state.go("previewQuestionnaire");
            }
            $scope.candidateClick = function(candidate){
	            $rootScope.currentQuestionnarie = candidate;
	            candidate.isNew = false;
	            recruiterFactory.deleteNotification(candidate.notificationId);
	            $state.go("resumeAnswer");
            }
            $scope.candidates = [];
            $scope.linkToAnket = "";        
            
            $scope.explanaitionPopupCheck = function(){
                if($scope.candidates.length<1)
                    return true;
                return false;
            }
            
                
            var request = {};
            if($stateParams.typeOfList == undefined){
	            request = {
		            'anket_id':$rootScope.currentAnket.id
	            };
	            $scope.showPreviewQuestionnaire = true;
		        $scope.linkToAnket = "http://reelhire.com/"+$rootScope.idToHash($rootScope.currentAnket.id,'webAnket');
            }else if($stateParams.typeOfList == 'searchAll'){
	            request = {
		            'anket_id': systemFactory.systemAnketID()
	            };
	            
	            $scope.showPreviewQuestionnaire = false;
            }else if($stateParams.typeOfList == 'favorites'){
	            request = {
		           
	            };
	            $scope.showPreviewQuestionnaire = false;
            }
            
            recruiterFactory.searchOnServer(request,$scope.candidates);
            recruiterFactory.getListOfFavorites();
            
            $scope.activeFilters = [];
            $scope.sortOptions = ["Experience from less to more","Experience from more to less","Date of Questionnaire from new to old","Date of Questionnaire from old to new","Rating from small to big","Rating from big to small"];
            $scope.filters = {};
            $scope.filters.categories = ["Select Filter Category","Industry Desired","Experience","Education","Area of Study","Able to Relocate","Location"];
            $scope.filters._categories = ["industrydesired","expirience","education","areaofstudy","abletorelocate","location"];
            $scope.filters.filters = [];
            $scope.filterBy = {};
            $scope.filterBy.category = $scope.filters.categories[0];
		    $scope.filterBy.filter = "";
		    $scope.emptyFilter = "";
		    $scope.sortBy = {};
		    
            $scope.onSortChanged = function(){
// 				console.log($scope.sortBy.sort);
				
            }

			

            $scope.sortedFunction = function(candidate){
	            if($scope.sortBy.sort === $scope.sortOptions[0]){
		            return $scope.experiences.indexOf(candidate.expirience);
	            }else if($scope.sortBy.sort === $scope.sortOptions[1]){
		            return -$scope.experiences.indexOf(candidate.dateadded);
	            }else if($scope.sortBy.sort === $scope.sortOptions[2]){
		            return -candidate.dateadded.replace("/Date(", "").replace("+0000)/", "");
	            }else if($scope.sortBy.sort === $scope.sortOptions[3]){
		            return candidate.dateadded.replace("/Date(", "").replace("+0000)/", "");   
	            }else if($scope.sortBy.sort === $scope.sortOptions[4]){
		            return candidate.rank;
	            }else if($scope.sortBy.sort === $scope.sortOptions[5]){
		            return -candidate.rank;
	            }
            }
            $scope.isFavorite = function(candidate){
	            return $.inArray(candidate.id, $rootScope.favorites) != -1;
            }
            $scope.setFavorite = function(candidate){
	            recruiterFactory.setFavorites(candidate.id, $.inArray(candidate.id, $rootScope.favorites) == -1);
            }
            
            $scope.countSelectedCandidates = function(){
	           
	            var selectedCandidates = [];
	            for(var i=0;i<$scope.candidates.length;i++){
			       if($scope.candidates[i].isSelected){
				       selectedCandidates.push($scope.candidates[i]);
			       }
		        }
		        $rootScope.selectedCandidates = selectedCandidates;
				return selectedCandidates.length>0;
            }
            
            
            
            
            //function rewriting to selected candidates object
			$scope.filteredCandidatesFn = function(filteredCandidates)
			{
			   if(!_.isEqual(currentFilteredCandidates, filteredCandidates))
			   {
			       currentFilteredCandidates = [];
			       _.extend(currentFilteredCandidates,filteredCandidates);
			   }
			   
			   return true;
			}
            
            // function to select/deselect all filtered candidates
			$rootScope.selectAllCandidates = function(){
			   if($rootScope.selectAllText === 'Select All')
			   {
			       for(var i = 0;i<currentFilteredCandidates.length;i++)
			       {
			           currentFilteredCandidates[i].isSelected = true;
			       }
			       _.extend($rootScope.selectedCandidates,currentFilteredCandidates); 
			   }
			   
			   if ($rootScope.selectAllText === 'Deselect All')
			   {
			       for(var i = 0;i<currentFilteredCandidates.length;i++)
			       {
			           currentFilteredCandidates[i].isSelected = false;
			       }
			       $rootScope.selectedCandidates = [];
			   }
			
			}
            
            $scope.customFilter = function(candidate){
	            if($stateParams.typeOfList == 'favorites'){
			        if(!$scope.isFavorite(candidate)) return false;
		        }
	            if($scope.activeFilters.length < 1) return true;
	            
	            var candidateReq = [];
	            for(var j=0;j<$scope.filters._categories.length;j++){
			        candidateReq[j] = 1;
		        }
		        
	            for(var i=0;i<$scope.activeFilters.length;i++){
		            for(var j=0;j<$scope.filters._categories.length;j++){
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && candidateReq[j]==1){
				            candidateReq[j] = 0;
			            }
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && $scope.activeFilters[i].filter === candidate[$scope.activeFilters[i]._category] && $scope.filters._categories[j] != "location"){
				            candidateReq[j] = 2;
			            }
			            if($scope.activeFilters[i]._category === $scope.filters._categories[j] && $scope.filters._categories[j] == "location"){
				            if(($scope.activeFilters[i].filter == $scope.locationOptions[0] && candidate["zip"] != "00000") || 
				            ($scope.activeFilters[i].filter == $scope.locationOptions[1] && candidate["zip"] == "00000") ){
				            	candidateReq[j] = 2;
				            }
			            }
			        }
	            }
	            
				var intFilter = 1;
				for(var j=0;j<$scope.filters._categories.length;j++){
			        intFilter *= candidateReq[j];
		        }
		        
	            return intFilter>0;
            }
            $scope.checkForOutSideResume = function(candidate){
		        if($rootScope.outsideResumeId != undefined && $rootScope.outsideResumeId != "" && $rootScope.outsideResumeId != candidate.id){
					return false;
				}
				return true;
            }
            $scope.$on('$destroy', function() {
			  	$rootScope.outsideResumeId = "";
			})
            $scope.isSelectSecondFilter = false;
            $scope.onCategoryChange = function(){
	            
	            $scope.isSelectSecondFilter = true;
	            $scope.emptyFilter = "Select Filter";
	            
	            if($scope.filterBy.category === $scope.filters.categories[1]){
		            $scope.filters.filters = $scope.professions;
		            $scope.filterBy._category = $scope.filters._categories[0];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[2]){
		            $scope.filters.filters = $scope.experiences;
		            $scope.filterBy._category = $scope.filters._categories[1];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[3]){
		            $scope.filters.filters = $scope.educations;
		            $scope.filterBy._category = $scope.filters._categories[2];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[4]){
		            $scope.filters.filters = $scope.professions;
		            $scope.filterBy._category = $scope.filters._categories[3];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[5]){
		            $scope.filters.filters = $scope.abletorelocateOptions;
		            $scope.filterBy._category = $scope.filters._categories[4];
	            }
	            else if($scope.filterBy.category === $scope.filters.categories[6]){
		            $scope.filters.filters = $scope.locationOptions;
		            $scope.filterBy._category = $scope.filters._categories[5];
	            }
	            else{
		            $scope.emptyFilter = "";
		            $scope.filterBy.filter = "";
		            $scope.isSelectSecondFilter = false;
	            }
            }
            
            $scope.onFilterChange = function(){
	            if($scope.filterBy.filter != $scope.emptyFilter && $scope.filterBy.filter != null){
		            $scope.newFilter();
		            $scope.isSelectSecondFilter = false;
		            $scope.filterBy.category = $scope.filters.categories[0];
		            $scope.filters.filters= [];
		            $scope.emptyFilter = "";
		        }
	        }
	        
	        $scope.newFilter = function(){
		        var activeFilter = {};
		        activeFilter._category = ""+$scope.filterBy._category;
		        activeFilter.category = ""+$scope.filterBy.category;
		        activeFilter.filter = ""+$scope.filterBy.filter;
		        
		        $scope.activeFilters.unshift(activeFilter);
	        }
	        
	        $scope.deleteFilter = function(filter){
		         for(var i=0;i<$scope.activeFilters.length;i++){
		            if(JSON.stringify($scope.activeFilters[i]) === JSON.stringify(filter) ){
			            $scope.activeFilters.splice(i, 1);
			            break;
		            }
	            }		
	        }
	        
            $http.get('app/resources/professions.json').success(function(data) {
                $scope.professions = data;
            });
            $scope.educations = ["No Education","High School","Technical School","College","Masters","Doctorate"];
            $scope.experiences = [ '1 year and less','1-2 years','3-5 years','5-10 years','10-15 years','20+ years'];
            $scope.abletorelocateOptions = ['Yes','No']; 
            $scope.locationOptions = ['US Candidates','Non US Candidates'];
            
            $scope.open = function(){
				window.open($scope.linkToAnket);
			};   
        }]);
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('insideQuestionnaire', {
                        url: '/insideQuestionnaire',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
                $stateProvider
                    .state('searchAll', {
                        url: '/searchAll',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        params:{
				            typeOfList: 'searchAll',
			            },
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                       
	                        }]
                        }
                    });    
                $stateProvider
                    .state('favorites', {
                        url: '/favorites',
                        templateUrl: 'app/insideQuestionnaire/insideQuestionnaire.html',
                        controller: 'InsideQuestionnaireCtrl',
                        params:{
				            typeOfList: 'favorites',
			            },
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                       
	                        }]
                        }
                    });    
            }
        ]);
})();
(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('NewQuestionnaireCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','popUpManager','photoUploadFactory',
        function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,popUpManager,photoUploadFactory) {
	        photoUploadFactory.setCurrentFactory(recruiterFactory);
	    	$scope.anket = $rootScope.currentAnket;
	    	$scope.error = recruiterFactory.errorField();
	    	$scope.isFilled = function(textToCheck){
                return textToCheck != undefined && textToCheck.toString().length>0;
            };
            $scope.setCurrentPhoto = function(name,link){
	            if($scope.anket.ispublished == "True") return;
	            $state.go("photoUploadRecruiter");
	            $scope.error.companyLogo = false;
                recruiterFactory.setCurrentPhoto({link:$scope.anket.companyLogo});
            }
            
            $scope.publishclick = function(){
// 	            console.log($scope.error);
	            if(recruiterFactory.checkAnket($scope.anket,$scope.error)  && recruiterFactory.checkRegistration()){  
	                recruiterFactory.publishPhoto($scope.anket.companyLogo).then(
		                function(result){
			                $scope.anket.companyLogo = result;
				            recruiterFactory.publishAnket($scope.anket);
			            },
			            function(err){
				            
			            }
		            ); 
                }
            };
            
            $scope.checkForCurrentPhoto = (function(){
                if(recruiterFactory.getCurrentPhoto().link.length>2){
                    if(recruiterFactory.getCurrentPhoto().name === 'companyLogo'){
                        $scope.anket.companyLogo = recruiterFactory.getCurrentPhoto().link;
                    }
                    recruiterFactory.resetCurrentPhoto();
                }
            });
            $scope.checkForCurrentPhoto();
            $scope.textSend = function(){
	            if($rootScope.selectedCandidates != undefined && $rootScope.selectedCandidates.length > 0){
		            return "Save and Send";
	            }
	            return "Save And Get Link";
	        }
	        $scope.isSelectedCandidates = function(){
				if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
					return false;
				}else{
					return true;
				}
			}    
            $scope.send = function(){
	            recruiterFactory.sendAnket();
            } 
	            
        }]);
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('newQuestionnaire', {
                        url: '/newQuestionnaire',
                        templateUrl: 'app/newQuestionnaire/newQuestionnaire.html',
                        controller: 'NewQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    })
                    .state('previewQuestionnaire', {
                        url: '/previewQuestionnaire',
                        templateUrl: 'app/newQuestionnaire/newQuestionnaire.html',
                        controller: 'NewQuestionnaireCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
            }
        ]);
})();
(function() {

    'use strict';

    var appLoading = angular.module('seeMeHireMeApp')
        .controller('RecruiterQuestionnairesCtrl', ['$scope', '$state', '$rootScope', '$cookies','resourceLogin', '$resource','$timeout','recruiterFactory','popUpManager',
            function($scope, $state, $rootScope, $cookies, resourceLogin, $resource, $timeout,recruiterFactory,popUpManager) {
	            
                $scope.$watch('firstLoginType',function(newValue,oldValue){
                   if(newValue!== null && newValue != undefined)
                        {
                           popUpManager.welcomeRecruiter();
                        }
               });
                
	            $scope.ankets = recruiterFactory.ankets();
	            $scope.anketId = function(anket){
		            return -Number(anket.id);
	            }
				$scope.copyAnket = function(anket){
					recruiterFactory.copyAnket(anket);
				};
                
				$scope.isSelectedCandidates = function(){
					if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
						return false;
					}else{
						return true;
					}
				}
				$scope.sendAnket = function(anket){
// 					console.log(anket);
					$rootScope.currentAnket = anket;
					recruiterFactory.sendAnket();
				};
				$scope.deleteAnket = function(anket){
					var popupMessage = {
						header:'Are you sure?',
						body:'Do you want to delete the Questionnaire for the position "' + anket.companyPosition + '"?',
						btnOneName:'Delete',
						btnTwoName:'Cancel',
						btnOneClick:function(){
							recruiterFactory.deleteAnket(anket);
						},
						btnTwoClick:function(){
							
						}
					}
	
					popUpManager.popupModal(popupMessage);
					
				};
				$scope.newAnket = function(){
					$rootScope.currentAnket = recruiterFactory.newAnket();
					$state.go("newQuestionnaire");
				};
				$scope.ifEmpty = function(text,ifEmpty){
					if(text.length > 0) return "";
					return ifEmpty;
				};
				$scope.openAnket = function(anket){
					$rootScope.currentAnket = anket;
					
					if(anket.ispublished === "True"){
						if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
							$state.go("insideQuestionnaire");
						}else{
							$state.go("previewQuestionnaire");
						}
					}else{
						$state.go("newQuestionnaire");
					}
				}
        }]);
})();
(function() {
    'use strict';
    angular.module('seeMeHireMeApp')
        .config(['$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state('recruiterQuestionnaires', {
                        url: '/recruiterQuestionnaires',
                        templateUrl: 'app/recruiterQuestionnaires/recruiterQuestionnaires.html',
                        controller: 'RecruiterQuestionnairesCtrl',
                        resolve: {
	                        stateOutput: ['$state', function ($state)
	                        {
		                      
	                        }]
                        }
                    });
            }
        ]);
})();
(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('PageMainCtrl', ['$scope',  '$state', 'resourceLogin','$window','resourceRegister','$rootScope','$location', function($scope, $state, resourceLogin,$window,resourceRegister,$rootScope,$location)
        {
            $scope.videoIsCollapsed = true;
            
            $scope.btnToState = function(state,userType){
                if(!$rootScope.checkIfMobile()){
	                $rootScope.userType = userType;
	                $rootScope.userEmail = "anonimus";
                    $state.go(state);
                }   
            };
            $scope.contactUs = function(){
                $state.go('contactUs')
            }
			
            
            $('.linkToMainVideo').bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1000, 'easeInOutExpo');
                event.preventDefault();
            });
        }
    ]);

    
    
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('main', {
            url: '/main',
            templateUrl: 'app/website/main/pageMain.html',
            controller: 'PageMainCtrl'
          });
      }]);
      
})();

(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
        .controller('AboutCtrl', ['$scope', '$state', 'resourceLogin','$window','resourceRegister','$stateParams','$rootScope', function($scope, $state, resourceLogin,$window,resourceRegister,$stateParams,$rootScope)
        {
		   if($stateParams.isJobSeeker===true)
               $scope.aboutState = 'jobseeker';
               
            if($scope.aboutState === 'jobseeker'){
                $scope.companyInfo = 'Hello Seeker';
                }
            else{
                $scope.companyInfo = 'Hello You'
            }
            
            $rootScope.$on('$routeChangeStart', function(event, currRoute, prevRoute){
                $rootScope.animation = currRoute.animation;
            });
            
            $scope.btnToState = function(state,userType){
                if(!$rootScope.checkIfMobile()){
	                $rootScope.userType = userType;
	                $rootScope.userEmail = "anonimus";
                    $state.go(state);
                }   
            };
                
        }]
    );

    
})();
(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('about', {
            url: '/about',
            templateUrl: 'app/website/about/about.html',
            controller: 'AboutCtrl',
            animation: 'third'
          });
      }]);
})();

(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('PanelCtrl', 
        ['$scope', '$location', '$window','$state','resumeFactory','questionnarieFactory','popUpManager','toggleSubButtons','$rootScope', function($scope, $location, $window, $state,resumeFactory,questionnarieFactory,popUpManager,toggleSubButtons,$rootScope) {
			$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			$scope.updateNotifications = function(){
				$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			}
            
            // Checks for the second time enterto page
            $scope.$watch('userType', function(newValue){
                if(newValue === undefined || newValue === null || newValue === '')
                    {
                        var temp = $location.path();
                        
                        if(temp !== '/main' && temp !== '/about' && temp !== '/webResume' && temp !== '/contactUs' && temp !== '/termsAndConditions' && temp !== '/privacyPolicy' && temp !== '/resetPwd' && temp !== '/'  && temp !== '/help'){
                            $state.go('login');
                        }
                    }
	       });

			$scope.onPage = function (states) {
				
				if(states.constructor===Array)
				{
					for(var i=0;i<states.length;i++){
					if(states[i] === $location.path())
					return states[i] === $location.path();
					}
				}
				else
				return states === $location.path();
            };
            
            $scope.onState = function(newState){
	            if(newState === "questionnairesPreview"){
		            if(questionnarieFactory.checkCurrentQuestionnarie()){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "resumePreview"){
		            if(resumeFactory.checkResume(resumeFactory.myResume())){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "questionnaires"){
		            if(!_.isEmpty(questionnarieFactory.ankets())){
		           		$state.go(newState);
		           	}
	            }
	            else if(newState === "getLink"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }else if(newState === "sendTo"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           	var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }
	            
            }
        }]
    );

})();

(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('panelRecruiterCtrl', 
        ['$scope','$rootScope', '$location', '$window','$state','resumeFactory','questionnarieFactory','popUpManager','recruiterFactory', function($scope, $rootScope, $location, $window, $state,resumeFactory,questionnarieFactory,popUpManager,recruiterFactory) { 
            
            console.log($rootScope.favorites);
            
			$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			
			$scope.updateNotifications = function(){
				$scope.notificationQuestionnaries = questionnarieFactory.notifications().length;
			}
			
			//side butttons checks if they are disabled or not
			$scope.goToLink = function(className, state, emailType)
			{
				if(recruiterFactory.checkRegistration() && !$('.'+ className).hasClass( "disabled" ))
				{
					$state.go(state);
					$rootScope.emailType = emailType;
				}
			}
			
			//select All rootscope variable
			$rootScope.selectAllText = 'Select All';
			
			//On change of state zero selected candidates and select all button
			$rootScope.$on('$stateChangeStart', function(){ 
			    $rootScope.selectAllText = 'Select All';
			    //$rootScope.selectedCandidates = [];
			})
			
			// function that changes select all to deselect all
			$scope.selectAll = function(){
				
				//function from inside questionnaires
				$rootScope.selectAllCandidates();
				
				if($rootScope.selectAllText === 'Select All')
				{
					$rootScope.selectAllText = 'Deselect All';
				}
				else
				{
					$rootScope.selectAllText = 'Select All';
				}
			}
			
			$scope.onPage = function (states) {
				
				if(states.constructor===Array)
				{
					for(var i=0;i<states.length;i++){
					if(states[i] === $location.path())
					return states[i] === $location.path();
					}
				}
				else
				return states === $location.path();
            };
            
            $scope.onState = function(newState){
	            if(newState === "questionnairesPreview"){
		            if(questionnarieFactory.checkCurrentQuestionnarie()){
		           		$state.go(newState);
		           	}
	            }
                else if(newState === 'recruiterQuestionnaires'){
                    $rootScope.selectedCandidates = [];
                    $state.go(newState);
                }
	            else if(newState === 'favorites'){
		            if($rootScope.favorites.length>0)
		            $state.go(newState);
	            }
	            else if(newState === "resumePreview"){
		            if(resumeFactory.checkResume(resumeFactory.myResume())){
		           		$state.go(newState);
		           	}
	            }else if(newState === "getLink"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }else if(newState === "sendTo"){
		            if(resumeFactory.isCurrentVersionPublished()){
		           		$state.go(newState);
		           	}else{
			           	
			           	var popupMessage = {
							header:'',
							body:'Please publish the current version of your resume or restore the version that you have already publised to get a url link to your resume',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
		           	}
	            }
	            
            }
        }]
    )
    .service('toggleSubButtons', function(){
	    return {
		    Disable:function(className){
			    $('.' + className).addClass("disabled");
		    },
		    Enable:function(className){
			    $('.' + className).removeClass("disabled");
		    }
	    };
    });

})();

(function() {

    'use strict';

    var appTutorial = angular.module('seeMeHireMeApp')
    .controller('TutorialCtrl', ['$scope', '$localStorage', '$rootScope', '$state', 'questionnarieFactory', 'toggleSubButtons', 'resumeFactory', 'recruiterFactory', function($scope,$localStorage,$rootScope,$state,questionnarieFactory,toggleSubButtons,resumeFactory,recruiterFactory){

            $( window ).scroll(function() {
                $('.popover').hide();
            });

            //Check if tutorial was previously turned on or off
            if($localStorage.tutorialIsActive===undefined)
            {
                $localStorage.tutorialIsActive = true;
                $scope.tutorialStatus = 'Off';
            }
            else if ($localStorage.tutorialIsActive===true)
            {
                $scope.tutorialStatus = 'Off';
            }
            else
            {
                $scope.tutorialStatus = 'On';
            }

            //function to turn off or on tutorial
            $scope.toggleTutorial = function(){
                if($localStorage.tutorialIsActive)
                {
                    $localStorage.tutorialIsActive = false;
                    $rootScope.tutorialStatusChanged = $localStorage.tutorialIsActive;
                    $scope.tutorialStatus = 'On';
                }
                else {
                    $localStorage.tutorialIsActive = true;
                    $rootScope.tutorialStatusChanged = $localStorage.tutorialIsActive;
                    $scope.tutorialStatus = 'Off';
                }
            }
            
            //Questionnaire dependency if there are questionnaires or not
            $scope.checkSeekerQuestionnaires = function()
            { 
	            if(!_.isEmpty(questionnarieFactory.ankets()))
                {
                    toggleSubButtons.Enable('seekerQuestionnaires');
                    
                    if($scope.tutorialSeekerQuestionnaires !== '')
                    $scope.tutorialSeekerQuestionnaires = 'Click to see your questionnaires';
                }
                else
                {
                    toggleSubButtons.Disable('seekerQuestionnaires');
                    
					if($scope.tutorialSeekerQuestionnaires !== '')
                    {
	                    $scope.tutorialSeekerQuestionnaires = 'This will become available once you receive a questionnaire from a recruiter';
                    }
                    
                }
                return true;
            }
            
            // Get Link and Send To dependency
            //Enables and disables get Link and Send To based on the published or unpublished resume
            $scope.checkSeekerInformation = function(){
                if(resumeFactory.isEqualWithServer() && resumeFactory.isCurrentVersionPublished())
                {
                    toggleSubButtons.Enable('myResumeGetLink');
                    toggleSubButtons.Enable('myResumeSendTo');
                    if($scope.tutorialSeekerGetLink !== '')
                    {
	                    $scope.tutorialSeekerGetLink = 'Click to get link to your resume';
						$scope.tutorialSeekerSendTo = 'Click to send your resume via e-mail';
                    }
                }
                else
                {
                    toggleSubButtons.Disable('myResumeGetLink');
                    toggleSubButtons.Disable('myResumeSendTo');
                    if($scope.tutorialSeekerGetLink !== '')
                    {
                    	$scope.tutorialSeekerGetLink = 'Publish your resume to get a link to it';
						$scope.tutorialSeekerSendTo = 'Publish your resume to send it via e-mail';
                    }
                }

                return true;
            }
            
            
            
            
            // Watches for is resume is filled or not to show Preview
            $scope.$watch('resumeScopeShare',function(newValue,oldValue){

                if(resumeFactory.checkResumeIsFilled(newValue))
                {
                    toggleSubButtons.Enable('myResumePreview');
                    if($scope.tutorialSeekerPreview !== '')
                    {
	                    $scope.tutorialSeekerPreview = 'Click to preview your Resume';
                    }
                }
                else
                {
                    toggleSubButtons.Disable('myResumePreview');
                    if($scope.tutorialSeekerPreview !== '')
                    {
	                    $scope.tutorialSeekerPreview = 'Fill all fields to preview your Resume';
                    }
                }
            },true);
            
            
            //checks if any candidates are selected
            $scope.$watch('selectedCandidates',function(newValue,oldValue){
	            if(!_.isEmpty(newValue))
	            {
		            toggleSubButtons.Enable('emailToSelectedClass');
		            toggleSubButtons.Enable('sendSelectedClass');
					toggleSubButtons.Enable('newQuestionnaireClass');
					if($scope.tutorialRecruiterEmailToSelected !== '') 
					{
						$scope.tutorialRecruiterEmailToSelected = 'Select candidates to send them an email';
						$scope.tutorialRecruiterSendSelected = 'Select candidates and include them in your email';
						$scope.tutorialRecruiterSendNewQuestionnaire = 'Select candidates to send them a new questionnaires';
					}
	            }
	            else
	            {
		           toggleSubButtons.Disable('emailToSelectedClass');
		           toggleSubButtons.Disable('sendSelectedClass');
		           toggleSubButtons.Disable('newQuestionnaireClass');
		           if($scope.tutorialRecruiterEmailToSelected !== '')
		           {
			           $scope.tutorialRecruiterEmailToSelected = 'Select atleast one candidate';
					   $scope.tutorialRecruiterSendSelected = 'Select atleast one candidate';
					   $scope.tutorialRecruiterSendNewQuestionnaire = 'Select atleast one candidate';
		           }
	            }
            },true);
            
            //check for favourites are there any
            //PROBLEM I need to call favorites here
            $scope.$watch('favorites',function(newValue,oldValue){
	        	if($rootScope.favorites.length>0)
	        	{
		        	toggleSubButtons.Enable('favouritesClass');
		        	$scope.tutorialRecruiterFavorites = 'See all the people you checked as favorites';
	        	}
	        	else
	        	{
		        	toggleSubButtons.Disable('favouritesClass');
		        	$scope.tutorialRecruiterFavorites = 'Add candidates to favourites to activate this folder';
	        	}
	        },true);
            

            //watching if tutorial has been turned off or on
            $scope.$watch('tutorialStatusChanged',function(){

                if ($localStorage.tutorialIsActive) {
                    $scope.tutorialOnBtnContinue = 'Continue and register later';
                    $scope.tutorialOnChoosingSeeker = 'Choose, if you are a job seeker';
                    $scope.tutorialOnChoosingRecruiter = 'Or, are you a recruiter?';
                    $scope.tutorialOnAddingAPhoto = 'Click here to add a photo';
                    $scope.tutorialOnAddingAVideo = 'Click here to answer the record an answer';
                    $scope.tutorialMergedVideo = 'All your videos are merged to ';
                    $scope.tutorialOnPublish = 'This will make a public version of your resume';
                    $scope.tutorialOnRestore = 'This will take your resume back to the last published version';
                    $scope.tutorialSeekerQuestionnaires = 'testfield';
                    $scope.tutorialSeekerPreview = 'testfield';
                    $scope.tutorialSeekerGetLink = 'testfield';
					$scope.tutorialSeekerSendTo = 'testfield';
                    $scope.tutorialSeekerExample = 'Click to see what you will receive after publishing you resume';
                    $scope.tutorialSeekerNewQuestionnaire = 'Click here to see your questionnaire';
                    $scope.tutorialQuestionnairePosition = 'This is the position you are applying for';
                    $scope.tutorialQuestionnaireVideo = 'This a a question asked by recruiter. Click to add a video answer';
                    $scope.tutorialQuestionnaireSend = 'This will send your resume to a recruiter. Once sent, it can not be changed';
                    $scope.tutorialMergedVideo = 'Your videos were combined for recruiter convenience';
                    $scope.tutorialRecruiterCreateNewQuestionnaire = 'Click to create new questionnaire';
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to see responses to your questionnaire';
                    $scope.tutorialRecruiterCopyQuestionnaire = 'Click to copy your questionnaire';
                    $scope.tutorialRecruiterOnAddingAPhoto = 'Click here to add your company photo';
                    $scope.tutorialRecruiterBasicInformation = 'You will receive all this info about a candidate automatically';
                    $scope.tutorialRecruiterQuestions = 'Ask your question and receive a video answer';
                    $scope.tutorialRecruiterQuestionairePublish = 'This will publish your questionnaire and you will be able to send it';
                    $scope.tutorialRecruiterSeeCandidateQuestionnaire = 'Click to see job seeker';
                    $scope.tutorialRecruiterQuestionairePreview = 'Click to view your questionnaire';
                    $scope.tutorialRecruiterAddToFavourites = 'Add candidate to Favourites';
                    $scope.tutorialRecruiterEmailToSelected = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSendSelected = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSendNewQuestionnaire = 'Select atleast one candidate';
                    $scope.tutorialRecruiterSeeCandidate = 'Click to see candidate\'s resume';
                    $scope.tutorialRecruiterSearchAll = 'Search all ReelHire base of job seekers';
                    $scope.tutorialRecruiterFavorites = 'Add candidates to favourites to activate this folder';
                    $scope.tutorialRecruiterPreviewQuestionnaire = 'Click to preview your questionnaire';
                    $scope.tutorialRecruiterLinkQuestionnaire = 'This is an external link to your questionnaire';
                    $scope.tutorialRecruiterSearchJobseekers = 'Search ReelHire user base and send them this questionnaire';
                    $scope.tutorialRecruiterQuestionaireInstantSend = 'Click to send questionnaire to job seekers';
                }
                else{
                    $scope.tutorialOnBtnContinue = '';
                    $scope.tutorialOnChoosingSeeker = '';
                    $scope.tutorialOnChoosingRecruiter = '';
                    $scope.tutorialOnAddingAPhoto = '';
                    $scope.tutorialOnAddingAVideo = '';
                    $scope.tutorialOnPublish = '';
                    $scope.tutorialOnRestore = '';
                    $scope.tutorialSeekerQuestionnaires = '';
                    $scope.tutorialSeekerPreview = '';
                    $scope.tutorialSeekerGetLink = '';
                    $scope.tutorialSeekerSendTo = '';
                    $scope.tutorialSeekerExample = '';
                    $scope.tutorialSeekerNewQuestionnaire = '';
                    $scope.tutorialQuestionnairePosition = '';
                    $scope.tutorialQuestionnaireVideo = '';
                    $scope.tutorialQuestionnaireSend = '';
                    $scope.tutorialMergedVideo = '';
                    $scope.tutorialRecruiterCreateNewQuestionnaire = '';
                    $scope.tutorialRecruiterGoInsideQuestionnaire = '';
                    $scope.tutorialRecruiterCopyQuestionnaire = '';
                    $scope.tutorialRecruiterOnAddingAPhoto = '';
                    $scope.tutorialRecruiterBasicInformation = '';
                    $scope.tutorialRecruiterQuestions = '';
                    $scope.tutorialRecruiterQuestionairePublish = '';
                    $scope.tutorialRecruiterSeeCandidateQuestionnaire = '';
                    $scope.tutorialRecruiterQuestionairePreview = '';
                    $scope.tutorialRecruiterAddToFavourites = '';
                    $scope.tutorialRecruiterEmailToSelected = '';
                    $scope.tutorialRecruiterSendSelected = '';
                    $scope.tutorialRecruiterSendNewQuestionnaire = '';
                    $scope.tutorialRecruiterSeeCandidate = '';
                    $scope.tutorialRecruiterSearchAll = '';
                    $scope.tutorialRecruiterFavorites = '';
                    $scope.tutorialRecruiterPreviewQuestionnaire = '';
                    $scope.tutorialRecruiterLinkQuestionnaire = '';
                    $scope.tutorialRecruiterSearchJobseekers = '';
                    $scope.tutorialRecruiterQuestionaireInstantSend = '';
                }
            });
        
        $scope.$watch('selectedCandidates',function(newValue,oldValue){
                if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to see responses to your questionnaire';
                }
                else{
                    $scope.tutorialRecruiterGoInsideQuestionnaire = 'Click to preview your questionnaire';
                }
            });


    }]);
})();



(function() {
    'use strict';

angular.module('seeMeHireMeApp')
    .controller('TopbarCtrl', 
        ['$scope', '$location', '$window','$state','systemFactory','$rootScope', function($scope, $location, $window, $state, systemFactory,$rootScope) {  
            
            //$scope.account = 'Login';
            $scope.inMenu = function(){
                if(systemFactory.md5().length>5)
                {
                    
                    if($location.path()==='/about' || $location.path()==='/main'){
                        $scope.account = 'My Account';
                    }
                    else{
                        return false;
                    }
                }
                else{
                    
                    $scope.account = 'Log In';
                }
                
                return true;
            };
            
            $scope.btnToState = function(state){
                if(!$rootScope.checkIfMobile()){
                    $state.go(state);
                }   
            };
            
            
        }]
    );

})();

(function(){
	'use strict';
	
	angular.module('seeMeHireMeApp')
	.service('popUpManager', ['$uibModal', function($uibModal){
		return{
			popupModal: function(modalMessageObject){
			    $uibModal.open({
			      templateUrl: 'app/popup/popup.html',
			      controller: 'testModalCtrl',
			      backdrop: 'static',
				  keyboard: false,
			      resolve: {
				    popupMessage:function(){
					    return modalMessageObject;
				    }
			      }
			    });   
		    },
            welcomeJobSeeker:function()
            {
                $uibModal.open({
                    templateUrl:'app/popup/welcomeJobSeeker.html',
                    controller: 'WelcomePopUpController',
                    backdrop: 'static',
                    keyboard: false
                });
            },
            welcomeRecruiter:function()
            {
                $uibModal.open({
                    templateUrl:'app/popup/welcomeRecruiter.html',
                    controller: 'WelcomePopUpController',
                    backdrop: 'static',
                    keyboard: false
                });
            }
		};
	}])
    .controller('WelcomePopUpController',['$scope', '$rootScope', '$uibModalInstance', function($scope, $rootScope, $uibModalInstance){
        $scope.btnClickOK = function(){
            $rootScope.firstLoginType = null;
            $uibModalInstance.close();
        }
    }])
	.controller('testModalCtrl',['$scope','$uibModalInstance','popupMessage',function($scope, $uibModalInstance, popupMessage){

// template for creating a modal popup
// by default btnOne closes popup
		$scope.popupText = {
			header:'',
			body:'',
			btnOneName:'',
			btnTwoName:'',
			btnOneClick:'function',
			btnTwoClick:'function'
		}
		
		//displaying second button
		$scope.secondButton = true;
		
		$scope.popupText.header = popupMessage.header;
		$scope.popupText.body = popupMessage.body;
		$scope.popupText.btnOneName = popupMessage.btnOneName;
		$scope.popupText.btnTwoName = popupMessage.btnTwoName;
		
		if($scope.popupText.btnTwoName===undefined)
		{
			$scope.secondButton = false;
		}


		$scope.btnOneClick = function(){
			
			if(typeof popupMessage.btnOneClick === 'function')
			popupMessage.btnOneClick();
			
			$uibModalInstance.close();
		}
		
		$scope.btnTwoClick = function(){
			
			if(typeof popupMessage.btnTwoClick === 'function')
			popupMessage.btnTwoClick();
			
			$uibModalInstance.close();
		}
	}])
})();







(function() {
	
    'use strict';

    var appResumeEdit = angular.module('seeMeHireMeApp')	 	
	.factory('resumeFactory', ['$rootScope', '$http', 'cookieManager', '$sce', 'systemFactory', '$q', 'popUpManager', '$state', '$location', function($rootScope,$http,cookieManager,$sce,systemFactory,$q,popUpManager,$state,$location){
	    var allResume = [];
        var sysAnketID = "0";
        var emptyResume =  {
                 firstname : {text:'',error:false},
				 lastname : {text:'',error:false},
				 email : {text:'',error:false},
				 zipcode : {text:'',error:false},
                 city : '',
				 state : '',
				 education : {text:'',error:false},
				 areaofstudy : {text:'',error:false},
				 experience : {text:'',error:false},
				 industrydesired : {text:'',error:false},
				 abletorelocate : {text:'',error:false},
                 linkedIn : {text:''},
				 additionalLink : {text:''},
                 ispublished: false,
                 photo : {link:'', text:'',error:false},
				 video1 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video2 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video3 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 video4 : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                },
                 videoMerged : {
                    link:'',
                    text:'',
                    html:'',
                    error:false
                }
            };
        var myResume = jQuery.extend(true, {}, emptyResume);
        var serverResume = {};

        var currentVideo = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var currentPhoto = {
                name:'',
                link:'',
                html:'',
                error:false
        };
         var restoreResume = function(firstLoad){
                if(!serverResume.ispublished && !firstLoad) return;
                if(!serverResume.hasOwnProperty('id')) return;
                
                var abletorelocate = (serverResume.abletorelocate=="1" || serverResume.abletorelocate==="Yes")?'Yes':'No'; 
                
                myResume.firstname = {text:serverResume.first_name,error:false};
                myResume.lastname = {text:serverResume.last_name,error:false};
                myResume.email = {text:serverResume.email,error:false};
                myResume.zipcode = {text:serverResume.zip,error:false};
                myResume.city = serverResume.city;
                myResume.state = serverResume.state;
                myResume.education = {text:serverResume.education,error:false};
                myResume.areaofstudy = {text:serverResume.areaofstudy,error:false};
                myResume.experience = {text:serverResume.expirience,error:false};
                myResume.industrydesired = {text:serverResume.industrydesired,error:false};
                myResume.abletorelocate = {text:abletorelocate,error:false};
                myResume.linkedIn = {text:serverResume.linkedIn};
                myResume.additionalLink = {text:serverResume.additionalLink};
                myResume.photo = {link:serverResume.photo_url,text:'',error:false};
       
                myResume.video1.link = serverResume.video1_url.replace("watch?v=", "v/");
		        myResume.video2.link = serverResume.video2_url.replace("watch?v=", "v/");
		        myResume.video3.link = serverResume.video3_url.replace("watch?v=", "v/");
		        myResume.video4.link = serverResume.video4_url.replace("watch?v=", "v/");
		        myResume.video1.html = "";
		        myResume.video2.html = "";
		        myResume.video3.html = "";
		        myResume.video4.html = "";
                myResume.videoMerged.link = serverResume.video_merged_url;
                myResume.videoMerged.html = $sce.trustAsHtml('<iframe src="'+serverResume.video_merged_url.replace("https://www.youtube.com/watch?v=", "http://www.youtube.com/embed/")+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  width="100%"  height="100%"></iframe>');
                
                serverResume.myResumeFormat = jQuery.extend(true,{},myResume);            
            };
            var setMyResumeFormatForServerResume = function(){	        
                if(!serverResume.ispublished) return;
                
                var abletorelocate = (serverResume.abletorelocate=="1" || serverResume.abletorelocate==="Yes") ?'Yes':'No';
                var myResumeFormat = {};
                
                myResumeFormat.firstname = {text:serverResume.first_name,error:false};
                myResumeFormat.lastname = {text:serverResume.last_name,error:false};
                myResumeFormat.email = {text:serverResume.email,error:false};
                myResumeFormat.zipcode = {text:serverResume.zip,error:false};
                myResumeFormat.city = serverResume.city;
                myResumeFormat.state = serverResume.state;
                myResumeFormat.education = {text:serverResume.education,error:false};
                myResumeFormat.areaofstudy = {text:serverResume.areaofstudy,error:false};
                myResumeFormat.experience = {text:serverResume.expirience,error:false};
                myResumeFormat.industrydesired = {text:serverResume.industrydesired,error:false};                
                myResumeFormat.abletorelocate = {text:abletorelocate,error:false};
                myResumeFormat.linkedIn = {text:serverResume.linkedIn};
                myResumeFormat.additionalLink = {text:serverResume.additionalLink};
                myResumeFormat.photo = {link:serverResume.photo_url,text:'',error:false};
                myResumeFormat.videoMerged = {};
                myResumeFormat.videoMerged.link = serverResume.video_merged_url;
                myResumeFormat.videoMerged.html = $sce.trustAsHtml('<iframe src="'+serverResume.video_merged_url.replace("watch?v=", "v/")+"?rel=0&"+'" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"  width="100%"  height="100%"></iframe>');
                
                serverResume.myResumeFormat = myResumeFormat;
                
            };
            var isEqualWithServer = function(isPublishButton){
	            var isEqual = true;

                if(Object.keys(serverResume).length < 1 || !serverResume.ispublished){
	                if(isPublishButton){
		                return false;
	                }else{
	                 	return true;
	                }
                }
                
                jQuery.each(serverResume.myResumeFormat, function(key, val) {	                
                    if(val != myResume[key] && val.constructor != {}.constructor){
                        isEqual = false;
                        return false;
                    }
                    if(key.indexOf("video")>-1 && key!="videoMerged") return true;
                    if(val.constructor != {}.constructor)  return true;
                    jQuery.each(val, function(key1, val1) {
	                    
                        if(myResume[key][key1]==null || val1.toString() !== myResume[key][key1].toString()){
                            isEqual = false;
                            return false;
                        }
                    });
                });
                
                return isEqual;
            }
	    return {
		    restoreResume: function(firstLoad){
			    restoreResume(firstLoad);
		    },
		    emptyResume: function(){
			    return jQuery.extend(true, {}, emptyResume);
		    },
            getCurrentVideo: function(){
			    return currentVideo;
		    },
            setCurrentVideo: function(video){
                jQuery.each(currentVideo, function(key, val) {
                    if(video.hasOwnProperty(key)){
                        currentVideo[key] = video[key];
                    }
                });
                myResume["video"+$rootScope.currentVideoIndex].link = video.link;
		    },
            resetResume:function(){
            	myResume = jQuery.extend(true, {}, emptyResume);
            	currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
            	 currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
                serverResume = {};
                allResume = [];
            },
            resetCurrentVideo: function(){
                currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            setCurrentPhoto: function(photo){
                jQuery.each(currentPhoto, function(key, val) {
                    if(photo.hasOwnProperty(key)){
                        currentPhoto[key] = photo[key];
                        myResume.photo.link = photo[key];
                    }
                });
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            myResume: function(){
                return myResume;
            },
            serverResume: function(){
                return serverResume;
            },
            setServerResume: function(_serverResume){
                serverResume = _serverResume;
            },
            setMyResume: function(newResume){
                jQuery.each(myResume, function(key, val) {
                    if(newResume.hasOwnProperty(key)){
                        myResume[key] = newResume[key];
                    }
                });
            },
            allResume: function(){
			    return allResume;
		    },
			
		    getAllResume: function(cookieObject){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'resumes/all',
					headers: {
					   'skey': systemFactory.md5(),
					}
				};  
			    return $http(requestData).then(
				    	function(result){
					    	
					    	allResume = result.data;					    	
							for(var i =0;i<allResume.length;i++)
                            {
	                          	var isPubl = allResume[i].ispublished.toString().toLowerCase()==="true"?true:false;
								allResume[i].ispublished = isPubl;
								// if url starts with "/" then video was filmed on mobile
								if(allResume[i].video1_url.indexOf("/") == 0) allResume[i].video1_url = "";
								if(allResume[i].video2_url.indexOf("/") == 0) allResume[i].video2_url = "";
								if(allResume[i].video3_url.indexOf("/") == 0) allResume[i].video3_url = "";
								if(allResume[i].video4_url.indexOf("/") == 0) allResume[i].video4_url = "";
	                        }  
                            for(var i =0;i<allResume.length;i++)
                            {
                                myResume = jQuery.extend(true, {}, emptyResume);
                                serverResume = {};
                                if(sysAnketID == allResume[i].anket_id){
                                	serverResume = allResume[i];
	                                allResume.splice(i,1);
	                                restoreResume(true);
									return result;
								}
							}						
							return {};


	                    }
					);
			    },
            updateAllResume:function(cookieObject){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'resumes/all',
					headers: {
					   'skey': systemFactory.md5(),
					}
				};  
			    return $http(requestData).then(
				    	function(result){	          
							return allResume;
	                    }
					);
			    },
            checkResumeIsFilled: function(resume){
                var isCorrect = true;

                if(resume === undefined || resume === '')
                {
                    return false;
                }

                jQuery.each(resume, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
                        if(!val.hasOwnProperty('link')){
                            if(!val.text || val.text === undefined || val.text.length<1 || val.error) {
                                isCorrect = false;
                            }
                        }
                        else{
                            if(!val.link || val.link === undefined || val.link.length<1 ){
                                if(key === 'photo'){
                                    isCorrect = false;
                                }else if (val.text==undefined || val.text.length > 1 && (!resume.hasOwnProperty('videoMerged') || resume.videoMerged.link.length<1)){
                                    isCorrect = false;
                                }
                            }
                        }
                    }
                });

                return isCorrect;
            },
            checkRegistration: function(){
	            //console.log($rootScope.userEmail.length);
	            var ifRegistered = $rootScope.userEmail.indexOf("anonimus") != 0 || $rootScope.userEmail.length != 21;
	            if(!ifRegistered){
		            var popupMessage = {
						header:'',
						body:'Please register to have access for the all functionality',
						btnOneName:'Register',
						btnTwoName:'Register Later',
						btnOneClick:function(){
							$state.go('register');
						},
						btnTwoClick:function(){
							
						}
					}

					popUpManager.popupModal(popupMessage);
	            }
	            return ifRegistered;
            },
			checkResume: function(resume){
                var isCorrect = true;

                jQuery.each(resume, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
						if(!val.hasOwnProperty('link')){
	                        if(!val.text || val.text === undefined || val.text.length<1 || val.error) {
	                            val.error = true;
	                            isCorrect = false;
	                        }
	                    }else{
		                    if(!val.link || val.link === undefined || val.link.length<1 ){
			                    if(key === 'photo'){
			                    	val.error = true;
		                            isCorrect = false;
	                            }else if (val.text.length > 1 && (!resume.hasOwnProperty('videoMerged') || resume.videoMerged.link.length<1)){
		                            val.error = true;
		                            isCorrect = false;
		                        }
		                    }
	                    }
                    }
                });

                if(!isCorrect){

	               var popupMessage = {
						header:'',
						body:'Please check fields marked with red',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);

                }
                return isCorrect;
            },
            getSysAnketID:function(){
                
                var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'config',
                    data: {'name':'public_anket'}
                };	
                return $http(requestData).then(
                    function(result){
                        sysAnketID = result.data;
                        return result.data;
                    }
                );
            },
	        getResumeFromUrlId: function()
			{
				urlparams=getUrlVars();	
				
				var resumeDecodedId = $rootScope.hashToId(urlparams[0],'webResume');
					
				var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'resume/'+ resumeDecodedId,
                    headers: {
					   'skey': urlparams[1]
					}
                };	
				return $http(requestData)
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        getExampleData: function()
			{
				urlparams=getUrlVars();			

				return $http.get(systemFactory.serverUrl()+'resume/36',null) 
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        isEqualWithServer: function(isPublishButton){
                return isEqualWithServer(isPublishButton);
            },
            isCurrentVersionPublished:function(){
	            var isCurrentVersionPublished = isEqualWithServer();
	            if(Object.keys(serverResume).length < 1) return false;
	            if(serverResume.ispublished == false || serverResume.ispublished == "false") return false;
                return isCurrentVersionPublished;
            },
            resumeData: function()
			{
				urlparams=getUrlVars();	
				var currentResume = {};
				
				currentResume.first_name = myResume.firstname.text;
				currentResume.last_name = myResume.lastname.text;
				currentResume.email = myResume.email.text;
				currentResume.zip = myResume.zipcode.text;
				currentResume.city = myResume.city;
				currentResume.state = myResume.state;
				currentResume.education = myResume.education.text;
				currentResume.areaofstudy = myResume.areaofstudy.text;
				currentResume.expirience = myResume.experience.text;
				currentResume.industrydesired = myResume.industrydesired.text;
				currentResume.abletorelocate = (myResume.abletorelocate.text=="1" || myResume.abletorelocate.text==="Yes") ?"1":"0";
				currentResume.linkedIn = myResume.linkedIn.text;
				currentResume.additionalLink = myResume.additionalLink.text;
				currentResume.photo_url = myResume.photo.link;
				currentResume.video1_title = myResume.video1.text;
				currentResume.video2_title = myResume.video2.text;
				currentResume.video3_title = myResume.video3.text;
				currentResume.video4_title = myResume.video4.text;				
				currentResume.video1_url = myResume.video1.link;
				currentResume.video2_url = myResume.video2.link;
				currentResume.video3_url = myResume.video3.link;
				currentResume.video4_url = myResume.video4.link;
				
				if(myResume.videoMerged.html.length<1){
					currentResume.video_merged_url = "local";
				}else{
					currentResume.video_merged_url = serverResume.video_merged_url;
				}
				
				currentResume.data = currentResume;
				return currentResume;	        
			},
			publishResume: function (isBackgroundPublish){
				if (typeof(isBackgroundPublish)==='undefined') isBackgroundPublish=false;
				var resumeToPublish = {};
				resumeToPublish.companyInfo = "";
				resumeToPublish.companyLogo = "";
				resumeToPublish.companyName = "";
				resumeToPublish.companyPosition = "";
				resumeToPublish.first_name = myResume.firstname.text;
				resumeToPublish.last_name = myResume.lastname.text;
				resumeToPublish.email = myResume.email.text;
				resumeToPublish.zip = myResume.zipcode.text;
				resumeToPublish.city = myResume.city;
				resumeToPublish.state = myResume.state;
				resumeToPublish.education = myResume.education.text;
				resumeToPublish.areaofstudy = myResume.areaofstudy.text;
				resumeToPublish.expirience = myResume.experience.text;
				resumeToPublish.industrydesired = myResume.industrydesired.text;
				resumeToPublish.abletorelocate = (myResume.abletorelocate.text=="1" || myResume.abletorelocate.text==="Yes")?"1":"0";
				resumeToPublish.linkedIn = myResume.linkedIn.text;
				resumeToPublish.photo_url = myResume.photo.link;
				resumeToPublish.additionalLink = myResume.additionalLink.text;
				resumeToPublish.video1_title = myResume.video1.text;
				resumeToPublish.video2_title = myResume.video2.text;
				resumeToPublish.video3_title = myResume.video3.text;
				resumeToPublish.video4_title = myResume.video4.text;				
				resumeToPublish.video1_url = myResume.video1.link;
				resumeToPublish.video2_url = myResume.video2.link;
				resumeToPublish.video3_url = myResume.video3.link;
				resumeToPublish.video4_url = myResume.video4.link;				
				resumeToPublish.video1_thumbnail_url = "";
				resumeToPublish.video2_thumbnail_url = "";
				resumeToPublish.video3_thumbnail_url = "";
				resumeToPublish.video4_thumbnail_url = "";
				
				resumeToPublish.video_merged_title = "";
				resumeToPublish.video_merged_url = myResume.videoMerged.link;
				resumeToPublish.video_merged_thumbnail_url = "";
				
				resumeToPublish.ispublished = !isBackgroundPublish;
				resumeToPublish.anket_id = sysAnketID;
				if(Object.keys(serverResume).length > 1)
				resumeToPublish.id = serverResume.id;

				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':resumeToPublish},
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };
                
                $http(requestData).then(
                    function(result){
	                    var resumeId = result.data;
                        if(resumeId !== -1){
	                        if(!isBackgroundPublish){
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your resume has been published. It will be available in several minutes via the link that you can find in \"Get Link\" menu',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
		                        serverResume = resumeToPublish;
		                        
		                        restoreResume(true);
		                    }else{
								serverResume = resumeToPublish;
			                    serverResume.id = resumeId;
			                     $rootScope.currentResumeId = resumeId;
			                    resumeToPublish.ispublished = false;
		                    }
                        }
                    },function(error){
	                    
	                     var popupMessage = {
							header:'Something went wrong...',
							body:'You resume was not published. Try again',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
	                    
	                }
                );
	            
			},
			publishPhoto:function(){
				return $q(function(resolve, reject) {
					if(myResume.photo.link.search('data') == 0){
						var imageData = myResume.photo.link;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl()+"uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your photo was not uploaded. Try again',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
					        	
								reject(info);
				        	}
				        })
				        .error(function(err){
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);					        

				        	reject(err);
				        });
				    }else{
					    resolve(myResume.photo.link);
				    };
				});
            },
    	}; 
    
	}]);
	appResumeEdit.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
	}]);
})();


(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appEmails = angular.module('seeMeHireMeApp')	 	
	.factory('emailFactory', ['$http', '$sce', 'resumeFactory', 'systemFactory', '$rootScope', function($http,$sce,resumeFactory,systemFactory,$rootScope){
        var candidates = [];
	    return{
		    candidates: function(){
			    if(systemFactory.userType() == "seeker"){
				    var serverResume = resumeFactory.serverResume();
				    candidates = [];
				    candidates[0] = {};
				    candidates[0].photoLink = serverResume.photo_url;
					candidates[0].firstName = serverResume.first_name;
					candidates[0].lastName = serverResume.last_name;
					candidates[0].resumeId = serverResume.id; 
			    }else{
				    candidates = [];
				    for(var i=0;i<$rootScope.selectedCandidates.length;i++){
					    candidates[i] = {};
					    candidates[i].photoLink = $rootScope.selectedCandidates[i].photo_url;
						candidates[i].firstName = $rootScope.selectedCandidates[i].first_name;
						candidates[i].lastName = $rootScope.selectedCandidates[i].last_name;
						candidates[i].resumeId = $rootScope.selectedCandidates[i].id;
						candidates[i].email = $rootScope.selectedCandidates[i].email;  
						
				    }
			    }
			    return candidates;
		    },
		    resetCandidates: function(){
			    candidates = [];
		    },
		    emailTitle: function(){
			    if(systemFactory.userType() == "seeker"){
				    return "";//"Please see my completed Video Resume & Questionnaire";
				}else{
					if($rootScope.emailType == 'emailSelected'){
						return "";//"This is a candidate of interest. Please check out the Video Resume";
					}
					else{
						return "";
					}
				}
				return true;
		    },
            checkEmail: function(scope){
                var isCorrect = true;
                jQuery.each(scope, function(key, val) {
                    if(val && val.constructor === {}.constructor  && val.hasOwnProperty('error') ) {
	                    
                        if(!val.text || val.text === undefined || val.text.length<1 ) {
	                        console.log(val);
                            val.error = true;
                            isCorrect = false;   
                        }
                    }
                });
                return isCorrect;
            }
    	}; 
    
	}]);
})();
(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appResumeEdit = angular.module('seeMeHireMeApp')
   	.factory('systemFactory',['$q', '$http', '$rootScope', 'popUpManager', function($q,$http,$rootScope,popUpManager){
        var systemAnketID;
        var systemAnket = {};
        var userType = "none";
        var md5 = "";
        var mc = {};
        var welcomeSeekerEmailTemplate = "";
        var welcomeRecruiterEmailTemplate = "";
        
        $rootScope.idToHash = function(id,key){	        
	        var hashids = new Hashids(key, 8,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	        id = Number(id);
	        return hashids.encode(id);
        };
        
        $rootScope.hashToId = function(hash,key){
	        var hashids = new Hashids(key, 8,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	        return hashids.decode(hash);
        };
		
		$rootScope.checkIfMobile = function()
        {
            if( navigator.userAgent.match(/Android/i)
             || navigator.userAgent.match(/webOS/i)
             || navigator.userAgent.match(/iPhone/i)
             || navigator.userAgent.match(/iPad/i)
             || navigator.userAgent.match(/iPod/i)
             || navigator.userAgent.match(/BlackBerry/i)
             || navigator.userAgent.match(/Windows Phone/i)
             ){
                var popupMessage = {
					header:'',
					body:'Please download ReelHire mobile app or use desktop version!',
					btnOneName:"OK",
					btnOneClick:function(){}
				}

				popUpManager.popupModal(popupMessage);
                return true;
            }
            else {
                return false;
            }
        };
        $rootScope.checkIfMobileNoPopup = function()
        {
             if( navigator.userAgent.match(/Android/i)
	             || navigator.userAgent.match(/webOS/i)
	             || navigator.userAgent.match(/iPhone/i)
	             || navigator.userAgent.match(/iPad/i)
	             || navigator.userAgent.match(/iPod/i)
	             || navigator.userAgent.match(/BlackBerry/i)
	             || navigator.userAgent.match(/Windows Phone/i)
             ){
	            $rootScope.isMobile = true;
                return true;
            }
            else {
	            $rootScope.isMobile = false;
                return false;
            }
        };
        
        $rootScope.isWebResume = false;
        
        var loadWelcomeEmailTemplates = function(){
	        var client = new XMLHttpRequest();
			client.open('GET', '/app/serverRequests/welcomeEmailSeeker.html');
			client.onreadystatechange = function() {
			  	welcomeSeekerEmailTemplate = client.responseText;
			}
			client.send();
			
			var client2 = new XMLHttpRequest();
			client2.open('GET', '/app/serverRequests/welcomeEmailRecruiter.html');
			client2.onreadystatechange = function() {
			  	welcomeRecruiterEmailTemplate = client2.responseText;
			}
			client2.send();
        };
        loadWelcomeEmailTemplates();
        
		
        var factory  = {
	        serverUrl: function(){
		        return 'http://apevo.cloudapp.net/service.svc/';
		    },
	        md5: function(){ 
                return md5;
            },
            setMd5: function(_md5){ 
                md5 = _md5;
            },
	        userType: function(){ 
                return userType;
            },
            setUserType: function(typeOfUser){ 
                userType = typeOfUser;
            },		
            systemAnketID: function(){ 
                return systemAnketID;
            },
            getSystemAnketID:function(){
                var requestData = {
                    method: 'POST',
                    url: factory.serverUrl()+'config',
                    data: {'name':'public_anket'}
                };	
                return $http(requestData).then(
                    function(result){
                        systemAnketID = result.data;
                        return result.data;
                    }
                );
            },
            systemAnket: function(){ 
                return systemAnket;
            },
            getSystemAnket:function(){
                var requestData = {
                    method: 'GET',
                    url: factory.serverUrl()+'anket/'+systemAnketID,
                    headers: {'skey':md5}
                };	
                return $http(requestData).then(
                    function(result){
                        systemAnket = result.data;
                        return result.data;
                    }
                );
            },
            sendWelcomeEmail: function(email,userType){
	            var message = "";
	            if(userType === "seeker"){
		            message = welcomeSeekerEmailTemplate;
	            }else{
		            message = welcomeRecruiterEmailTemplate;
	            }
	            
	            
	          	var request = $http({
				    method: "post",
				    url: "/components/sendgrid-php/myMail.php",
				    data: {
					    sm: mc.id,
					    sp: mc.value,
				        emailTo: email,
				        emailFrom: "no-reply@reelhire.com",
				        subject : "Welcome to ReelHire",
						message : message
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					request.success(function (dataQQQ) {
	
					});  
            },
            mc: function(){
	          return mc;  
            },
            getMailConfig:function(){
                var requestData = {
                    method: 'GET',
                    url: factory.serverUrl()+'systemmail/zxc34223145'
                };	
                return $http(requestData).then(
                    function(result){
                        mc = result.data;
                        mc.value = "IlInam17";
                    }
                );
            },
            waitForChangeOnServer: function (url,fieldToLookAt,scope) {
			  	return $q(function(resolve, reject) {				  		
				  	var requestData = {
	                    method: 'GET',
	                    url: factory.serverUrl()+url,
	                    headers: {'skey':md5}
	                };
	                var attempt = 0;
	                var maxAttempt = 50;
					var prevField = "";
					
		            function newRequest(){ 
			            if(scope.isRequestCanceled){
				            reject('0');
				            return;
				        }  	
		                $http(requestData).then(
							function(result){
								var data = result.data;
		                        var newField = data[fieldToLookAt];
		                        if(attempt == 0){
			                        prevField = newField;
			                        setTimeout(function() {
										newRequest();
							    	}, 3000);
		                        }else{
			                        if(prevField === newField){
				                        if(attempt<maxAttempt){
					                        setTimeout(function() {
												newRequest();
									    	}, 3000);
					                    }else{
						                    reject('server doesn\'t have changes');
					                    }
			                        }else{
				                        resolve(newField);
			                        }
		                        }
		                        attempt = attempt + 1;
							}
						);
		            };   
					newRequest();
			  	});
			}
        }
        return factory;
     }])
     .directive('myOnChange', function() {
      return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                  var onChangeHandler = scope.$eval(attrs.myOnChange);
                  element.bind('change', onChangeHandler);
            }
        };
    });
     
	
})();
(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var appResumeEdit = angular.module('seeMeHireMeApp')	 	
	.factory('questionnarieFactory', ['$rootScope', '$http', 'cookieManager', 'systemFactory', '$sce', 'resumeFactory', '$q', '$state', 'popUpManager', function($rootScope,$http,cookieManager,systemFactory,$sce,resumeFactory,$q,$state,popUpManager){
	    
        var questionnaries = [];
        var notifications = [];
        var ankets = [];
        var getAnket = function(id){
            var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'anket/'+id,
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    return $http(requestData).then(
                function(result){
                    var anket = result.data;
                    var isFoundResumeForAnket = false;
                    var questionnariesId = 0;
                    var currQ = {};
					for(var i=0; i<questionnaries.length; i++){
						if(questionnaries[i].anket_id === anket.id){
							currQ = questionnaries[i];
							if(currQ.ispublished){
					        	isFoundResumeForAnket = true;
								currQ.isHasAnket = true;
					        }else{
						        questionnariesId = currQ.id; 
					        }
				        }
			        }
			        
			        if(!isFoundResumeForAnket){
				        var newQuestionnarie = resumeFactory.emptyResume();
				        newQuestionnarie.companyInfo = anket.companyInfo;
				        newQuestionnarie.companyLogo = anket.companyLogo;
				        newQuestionnarie.companyName = anket.companyName;
				        newQuestionnarie.companyPosition = anket.companyPosition;
				        newQuestionnarie.anket_id = anket.id;
				        newQuestionnarie.video1.text = anket.video1_title;
				        newQuestionnarie.video2.text = anket.video2_title;
				        newQuestionnarie.video3.text = anket.video3_title;
				        newQuestionnarie.video4.text = anket.video4_title;
				        newQuestionnarie.isHasAnket = true;
						newQuestionnarie.isNew = false;
						newQuestionnarie.notificationId = 0;
						for(var i=0; i<notifications.length; i++){
							if(notifications[i].anket_id === newQuestionnarie.anket_id){
								newQuestionnarie.isNew = true;
								newQuestionnarie.notificationId = notifications[i].id;
							}
						}
						if(questionnariesId !== 0){
							if(currQ.hasOwnProperty('video1_url')){
								newQuestionnarie.video1.link = currQ.video1_url.replace("watch?v=", "v/");
						        newQuestionnarie.video2.link = currQ.video2_url.replace("watch?v=", "v/");
						        newQuestionnarie.video3.link = currQ.video3_url.replace("watch?v=", "v/");
						        newQuestionnarie.video4.link = currQ.video4_url.replace("watch?v=", "v/");
					        }

					        newQuestionnarie.id = currQ.id;
// 					        currQ = newQuestionnarie;
					        for(var i=0; i<questionnaries.length; i++){
								if(questionnaries[i].id === questionnariesId){
									if(!currQ.hasOwnProperty("firstname")){
										questionnaries[i] = newQuestionnarie;
									}else{
										questionnaries[i] = jQuery.extend(true, {}, currQ);
										questionnaries[i].isHasAnket = true;
										
									}
								}
							}
						}else{
					        questionnaries.push(newQuestionnarie);
					    }
			        }
			       if($rootScope.outSideAnketIdToShow != undefined && $rootScope.outSideAnketIdToShow != "" && $rootScope.outSideAnketIdToShow == anket.id){
				   		$rootScope.outSideAnketIdToShow = "";
		               	currQ.isNew = false;
						factory.deleteNotification(currQ.notificationId);
						if(currQ.ispublished){
							factory.setCurrentQuestionnarie(currQ.id);
							$state.go('publishedQuestionnaire');
						}else{
							factory.setCurrentQuestionnarie(anket.id);
		
							if(!currQ.hasOwnProperty('id')){
								factory.publishQuestionnarie(true);
							}else{
								$rootScope.currentResumeId = currQ.id;
							}
							$state.go('questionnairesEdit');
						}
		            }
			       
                }
            );
        };
       
        var getAnketsList = function(){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'folder/1',
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    $http(requestData).then(
		    	function(result){
			    	var anketList = result.data;
			    	ankets = anketList;
			    	if($rootScope.outSideAnketId){
				    	var isAlreadyInArray = false;
				    	for(var i=0;i<anketList.length;i++){
					    	if(anketList[i].id == $rootScope.outSideAnketId){
						    	isAlreadyInArray = true;
					    	}
				    	}
				    	if(!isAlreadyInArray){
					    	var temp = {};
					    	temp.id = $rootScope.outSideAnketId[0].toString();
					    	anketList[anketList.length] = temp;
					    	
				    		var request = {};
							request.aid = temp.id;
							request.to_user_id = $rootScope.userId;
							var requestData = {
			                    method: 'POST',
			                    url: systemFactory.serverUrl()+'share_anket',
			                    data: request,
			                    headers: {
								   'skey': systemFactory.md5(),
								}
			                };
			                $http(requestData).then(
			                	function(respond){console.log(respond);}
			                );
				    	}
				    	$rootScope.outSideAnketIdToShow = $rootScope.outSideAnketId;
				    	$rootScope.outSideAnketId = ''; 
				    }
					
			    	for(var i=0; i<anketList.length; i++)
                    {
                        getAnket(anketList[i].id);
                    }
                }
			);
		};
		var getNotificatoinList = function(){
		    var requestData = {
			    method: 'GET',
				url: systemFactory.serverUrl()+'notify',
				headers: {
				   'skey': systemFactory.md5()
				}
			};  
		    $http(requestData).then(
		    	function(result){
			    	var notificationList = result.data;
			    	notifications = notificationList;
			    	for(var i=0; i<notificationList.length; i++)
                    {
                        for(var j=0; j<questionnaries.length; j++){
							if(questionnaries[j].anket_id === notificationList[i].anket_id){
					        	questionnaries[j].isNew = true;
					        	questionnaries[j].notificationId = notificationList[i].id;
					        }
				        }
                    }
                    angular.element(document.getElementById('wrapper')).scope().updateNotifications();
                }
                
                
			);
		};

		var updateWithMyResume = function(questionnarie){
			var myResume = resumeFactory.myResume();
			
			if(!isFilled(questionnarie.firstname.text)) questionnarie.firstname.text = myResume.firstname.text;
			if(!isFilled(questionnarie.lastname.text)) questionnarie.lastname.text = myResume.lastname.text;
			if(!isFilled(questionnarie.email.text)) questionnarie.email.text = myResume.email.text;
			if(!isFilled(questionnarie.zipcode.text)) questionnarie.zipcode.text = myResume.zipcode.text;
			if(!isFilled(questionnarie.education.text)) questionnarie.education.text = myResume.education.text;
			if(!isFilled(questionnarie.areaofstudy.text)) questionnarie.areaofstudy.text = myResume.areaofstudy.text;
			if(!isFilled(questionnarie.experience.text)) questionnarie.experience.text = myResume.experience.text;
			if(!isFilled(questionnarie.industrydesired.text)) questionnarie.industrydesired.text = myResume.industrydesired.text;
			if(!isFilled(questionnarie.abletorelocate.text)) questionnarie.abletorelocate.text = myResume.abletorelocate.text;
			if(!isFilled(questionnarie.linkedIn.text)) questionnarie.linkedIn.text = myResume.linkedIn.text;
			if(!isFilled(questionnarie.additionalLink.text)) questionnarie.additionalLink.text = myResume.additionalLink.text;
			if(!isFilled(questionnarie.photo.link)) questionnarie.photo.link = myResume.photo.link;
			//if(!isFilled(questionnarie.id)) questionnarie.id = myResume.id;
		}
		var isFilled = function(textToCheck){
			if(textToCheck === undefined || textToCheck == null) textToCheck = "";
			return textToCheck.toString().length>2;
		}
		var currentQuestionnarie = 0;
		var currentVideo = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var currentPhoto = {
                name:'',
                link:'',
                html:'',
                error:false
        };
        var getCurrentQuestionnarie = function(){
	       	var index = 0;
	        for(var i=0; i<questionnaries.length; i++){
		        if(questionnaries[i].anket_id === currentQuestionnarie)
		        	index = i;
		    }
		    updateWithMyResume(questionnaries[index]);
			return questionnaries[index];  
        };
        var emptyQuestionnarie = function(){
	        	return {
		        	companyInfo:"",
					companyLogo:"",
					companyName:"",
					companyPosition:"",
					first_name:"",
					last_name:"",
					email:"",
					zip:"",
					city:"",
					state:"",
					education:"",
					areaofstudy:"",
					expirience:"",
					industrydesired:"",
					abletorelocate:"",
					linkedIn:"",
					photo_url:"",
					additionalLink:"",
					video1_title :"",
					video2_title :"",
					video3_title:"",
					video4_title :"",
					
					video1_url :"",
					video2_url :"",
					video3_url :"",
					video4_url :"",
					
					video1_thumbnail_url :"",
					video2_thumbnail_url :"",
					video3_thumbnail_url :"",
					video4_thumbnail_url :"",
					
					video_merged_title :"",
					video_merged_url :"",
					video_merged_thumbnail_url :"",
					
					ispublished :"",
					anket_id :""};
        }
	    var factory = {
		    notifications: function(){
			    return notifications
			},
		    questionnaries: function(){
			    return questionnaries;
		    },
		    ankets: function(){
			    return ankets;
		    },
		    deleteAnket: function(id){
			    var q = {};
			    for(var i=0;i<questionnaries.length;i++){
				    if(questionnaries[i].anket_id == id){
					    q = questionnaries[i];
				    }
			    }
			    this.deleteNotification(q.notificationId);
				this.eraseQuestionnarie(q);
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deleteanket',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
		
				}; 

			    $http(requestData).then(
				    function(respond){					   
					    for(var prop in ankets){
						    if(id == ankets[prop].id){
						        ankets.splice(ankets.indexOf(ankets[prop]), 1);
						    }
						}
				    }
			    );
			},
			deleteAllQuestionnaries: function(){
				
				questionnaries.forEach(function(q){
						this.deleteAnket(q.anket_id);	
					},this);
				questionnaries = [];
				var newServerResume = this.eraseQuestionnarie(resumeFactory.serverResume());
				resumeFactory.setServerResume(newServerResume);
				resumeFactory.restoreResume(true);
			},
			eraseQuestionnarie: function(q){
				var questionnarieToPublish = emptyQuestionnarie();
				questionnarieToPublish.id = q.id;
				questionnarieToPublish.anket_id = q.anket_id;
				questionnarieToPublish.ispublished = "false";
				q = questionnarieToPublish;

				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':questionnarieToPublish},
                    headers: {
					   'skey': systemFactory.md5()
					}
                };

                $http(requestData).then(
                    function(result){
	                    
	            });


				return questionnarieToPublish;
			},
		    deleteNotification: function(id){
			    if(id == 0) return;
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deletenotify',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
				};  
				 
			    $http(requestData);
			    for(var i=0; i<notifications.length; i++){
				    if(notifications[i].id === id){
			    		notifications.splice(i, 1);
			    	}
			    }
			    angular.element(document.getElementById('wrapper')).scope().updateNotifications();
		    },
		    setAnketsAnswers: function(){
			   
		        questionnaries = resumeFactory.allResume();
		        for(var i=0; i<questionnaries.length; i++){
			        questionnaries[i].isHasAnket = false;
			        questionnaries[i].isNew = false;
			        questionnaries[i].notificationId = 0;
		        }
		        getAnketsList();
		        getNotificatoinList();


	        },
	        currentQuestionnarie: function(){
		        return getCurrentQuestionnarie();
	        },
	        setCurrentQuestionnarie: function(anket_id){
		        currentQuestionnarie = anket_id;
	        },
			getPublishedQuestionnarieData: function()
			{
				urlparams=getUrlVars();	
				return $http.get(systemFactory.serverUrl()+'resume/'+currentQuestionnarie,null) 
		        .success(function(data) { 
		          return data;             
		        }) 
		        .error(function(err) {  
		          return err;
		        }); 

	        },
	        getQuestionnairesPreviewData: function()
			{
				urlparams=getUrlVars();	
				var qResume = {};
				var qCurrent = getCurrentQuestionnarie();

				qResume.companyInfo = qCurrent.companyInfo;
				qResume.companyLogo = qCurrent.companyLogo;
				qResume.companyName = qCurrent.companyName;
				qResume.companyPosition = qCurrent.companyPosition;
				qResume.first_name = qCurrent.firstname.text;
				qResume.last_name = qCurrent.lastname.text;
				qResume.email = qCurrent.email.text;
				qResume.zip = qCurrent.zipcode.text;
				qResume.city = qCurrent.city;
				qResume.state = qCurrent.state;
				qResume.education = qCurrent.education.text;
				qResume.areaofstudy = qCurrent.areaofstudy.text;
				qResume.expirience = qCurrent.experience.text;
				qResume.industrydesired = qCurrent.industrydesired.text;
				qResume.abletorelocate = qCurrent.abletorelocate.text==="Yes"?1:0;
				qResume.linkedIn = qCurrent.linkedIn.text;
				qResume.additionalLink = qCurrent.additionalLink.text;
				qResume.photo_url = qCurrent.photo.link;
				qResume.video1_title = qCurrent.video1.text;
				qResume.video2_title = qCurrent.video2.text;
				qResume.video3_title = qCurrent.video3.text;
				qResume.video4_title = qCurrent.video4.text;
				
				qResume.video1_url = qCurrent.video1.link;
				qResume.video2_url = qCurrent.video2.link;
				qResume.video3_url = qCurrent.video3.link;
				qResume.video4_url = qCurrent.video4.link;
				
				qResume.video_merged_url = "local";
				qResume.data = qResume;
				return qResume;
	        },
	        getCurrentVideo: function(){
			    return currentVideo;
		    },
            setCurrentVideo: function(video){
                jQuery.each(currentVideo, function(key, val) {
                    if(video.hasOwnProperty(key)){
                        currentVideo[key] = video[key];
                    }
                });
                getCurrentQuestionnarie()["video"+$rootScope.currentVideoIndex].link = video.link;
		    },
		    resetCurrentVideo: function(){
                currentVideo = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
		    getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            setCurrentPhoto: function(photo){
                jQuery.each(currentPhoto, function(key, val) {
                    if(photo.hasOwnProperty(key)){
                        currentPhoto[key] = photo[key];
                        getCurrentQuestionnarie().photo.link = photo[key];
                    }
                });
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'',
                        link:'',
                        html:'',
                        error:false
                };
		    },
		    checkCurrentQuestionnarie: function(){
			    return resumeFactory.checkResume(getCurrentQuestionnarie());
		    },
		    publishResume: function (isBackgroundPublish){
			    factory.publishQuestionnarie(isBackgroundPublish);
			},
		    publishQuestionnarie: function (isBackgroundPublish){
			    var currQuestionnarie = getCurrentQuestionnarie();
				var questionnarieToPublish = {};
				questionnarieToPublish.companyInfo = currQuestionnarie.companyInfo;
				questionnarieToPublish.companyLogo = currQuestionnarie.companyLogo;
				questionnarieToPublish.companyName = currQuestionnarie.companyName;
				questionnarieToPublish.companyPosition = currQuestionnarie.companyPosition;
				questionnarieToPublish.first_name = currQuestionnarie.firstname.text;
				questionnarieToPublish.last_name = currQuestionnarie.lastname.text;
				questionnarieToPublish.email = currQuestionnarie.email.text;
				questionnarieToPublish.zip = currQuestionnarie.zipcode.text;
				questionnarieToPublish.city = currQuestionnarie.city;
				questionnarieToPublish.state = currQuestionnarie.state;
				questionnarieToPublish.education = currQuestionnarie.education.text;
				questionnarieToPublish.areaofstudy = currQuestionnarie.areaofstudy.text;
				questionnarieToPublish.expirience = currQuestionnarie.experience.text;
				questionnarieToPublish.industrydesired = currQuestionnarie.industrydesired.text;
				questionnarieToPublish.abletorelocate = currQuestionnarie.abletorelocate.text==="Yes"?"1":"0";
				questionnarieToPublish.linkedIn = currQuestionnarie.linkedIn.text;
				questionnarieToPublish.photo_url = currQuestionnarie.photo.link;
				questionnarieToPublish.additionalLink = currQuestionnarie.additionalLink.text;
				questionnarieToPublish.video1_title = currQuestionnarie.video1.text;
				questionnarieToPublish.video2_title = currQuestionnarie.video2.text;
				questionnarieToPublish.video3_title = currQuestionnarie.video3.text;
				questionnarieToPublish.video4_title = currQuestionnarie.video4.text;
				
				questionnarieToPublish.video1_url = currQuestionnarie.video1.link;
				questionnarieToPublish.video2_url = currQuestionnarie.video2.link;
				questionnarieToPublish.video3_url = currQuestionnarie.video3.link;
				questionnarieToPublish.video4_url = currQuestionnarie.video4.link;
				
				questionnarieToPublish.video1_thumbnail_url = "";
				questionnarieToPublish.video2_thumbnail_url = "";
				questionnarieToPublish.video3_thumbnail_url = "";
				questionnarieToPublish.video4_thumbnail_url = "";
				
				questionnarieToPublish.video_merged_title = "";
				questionnarieToPublish.video_merged_url = currQuestionnarie.videoMerged.link;
				questionnarieToPublish.video_merged_thumbnail_url = "";
				
				questionnarieToPublish.ispublished = !isBackgroundPublish;
				questionnarieToPublish.anket_id = currQuestionnarie.anket_id;
				if(currQuestionnarie.hasOwnProperty('id'))
				questionnarieToPublish.id = currQuestionnarie.id;
				
				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'resume',
                    data: {'r':questionnarieToPublish},
                    headers: {
					   'skey': systemFactory.md5()
					}
                };
                //console.log(questionnarieToPublish);
                if(!isBackgroundPublish){
	                $state.go('loading');
	            }
                $http(requestData).then(
                    function(result){
                        var questionnarieId = result.data;
                        if(questionnarieId !== -1){
	                        if(!isBackgroundPublish){
		                        currQuestionnarie.id = questionnarieId;
		                        var requestDataMerge = {
									method: 'GET',
				                    url: "http://apevovideo.cloudapp.net/v/service1.svc/ffmpegmerge?resume_id="+questionnarieId
								};
								$http(requestDataMerge);
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnaire was successfully published. Now you can share it by link or send it directly to jobseekers using the ReelHire app',
									btnOneName:"OK"
								}
								popUpManager.popupModal(popupMessage);
		                        currQuestionnarie.ispublished = true;
		                        //alert("Congratulations! Your questionnarie has been published." );
		                        $state.go('questionnaires');
		                    }else{
			                    currQuestionnarie.id = questionnarieId;
			                    $rootScope.currentResumeId = questionnarieId;
			                    currQuestionnarie.ispublished = false;
		                    }
                        }
                    },function(error){
	                    
	                    var popupMessage = {
								header:'Something went wrong...',
								body:'Your questionnarie was not published. Try again.',
								btnOneName:"OK"
							}
							popUpManager.popupModal(popupMessage);
							
	                    
	                }
                );
	            
			},
			publishPhoto:function(){
				return $q(function(resolve, reject) {
					if(getCurrentQuestionnarie().photo.link.search('data') == 0){
						var imageData = getCurrentQuestionnarie().photo.link;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl() + "uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	$window.history.back();
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your photo was not uploaded. Try again.',
									btnOneName:"OK"
								}
								popUpManager.popupModal(popupMessage);
					        	
					        	
								reject(info);
				        	}
				        })
				        .error(function(err){
					        $window.history.back();
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again.',
								btnOneName:"OK"
							}
							popUpManager.popupModal(popupMessage);
					        
					        
				        	reject(err);
				        });
				    }else{
					    resolve(getCurrentQuestionnarie().photo.link);
				    };
				});
			},
    	}; 
		return factory;
	}]);
})();



(function() {
	
    'use strict';
    /*jshint camelcase: false */

    var recruiterFactory = angular.module('seeMeHireMeApp')
   	.factory('recruiterFactory',['$q', '$http', 'systemFactory', '$rootScope', 'popUpManager', '$state', function($q,$http,systemFactory,$rootScope,popUpManager,$state){
	   	$rootScope.favorites = [];
        var recruiterFolder = "";
        var listOfAnketsId = [];
        var ankets = [];
		var currentPhoto = {
                name:'companyLogo',
                link:'',
                html:'',
                error:false
        };
        
        
        
    	var client = new XMLHttpRequest();
        var emailTemplate = "";
		client.open('GET', '/app/serverRequests/emailTemplate.html');
		client.onreadystatechange = function() {
		  	emailTemplate = client.responseText;
		}
		client.send();
		
		
		var messageToNotify = function(name,link){
			var message = ""+emailTemplate;
			var startOfCandidateSection = message.indexOf("<br><div style=\"b");
			var lengthOfCandidateSection = message.indexOf('---->')-startOfCandidateSection+5;
			var sectionCandidate = message.slice(message.indexOf("<br><div style=\"b"),message.indexOf('<!---->'));
			message = message.replace(sectionCandidate,"");
			
			message = message.replace("{textTitle}","")
					.replace("{textBody}","Hi"+name+",<br><br>Please take a few moments and complete this brief Video Questionnaire from your phone or laptop.<br><br>http://reelhire.com/"+link+"<br><br>Show us your best, we can’t wait to see you!");
			return message;
		}
		
		var timeconvert = function(ds){
	        var D, dtime, T, tz, off,
	        dobj= ds.match(/(\d+)|([+-])|(\d{4})/g);
	        T= parseInt(dobj[0]);
	        tz= dobj[1];
	        off= dobj[2];
	        if(off){
	            off= (parseInt(off.substring(0, 2), 10)*3600000)+
	(parseInt(off.substring(2), 10)*60000);
	            if(tz== '-') off*= -1;
	        }
	        else off= 0;
	        return new Date(T+= off);
	    };
	    
	    
        var factory = {
	        
	        searchOnServer:function(request,candidates){
		        var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'search',
                    headers: {
					   'skey': systemFactory.md5()
					},
					data: request
                };	
                return $http(requestData).then(
                    function(result){
	                    var dates = [];
	                    for (var i = 0; i < result.data.length; i++) {
						    candidates[i] = jQuery.extend(true, {}, result.data[i]);
						    candidates[i].isNew = false;
						    var tempDate = timeconvert(candidates[i].dateadded);
							candidates[i].abletorelocate = candidates[i].abletorelocate=="1"?"Yes":"No"; 
							candidates[i].date = (tempDate.getMonth() + 1) + '/' + tempDate.getDate() + '/' +  tempDate.getFullYear();
							candidates[i].dateMilisec = candidates[i].dateadded.replace("/Date(", "").replace("+0000)/", "");
							dates[i] = candidates[i].dateMilisec;

						    if($rootScope.currentAnket != undefined && $rootScope.currentAnket != {} && candidates[i].anket_id != systemFactory.systemAnketID()){
							    for (var j = 0; j < $rootScope.currentAnket.notificationList.length; j++) {
								    var foundCandidateForNotification = false;
							    	if($rootScope.currentAnket.notificationList[j].oid === candidates[i].id){
								    	foundCandidateForNotification = true;
								    	candidates[i].isNew = true;
								    	candidates[i].notificationId = $rootScope.currentAnket.notificationList[j].id;
							    	}
							    	if(!foundCandidateForNotification){
								    	console.log($rootScope.currentAnket);
								    	console.log($rootScope.currentAnket.notificationList[j]);
								    	//factory.deleteNotification($rootScope.currentAnket.notificationList[j].id);
							    	}
							    }
						    }
						}
						if(result.data.length == 0){
							if($rootScope.currentAnket != undefined && $rootScope.currentAnket != {} && $rootScope.currentAnket.id!= systemFactory.systemAnketID()){
							    for (var j = 0; j < $rootScope.currentAnket.notificationList.length; j++) {
								    factory.deleteNotification($rootScope.currentAnket.notificationList[j].id);
							    }
						    }
						}
						candidates.splice(result.data.length,candidates.length-result.data.length);
						candidates.sort(function(a, b) {
							return b.dateMilisec - a.dateMilisec});
						return(candidates);
                    }
                );
	        },
	        getListOfFavorites:function(){
		         var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'favorites',
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
                        $rootScope.favorites = result.data;
                    }
                );
	        },
	        setFavorites:function(id,isFavorite){
		         var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'favorites',
                    headers: {
					   'skey': systemFactory.md5()
					},
					data: {'id':id}
                };
                if(!isFavorite){
	                requestData = {
	                    method: 'POST',
	                    url: systemFactory.serverUrl()+'removefavorites',
	                    headers: {
						   'skey': systemFactory.md5()
						},
						data: {'oid':id}
	                };
                }	
                return $http(requestData).then(
	                function(){
	                    if(isFavorite){
		                    if($.inArray(id, $rootScope.favorites) == -1) $rootScope.favorites.push(id);
	                    }else{
		                    if($.inArray(id, $rootScope.favorites) != -1) $rootScope.favorites.splice($.inArray(id, $rootScope.favorites), 1);
	                    }
	                }
                );
	        },
	        getRecruiterFolder: function(){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'folder',
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
	                    if(result.data.length == 0){
		                    var requestData2 = {
			                    method: 'POST',
			                    url: systemFactory.serverUrl()+'folder',
			                    data: {'name':'anketFolder'},
			                    headers: {
								   'skey': systemFactory.md5(),
								}
			                };	
			                return $http(requestData2).then(
			                    function(result){
				                    recruiterFolder = result.data;
				                    return result.data;
				                }
				            );
	                    }else{
                        	recruiterFolder = result.data[0].id;
							return recruiterFolder;
						}
                    }
                );
            },
            getListOfAnkets: function(){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'folder/'+recruiterFolder,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
                        listOfAnketsId = result.data;
                        ankets = [];
                        for(var i=0;i<listOfAnketsId.length;i++){
	                       ankets[i] = {};
	                       ankets[i].id = listOfAnketsId[i].id;
	                       ankets[i].additionalLink = "";
	                       ankets[i].companyInfo = "";
	                       ankets[i].companyLogo = "";
	                       ankets[i].companyName = "";
	                       ankets[i].companyPosition = "";
	                       ankets[i].email = "";
	                       ankets[i].folder_id = "";
	                       ankets[i].ispublished = "";
	                       ankets[i].owner_id = "";
	                       ankets[i].video1_title = "";
	                       ankets[i].video2_title = "";
	                       ankets[i].video3_title = "";
	                       ankets[i].video4_title = "";
	                       ankets[i].notificationList = [];
	                       factory.getAnket(i);
                        }
                        factory.getNotificatoinList();
                        return listOfAnketsId;
                    }
                );
            },
            ankets: function(){
	            return ankets;
            },
            newAnket:function(){
	        	var newAnket = {};
                var maxNumber = 0;
				for(var i=0;i<ankets.length;i++){
					if(Number(ankets[i].id)>=maxNumber){
						maxNumber = Number(ankets[i].id)*1.0000001;
						newAnket.companyLogo = ankets[i].companyLogo;
		                newAnket.companyName = ankets[i].companyName;
					}
				}
				newAnket.id = maxNumber;
                newAnket.additionalLink = "";
                newAnket.companyInfo = "";
                newAnket.companyPosition = "";
                newAnket.email = "";
                newAnket.folder_id = recruiterFolder;
                newAnket.ispublished = "False";
                newAnket.owner_id = $rootScope.userId;
                newAnket.video1_title = "";
                newAnket.video2_title = "";
                newAnket.video3_title = "";
                newAnket.video4_title = "";
                newAnket.notificationList = [];
				ankets.unshift(newAnket);
				return newAnket; 
            },
            getAnket:function(anketIndex){ 
                var requestData = {
                    method: 'GET',
                    url: systemFactory.serverUrl()+'anket/'+ankets[anketIndex].id,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };	
                return $http(requestData).then(
                    function(result){
	                    var notificationList = ankets[anketIndex].notificationList;
	                    ankets[anketIndex] = result.data;
	                    ankets[anketIndex].notificationList = notificationList;
	                    
                    }
                );
            },
            copyAnket:function(anket){
	        	var newAnket = jQuery.extend(true, {}, anket);
				newAnket.ispublished = "False";
				newAnket.notificationList = [];
				var maxNumber = 0;
				for(var i=0;i<ankets.length;i++){
					if(Number(ankets[i].id)>=maxNumber) maxNumber = Number(ankets[i].id)*(1.0000001);
				}
				newAnket.id = maxNumber;
				newAnket.companyPosition = anket.companyPosition + " Copy";
				ankets.unshift(newAnket);  
            },
            deleteAnket:function(anket){
	            for(var i=0;i<ankets.length;i++){
		            if(JSON.stringify(ankets[i]) === JSON.stringify(anket) ){
			            factory.deleteAnketFromServer(ankets[i].id);
			            ankets.splice(i, 1);
			            break;
		            }
	            }				
            },
            deleteAnketFromServer:function(anketIndex){ 
                var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'deleteanket',
                    headers: {
					   'skey': systemFactory.md5(),
					},
					data: {'id': anketIndex}
                };	
                return $http(requestData).then(
                    function(result){
// 						console.log(result);
                    }
                );
            },
            deleteAllAnkets:function(){
	            for(var i=0;i<ankets.length;i++){
			        factory.deleteAnketFromServer(ankets[i].id);
	            };
	            ankets = {};				
            },
            setRank: function(id,rank){
	            var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'rank',
                    headers: {
					   'skey': systemFactory.md5(),
					},
					data: {'resume_id': id,'rank':rank}
                };	
                return $http(requestData);
            },
            getNotificatoinList: function(){
			    var requestData = {
				    method: 'GET',
					url: systemFactory.serverUrl()+'notify',
					headers: {
					   'skey': systemFactory.md5()
					}
				};  
			    $http(requestData).then(
			    	function(result){
				    	var notificationList = result.data;
				    	for(var j=0;j<notificationList.length;j++){
					    	for(var i=0;i<ankets.length;i++){
					    		if(notificationList[j].anket_id === ankets[i].id){
						    		ankets[i].notificationList.push(notificationList[j]);
					    		}
					    	}
					    }
				    	
                	}
                );
            },
            deleteNotification: function(id){
			    if(id == 0) return;
			    var requestData = {
				    method: 'POST',
					url: systemFactory.serverUrl()+'deletenotify',
					headers: {
					   'skey': systemFactory.md5()
					},
					data: { 'id': id }
				};  
				 
			    $http(requestData);
			    for(var i=0;i<ankets.length;i++){
				    for(var j=0;j<ankets[i].notificationList.length;j++){
			    		if(ankets[i].notificationList[j].id === id){
				    		ankets[i].notificationList.splice(j,1);
			    		}
			    	}
		    	}
			    //angular.element(document.getElementById('wrapper')).scope().updateNotifications();
		    },
            errorField:function(){
	            var error = {};
	            error.companyInfo = false;
	            error.companyLogo = false;
	            error.companyName = false;
	            error.companyPosition = false;
	            error.video1_title = false;
	            return error;
            },
            checkAnket:function(anket,error){
	            var isCorrect = true;
                if(anket.companyInfo.length < 1){ error.companyInfo = true; isCorrect = false; };
                if(anket.companyLogo.length < 1){ error.companyLogo = true; isCorrect = false; };
                if(anket.companyName.length < 1){ error.companyName = true; isCorrect = false; };
                if(anket.companyPosition.length < 1){ error.companyPosition = true; isCorrect = false; };
                if(!isCorrect){
	                var popupMessage = {
						header:'',
						body:'Please check fields marked with red',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
					return false;
                }
                var oneFieldNotEmplty = false;
                for(var i=1;i<5;i++){
					if(anket["video"+i+"_title"].length > 0) oneFieldNotEmplty = true;
				}
				if(!oneFieldNotEmplty){
					var popupMessage = {
						header:'',
						body:'Please fill out at least one question',
						btnOneName:'OK'
					}
					popUpManager.popupModal(popupMessage);
					error.video1_title = true;
					return false;
				}
                return isCorrect;
            },
            setCurrentPhoto: function(photo){
	            currentPhoto.link = photo.link;
		    },
		    getCurrentPhoto: function(){
			    return currentPhoto;
		    },
            resetCurrentPhoto: function(){
                currentPhoto = {
                        name:'companyLogo',
                        link:'',
                        html:'',
                        error:false
                };
		    },
            publishPhoto:function(photoLink){
	            return $q(function(resolve, reject) {
					if(photoLink != undefined && photoLink.search('data') == 0){
						var imageData = photoLink;
			            var b64 = imageData.replace('data:image/png;base64,','');
			            var binary = atob(b64);
			            var array = [];
			            for (var i = 0; i < binary.length; i++) {
			                array.push(binary.charCodeAt(i));
			            }
			            var photoBlob = new Blob([new Uint8Array(array)], {type: 'image/png'});
		                var file = photoBlob;
		                var fd = new FormData();
				        fd.append('file', file);
				        fd.append('oid', '0');
				        fd.append('name', 'photo');
				        fd.append('extension', '.png');
				        fd.append('type_id', '1');
				        return $http.post(systemFactory.serverUrl()+"uploadfile", fd, {
				            transformRequest: angular.identity,
				            headers: {'Content-Type': 'multipart/form-data'}
				        })
				        .success(function(info){
					        if(info!=-1){
				        		resolve(info);
				        	}
				        	else{
					        	
					        	var popupMessage = {
									header:'Something went wrong...',
									body:'Your questionnaire was not published. Try again',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
					        	
					        	//alert("Something went wrong. You resume was not published. Try again.");
								reject(info);
				        	}
				        })
				        .error(function(err){
					        
					        var popupMessage = {
								header:'Something went wrong...',
								body:'Your photo was not uploaded. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);					        

					        //alert("Something went wrong. You resume was not published. Try again.");
				        	reject(err);
				        });
				    }else{
					    resolve(photoLink);
				    };
				});
            },
            sendAnket:function(){
				for(var i=0;i<$rootScope.selectedCandidates.length;i++){
					var request = {};
					request.aid = $rootScope.currentAnket.id;
					request.to_user_id = $rootScope.selectedCandidates[i].user_id;
					var requestData = {
	                    method: 'POST',
	                    url: systemFactory.serverUrl()+'share_anket',
	                    data: request,
	                    headers: {
						   'skey': systemFactory.md5(),
						}
	                };
	                //console.log($rootScope.selectedCandidates);
	                $http(requestData).then(
	                	function(respond){}
	                	);
	                	

                	var request = $http({
				    method: "post",
				    url: "/components/sendgrid-php/myMail.php",
				    data: {
					    sm: systemFactory.mc().id,
					    sp: systemFactory.mc().value,
				        emailTo: $rootScope.selectedCandidates[i].email,
				        emailFrom: $rootScope.userEmail,
				        subject : "Job Application",
						message : messageToNotify($rootScope.selectedCandidates[i].first_name,$rootScope.idToHash($rootScope.currentAnket.id,'webAnket'))
					    },
					    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					});
					
					request.success(function (dataQQQ) {

					});
	            }
	            $rootScope.selectedCandidates = [];
	            var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnarie was send.',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
            },
            checkRegistration: function(){
	            var ifRegistered = $rootScope.userEmail.indexOf("anonimus") != 0 || $rootScope.userEmail.length != 21;
	            if(!ifRegistered){
		            var popupMessage = {
						header:'',
						body:'Please register to have access for the all functionality',
						btnOneName:'Register',
						btnTwoName:'Register Later',
						btnOneClick:function(){
							$state.go('register');
						},
						btnTwoClick:function(){
							
						}
					}

					popUpManager.popupModal(popupMessage);
	            }
	            return ifRegistered;
            },
            publishAnket:function(anket){
				var anketToPublish = {};
				anketToPublish.companyInfo = anket.companyInfo;
				anketToPublish.companyLogo = anket.companyLogo;
				anketToPublish.companyName = anket.companyName;
				anketToPublish.ispublished = "True";
				anketToPublish.additionalLink = anket.additionalLink;
				anketToPublish.folder_id = anket.folder_id;
				anketToPublish.companyPosition = anket.companyPosition;
				anketToPublish.email = anket.email;
				anketToPublish.name = "";
				anketToPublish.linkedIn = "";
				anketToPublish.questions = [];
				anketToPublish.questions[0] = "q1";
				for(var i=1;i<6;i++){
					anketToPublish["video"+i+"_title"] = "";
				}
				var j = 1;
				for(var i=1;i<5;i++){
					if(anket["video"+i+"_title"].toString().length>0){
						anketToPublish["video"+j+"_title"] = ""+anket["video"+i+"_title"];
						j++;
					}
				}
				
				var requestData = {
                    method: 'POST',
                    url: systemFactory.serverUrl()+'anket',
                    data: anketToPublish,
                    headers: {
					   'skey': systemFactory.md5(),
					}
                };
                $http(requestData).then(
                    function(result){
	                    var anketID = result.data;
                        if(anketID != -1){
	                        $rootScope.currentAnket.id = anketID;
	                        if($rootScope.selectedCandidates == undefined || $rootScope.selectedCandidates.length == 0){
// 	                        	console.log(anketID);
		                        var popupMessage = {
									header:'Congratulations!',
									body:'Your questionnaire was successfully published. Now you can share it by link or send it directly to job seekers using app.',
									btnOneName:'OK'
								}
				
								popUpManager.popupModal(popupMessage);
								$state.go("recruiterQuestionnaires");
							}else{
								factory.sendAnket();
							}
								anket.ispublished = "True";
                        }else{
	                        var popupMessage = {
								header:'Something went wrong...',
								body:'Your questionnaire was not published. Try again',
								btnOneName:'OK'
							}
			
							popUpManager.popupModal(popupMessage);
                        }
                    },function(error){
	                    
	                     var popupMessage = {
							header:'Something went wrong...',
							body:'Your questionnaire was not published. Try again',
							btnOneName:'OK'
						}
		
						popUpManager.popupModal(popupMessage);
	                }
                );
            },
        };
        return factory;
     }])
	
})();
var Hashids=function(){"use strict";function Hashids(salt,minHashLength,alphabet){var uniqueAlphabet,i,j,len,sepsLength,diff,guardCount;this.version="1.0.2";this.minAlphabetLength=16;this.sepDiv=3.5;this.guardDiv=12;this.errorAlphabetLength="error: alphabet must contain at least X unique characters";this.errorAlphabetSpace="error: alphabet cannot contain spaces";this.alphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";this.seps="cfhistuCFHISTU";this.minHashLength=parseInt(minHashLength,10)>0?minHashLength:0;this.salt=typeof salt==="string"?salt:"";if(typeof alphabet==="string"){this.alphabet=alphabet}for(uniqueAlphabet="",i=0,len=this.alphabet.length;i!==len;i++){if(uniqueAlphabet.indexOf(this.alphabet.charAt(i))===-1){uniqueAlphabet+=this.alphabet.charAt(i)}}this.alphabet=uniqueAlphabet;if(this.alphabet.length<this.minAlphabetLength){throw this.errorAlphabetLength.replace("X",this.minAlphabetLength)}if(this.alphabet.search(" ")!==-1){throw this.errorAlphabetSpace}for(i=0,len=this.seps.length;i!==len;i++){j=this.alphabet.indexOf(this.seps.charAt(i));if(j===-1){this.seps=this.seps.substr(0,i)+" "+this.seps.substr(i+1)}else{this.alphabet=this.alphabet.substr(0,j)+" "+this.alphabet.substr(j+1)}}this.alphabet=this.alphabet.replace(/ /g,"");this.seps=this.seps.replace(/ /g,"");this.seps=this.consistentShuffle(this.seps,this.salt);if(!this.seps.length||this.alphabet.length/this.seps.length>this.sepDiv){sepsLength=Math.ceil(this.alphabet.length/this.sepDiv);if(sepsLength===1){sepsLength++}if(sepsLength>this.seps.length){diff=sepsLength-this.seps.length;this.seps+=this.alphabet.substr(0,diff);this.alphabet=this.alphabet.substr(diff)}else{this.seps=this.seps.substr(0,sepsLength)}}this.alphabet=this.consistentShuffle(this.alphabet,this.salt);guardCount=Math.ceil(this.alphabet.length/this.guardDiv);if(this.alphabet.length<3){this.guards=this.seps.substr(0,guardCount);this.seps=this.seps.substr(guardCount)}else{this.guards=this.alphabet.substr(0,guardCount);this.alphabet=this.alphabet.substr(guardCount)}}Hashids.prototype.encode=function(){var ret="",i,len,numbers=Array.prototype.slice.call(arguments);if(!numbers.length){return ret}if(numbers[0]instanceof Array){numbers=numbers[0]}for(i=0,len=numbers.length;i!==len;i++){if(typeof numbers[i]!=="number"||numbers[i]%1!==0||numbers[i]<0){return ret}}return this._encode(numbers)};Hashids.prototype.decode=function(hash){var ret=[];if(!hash.length||typeof hash!=="string"){return ret}return this._decode(hash,this.alphabet)};Hashids.prototype.encodeHex=function(str){var i,len,numbers;str=str.toString();if(!/^[0-9a-fA-F]+$/.test(str)){return""}numbers=str.match(/[\w\W]{1,12}/g);for(i=0,len=numbers.length;i!==len;i++){numbers[i]=parseInt("1"+numbers[i],16)}return this.encode.apply(this,numbers)};Hashids.prototype.decodeHex=function(hash){var ret=[],i,len,numbers=this.decode(hash);for(i=0,len=numbers.length;i!==len;i++){ret+=numbers[i].toString(16).substr(1)}return ret};Hashids.prototype._encode=function(numbers){var ret,lottery,i,len,number,buffer,last,sepsIndex,guardIndex,guard,halfLength,excess,alphabet=this.alphabet,numbersSize=numbers.length,numbersHashInt=0;for(i=0,len=numbers.length;i!==len;i++){numbersHashInt+=numbers[i]%(i+100)}lottery=ret=alphabet.charAt(numbersHashInt%alphabet.length);for(i=0,len=numbers.length;i!==len;i++){number=numbers[i];buffer=lottery+this.salt+alphabet;alphabet=this.consistentShuffle(alphabet,buffer.substr(0,alphabet.length));last=this.hash(number,alphabet);ret+=last;if(i+1<numbersSize){number%=last.charCodeAt(0)+i;sepsIndex=number%this.seps.length;ret+=this.seps.charAt(sepsIndex)}}if(ret.length<this.minHashLength){guardIndex=(numbersHashInt+ret[0].charCodeAt(0))%this.guards.length;guard=this.guards[guardIndex];ret=guard+ret;if(ret.length<this.minHashLength){guardIndex=(numbersHashInt+ret[2].charCodeAt(0))%this.guards.length;guard=this.guards[guardIndex];ret+=guard}}halfLength=parseInt(alphabet.length/2,10);while(ret.length<this.minHashLength){alphabet=this.consistentShuffle(alphabet,alphabet);ret=alphabet.substr(halfLength)+ret+alphabet.substr(0,halfLength);excess=ret.length-this.minHashLength;if(excess>0){ret=ret.substr(excess/2,this.minHashLength)}}return ret};Hashids.prototype._decode=function(hash,alphabet){var ret=[],i=0,lottery,len,subHash,buffer,r=new RegExp("["+this.guards+"]","g"),hashBreakdown=hash.replace(r," "),hashArray=hashBreakdown.split(" ");if(hashArray.length===3||hashArray.length===2){i=1}hashBreakdown=hashArray[i];if(typeof hashBreakdown[0]!=="undefined"){lottery=hashBreakdown[0];hashBreakdown=hashBreakdown.substr(1);r=new RegExp("["+this.seps+"]","g");hashBreakdown=hashBreakdown.replace(r," ");hashArray=hashBreakdown.split(" ");for(i=0,len=hashArray.length;i!==len;i++){subHash=hashArray[i];buffer=lottery+this.salt+alphabet;alphabet=this.consistentShuffle(alphabet,buffer.substr(0,alphabet.length));ret.push(this.unhash(subHash,alphabet))}if(this._encode(ret)!==hash){ret=[]}}return ret};Hashids.prototype.consistentShuffle=function(alphabet,salt){var integer,j,temp,i,v,p;if(!salt.length){return alphabet}for(i=alphabet.length-1,v=0,p=0;i>0;i--,v++){v%=salt.length;p+=integer=salt.charAt(v).charCodeAt(0);j=(integer+v+p)%i;temp=alphabet.charAt(j);alphabet=alphabet.substr(0,j)+alphabet.charAt(i)+alphabet.substr(j+1);alphabet=alphabet.substr(0,i)+temp+alphabet.substr(i+1)}return alphabet};Hashids.prototype.hash=function(input,alphabet){var hash="",alphabetLength=alphabet.length;do{hash=alphabet.charAt(input%alphabetLength)+hash;input=parseInt(input/alphabetLength,10)}while(input);return hash};Hashids.prototype.unhash=function(input,alphabet){var number=0,pos,i;for(i=0;i<input.length;i++){pos=alphabet.indexOf(input[i]);number+=pos*Math.pow(alphabet.length,input.length-i-1)}return number};if(typeof define==="function"&&typeof define.amd==="object"&&define.amd){define(function(){return Hashids})}return Hashids}();
(function() {

    'use strict';

    angular.module('seeMeHireMeApp')
      .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
          .state('exampleResume', {
            url: '/exampleResume',
            templateUrl: 'app/webResume/webResume.html',
            controller: 'webResumeCtrl',
            params:{
	            buttonBack:true
            },
            resolve: {
	            prepareData: ['resumeFactory', function(resumeFactory)
				{
					return resumeFactory.getExampleData();
				}]
            }
          });
      }]);
      
})();
