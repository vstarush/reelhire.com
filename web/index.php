<!DOCTYPE html>
<html>
<head>
	<title>ReelHire</title>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="C">
    <meta name="keywords" content="reelhire, job, hire, recruiter, find job, employer, employee">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1043080494062-qr9m3t22e4n54fbvgjko5hkslnp18te1.apps.googleusercontent.com">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">    


	<!-- External Styles -->
    <link rel='stylesheet' href="//fonts.googleapis.com/css?family=Lato:300,700"  type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/4.12.0/bootstrap-social.css" /> 
    <link rel="stylesheet" href="//code.jquery.com/ui/jquery-ui-git.css">
    <link rel="stylesheet" href="//assets-cdn.ziggeo.com/v1-stable/ziggeo.css" />
    
    
	<!--  Local Styles -->
    <link rel="stylesheet" href="app/website/Libs/hover.css">
    <link rel="stylesheet" href="app/app.css">
    <link rel="stylesheet" href="app/topbar/topbar.css">
    <link rel="stylesheet" href="app/panel/panel.css">
    <link rel="stylesheet" href="app/panelRecruiter/panelRecruiter.css">
    <link rel="stylesheet" href="app/resumeEdit/resumeEdit.css">
    <link rel="stylesheet" href="app/login/login.css">
    <link rel="stylesheet" href="app/loading/loading.css"> 
    <link rel="stylesheet" href="app/chooseRole/chooseRole.css"> 
    <link rel="stylesheet" href="app/preview/preview.css">
    <link rel="stylesheet" href="app/getLink/getLink.css"> 
    <link rel="stylesheet" href="app/sendTo/sendTo.css"> 
    <link rel="stylesheet" href="app/photoUpload/photoUpload.css">
    <link rel="stylesheet" href="app/videoUpload/videoUpload.css">
    <link rel="stylesheet" href="app/register/register.css">
    <link rel="stylesheet" href="app/webResume/webResume.css">
    <link rel="stylesheet" href="app/questionnaires/questionnaires.css">
    <link rel="stylesheet" href="app/questionnairesEdit/questionnairesEdit.css">
	<link rel="stylesheet" href="app/popup/popup.css">
    <link rel="stylesheet" href="app/popup/welcomePopUp.css">
    <link rel="stylesheet" href="app/profile/profile.css">
    <link rel="stylesheet" href="app/changePassword/changePassword.css">
    <link rel="stylesheet" href="app/insideQuestionnaire/insideQuestionnaire.css">
    <link rel="stylesheet" href="app/newQuestionnaire/newQuestionnaire.css">
    <link rel="stylesheet" href="app/recruiterQuestionnaires/recruiterQuestionnaires.css">
    <link rel="stylesheet" href="app/website/main/pageMain.css">
    <link rel="stylesheet" href="app/website/about/about.css"> 
<!--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/animatecss/3.5.1/animate.min.css">-->
    <link rel="stylesheet" href="app/website/Libs/animate.css">
<!--     <link rel="stylesheet" href="components/ziggeo/ziggeoapi.css" /> Custom Recorder css-->
    
    
    
    
</head>
<body ng-controller="AppCtrl"> <!-- ng-controller="AppCtrl" > -->

	<!-- Include Angulars templates -->
	<ng-include src="'app/topbar/topbar.html'"  ng-show="!checkIfMobile() || !ifWebResume()"></ng-include>
	<ng-include src="'app/panel/panel.html'" ng-show="type==='seeker'"></ng-include>
	<ng-include src="'app/panelRecruiter/panelRecruiter.html'" ng-show="type==='recruiter'||type==='recruter'"></ng-include>
    
	<!--     App container -->
    <ui-view autoscroll="true"></ui-view>
	
	
	
    <!-- CDN -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.1/moment.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
	<script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>	   
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-cookies.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-resource.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-cookies.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-sanitize.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-animate.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.10/ngStorage.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/angular-moment/0.9.2/angular-moment.min.js"></script>
	<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-0.14.3.js"></script>
	<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.3.js"></script>
<!-- 	<script src="//cdn.temasys.com.sg/adapterjs/0.13.x/adapter.min.js"></script> -->
<!-- 	<script src="//cdn.rawgit.com/webrtc/adapter/master/adapter.js"></script>   Not found-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.5/clipboard.min.js"></script>
	<script src="//crypto-js.googlecode.com/svn/tags/3.0.2/build/rollups/md5.js"></script>
    <script src="//apis.google.com/js/client:platform.js"></script>	
    <script src="//assets-cdn.ziggeo.com/v1-stable/ziggeo.js"> 
	
	
	<!-- Local libs -->
	<script src="components/scriptCam/scriptcam.js"></script> 
	<script src="components/WebRTC/RecordRTC.js"></script>  
	<script src="components/webCamJS/webcam.js"></script>
	<script src="components/cropImage/cropbox/cropbox.js"></script>

   
	<!-- Angular controllers and routers -->
	<script src="min/app.js"></script>
   
</body>
</html>